(* --------------------------------------------------------------------------

   Pointers Chain of Responsibilty Design Pattern (PCOR)
   A part of the Pointers OR - Generator (PORG) 
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/cor.html

   This package uses the Delphi Container Library
   (c) Jean-Philippe BEMPEL aka RDM rdm_30@yahoo.com
   Download http://sourceforge.net/projects/dclx/

   Version: 1.0
   $Id: strobj.pas,v 1.1.1.1 2010/02/28 12:15:10 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit strobj;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils; 

type

  (* ------------------------------------------------------------------------
   Objekt-Wrapper für einen String, da der String in Lazarus nicht direkt
   von TObject abgeleitet ist.
   ------------------------------------------------------------------------ *)
  TString = class
    data : String;
  public
    constructor create(aString : String);
    procedure setString(aString : String);
    function  getString() : String;
    procedure replace(astring : String);
  end;

implementation

 (* ------------------------------------------------------------------------
    Methoden von TString
    ------------------------------------------------------------------------ *)

constructor TString.create(aString : String);
begin
  inherited create();
  data := aString;
end;

procedure TString.setString(aString : String);
begin
  data := aString;
end;

function TString.getString() : String;
begin
  result := data;
end;

procedure TString.replace(aString : String);
begin
   data := aString;
end;

end.

