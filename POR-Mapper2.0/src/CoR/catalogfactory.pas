(* --------------------------------------------------------------------------

   Pointers Chain of Responsibilty Design Pattern (PCOR)
   A part of the Pointers OR - Generator (PORG) 
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/cor.html

   This package uses the Delphi Container Library
   (c) Jean-Philippe BEMPEL aka RDM rdm_30@yahoo.com
   Download http://sourceforge.net/projects/dclx/

   Version: 1.0
   $Id: catalogfactory.pas,v 1.1.1.1 2010/02/28 12:15:10 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit catalogfactory;

{$mode delphi}

interface

uses Classes, SysUtils, HashMap, Vector, catalog, stack;

type
 
  (* The TCatalogFactory is the default mechanism to load catalogs from
     a configuration file. The format of the configuration file is:

     ---------------------------------------------------------------------------
     definition     ::  meta [chaindef] catalog {catalog}
     meta           :: 'meta:' (a...z) 'endmeta:'
     chaindef       :: 'chaindef:' (a..z) simplecommand {simplecommand}
                       'endchaindef:'
     simplecommand  :: 'command:' class
     catalog        :: 'catalog:' (a..z) (complexcommand | chain | @chain)
                       {(complexcommand | chain | @chain)} 'endcatalog:'
     complexcommand :: 'command:' (a..z) class
     chain          :: 'chain:' (a..z) (simplecommand | chain | @chain)
                       {(simplecommand | chain | @chain)}'endchain:'
     @chain         :: '@chain' (a..z)
     ---------------------------------------------------------------------------

     where:

     {}     == 0..n
     meta   == exactly one
     []     == optional
     'a'    == exactly the string 'a' (terminal symbol)
     a..z   == Identifier
     class  == An object derived from TPersistence

     It is possible to include files via @include derictive, also recursive

     Additional it is possible to add new catalogs manually.
  *)

  TCatalogFactory = class
  private
    catalog    : TStrHashMap;  // The catalogs
    chainstack : TStack;       // Stack for recursive stacks
    chainstr   : TStrStack;    // Stack for the chainnames
    chaindefs  : TStrHashMap;  // List for chaindefs
    state      : Integer;      // State of the statemaschine
    isCatalog  : Boolean;      // State in catalog
    isChain    : Boolean;      // State in chain
    isChainDef : Boolean;      // State in chaindef
    isReference: Boolean;      // State in reference
    recursion  : Integer;      // Depth of the recursion
    function  loadFromFile(filename : String) : TVector;
    function  tokenize(const s : String) : TVector;
    procedure findestate(keyword : String);
    procedure analyseMETAAndCreateCatalogs(METAList : TVector);
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure  addCatalog(AName: String; ACatalog: TCatalog);
    function   getCatalog(AName: String) : TCatalog;
    procedure  loadCatalogData(fname: String);
  end;

const
  stbegin           = 0;
  stmeta            = 1;
  stmetaend         = 2;
  stcatalog         = 3;
  stcatalogend      = 4;
  stcommand         = 5;
  stchain           = 6;
  stchainend        = 7;
  stchaincommand    = 8;
  stchainrekursiv   = 20;
  stchaindef        = 30;
  stchaindefend     = 31;
  stchaindefcommand = 32;
  streferenz        = 40;
  stEnd             = 99;

  // Singleton Semantics
  function getCatalogFactory() : TCatalogFactory;

implementation

uses command, chain, strobj, {$I allcommands.inc};

var
  instanceCatalogFactory : TCatalogFactory;
  avoidInstancing        : boolean;

function getCatalogFactory() : TCatalogFactory;
begin
  if instanceCatalogFactory = nil then
  begin
    avoidInstancing := false;
    instanceCatalogFactory := TCatalogFactory.Create();
    avoidInstancing := true;
  end;
  result := instanceCatalogFactory;
end;

(* ----------------------------- Construction --------------------------------*)

// Default Constructor
constructor TCatalogFactory.Create;
begin
  if avoidInstancing then raise Exception.Create('Not allowed to instanciate Factory');
  inherited;
  catalog := TStrHashMap.Create();
  chainstack := TStack.Create();
  chainstr := TStrStack.Create();
  chaindefs := TStrHashMap.Create();
end;

// Default Destructor
destructor TCatalogFactory.Destroy;
begin
  inherited;
  catalog.free();
  chainstack.Free();
  chainstr.Free();
  chaindefs.Free();
end;

(* ------------------------ Public Methods -----------------------------*)

// Adds a new catalog to the factory.
procedure TCatalogFactory.addCatalog(AName: String; ACatalog: TCatalog);
begin
  catalog.PutValue(AnsiLowerCase(AName), ACatalog);
end;

// Gets a catalog out of the factory.
function TCatalogFactory.getCatalog(AName: String) : TCatalog;
begin
  result := TCatalog(catalog.GetValue(AnsiLowerCase(AName)));
end;

// Loads meta data for a catalog description.
procedure TCatalogFactory.loadCatalogData(fname: String);
var 
  metadata : TVector;
begin
  metadata := loadFromFile(fname);
  analyseMETAAndCreateCatalogs(metadata);
  FreeAndNil(metadata);
end;

(* -------------------------- Private Methods  -------------------------------*)

// Reads a file from the filesystem
function TCatalogFactory.loadFromFile(filename : String) : TVector;
var
  vec,
  vecar : TVector;
  I     : Integer;
  s     : String;
  fname : String;
  path  : String;
begin
  vecar := TVector.Create();
  with TStringList.Create() do
  begin
    LoadFromFile(filename);
    For I := 0 to Count -1 do
    begin
      s := Strings[I];
      s := trim(s);
      if s <> '' then
      begin
        if (pos('@include', s) > 0) then
        begin
          fname := copy(s, 9, length(s) - 8);
          path := ExtractFileDir(filename);
          loadFromFile(path + '\' + fname);
          continue;
        end;
        vec := tokenize(s);
        vecar.add(vec);
      end;
    end;
  end;
  result := vecar;
end;

// Tokenized a line
function TCatalogFactory.tokenize(const s : String) : TVector;
var
  scopy      : String;
  vec        : TVector;
  temp       : String;
  delemitter : Char;
begin
  delemitter := ' ';
  vec := TVector.Create();
  scopy := s;
  scopy := Trim(scopy);

  if (scopy <> '') then
     scopy := scopy + ' ';

  while pos(delemitter, scopy) > 0  do
  begin
    temp := copy(scopy, 1, pos(delemitter, scopy) - 1);
    vec.add(TString.Create(temp));
    delete(scopy, 1, pos(delemitter, scopy));
  end;
  result := vec;
end;

// Findet den Richtigen Status der statemaschine anhand von Schlüsselwörtern
procedure TCatalogFactory.findestate(keyword : String);
begin

  // references
  if (keyword = '@chain') then
  begin
    isReference := true;
    state := streferenz;
  end;

  // begin of the meta section
  if (state = stBegin) and (keyword = 'meta:') 
    then state := stmeta;

  // end of the meta section
  if (state = stmeta) and (keyword = 'endmeta:')
    then state := stmetaend;

  // begin of chaindef
  if (((state = stmetaend) or (state = stchaindefend))
     and (keyword = 'chaindef:')) then
  begin
    state := stchaindef;
  end;

  // end of chaindef
  if ((state = stchaindefcommand) and (keyword = 'endchaindef:')) then
  begin
    state := stchaindefend;
  end;

  // commanddescription in chaindefs
  if ((keyword = 'command:') and isChaindef and not isCatalog) then
  begin
    state := stchaindefcommand;
  end;

  // begin of a new catalog description
  if ((state = stmetaend) or (state = stcatalogend) or (state = stchaindefend)) and
      (keyword = 'catalog:')
  then begin
      state := stcatalog;
      isCatalog := true;
  end;

  // end of a catalog description
  if (keyword = 'endcatalog:') then
  begin
    state := stcatalogend;
    isCatalog := false;
  end;

  // begin of a command description
  if (keyword = 'command:') and isCatalog and not isChain then
  begin
    state := stCommand;
  end
  else if (keyword = 'command:') and isCatalog and isChain then
  begin
    state := stChainCommand;
  end else
  begin
    // Error
  end;

  // begin of a chain description
  if (keyword = 'chain:') and
      isCatalog and
      (state <> stchain) and
      (state <> stChainCommand) then
  begin
    state := stchain;
    isChain := true;
  end else

  // recursive chain descriptions
  if  (keyword = 'chain:') and
      ischain and
      isCatalog then
  begin
    state := stchainrekursiv;
  end;

  // end of a chain description
  if (keyword = 'endchain:') then
  begin
    state := stchainend;
    if recursion = 0 then
    isChain := false;
  end;
end;

(*
  statemaschine for building catalogs
*)
procedure TCatalogFactory.analyseMETAAndCreateCatalogs(METAList : TVector);
var
  line       : TVector;       // A line of the configuration file
  first      : String;        // The first token of the line
  catalog    : TCatalog;      // Current catalog instance
  chain      : TChain;        // Current chain instance
  refchain   : TChain;        // Building of a chainreference
  command    : TCommand;      // Current command instance
  catName    : String;        // The name of the catalog
  chName     : String;        // The name of the chain
  chdefName  : String;        // The name of the chaindefinition
  bezeichner : String;        // String variable
  tc         : TCommandClass; // Class reference of a TCommand class
  I          : Integer;       // for loops
  save       : TChain;        // save in recursion
begin
  // Initialization
  state := stBegin;
  isCatalog  := false;
  isChain    := false;
  isChainDef := false;
  catalog    := nil;
  chain      := nil;
  refchain   := nil;
  command    := nil;

  for I := 0 to METAList.Size - 1 do
  begin
    line := TVector(METAlist.GetObject(I));
    first := TString(line.GetObject(0)).getString();
    first := Trim(first);

    // Comments are not significant
    if  first = '#' then continue;

    first := AnsiLowerCase(first);
    findeState(first);

    case state of
    
    stmeta: ; // do nothing

    // Create new catalog
    stcatalog:
      begin
         Catalog := TCatalog.Create();
         catName := TString(line.GetObject(1)).getString();
      end;

    // Add catalog to the factory
    stcatalogend:
      begin
         addCatalog(catName, Catalog);
      end;  

    // Construct a new command
    stcommand:
      begin
         bezeichner := TString(line.GetObject(2)).getString();
         tc := TCommandClass(FindClass(bezeichner));
         command := TCommand(tc.newinstance);
         Catalog.addCommand(TString(line.GetObject(1)).getString(), command);
      end;

    // create a new chain
    stchain:
      begin
        Chain := TChain.Create();
        chName := TString(line.GetObject(1)).getString();
      end;

    // recursive description of chains
    stchainrekursiv:
      begin
        chainstack.push(chain);
        chainstr.push(chName);
        inc(recursion);
        Chain := TChain.Create();
        chName := TString(line.GetObject(1)).getString();
      end;

    // end of chain, save it
    stchainend:
      begin
         if recursion = 0 then
           Catalog.addCommand(chName, Chain)
         else
         // if recursion chain definition then a little bit poping
         begin
            save := Chain;

            Chain := TChain(chainstack.pop());
            chName := chainstr.Pop();
            dec(recursion);

            Chain.addCommand(save);
            state := stChain;
         end
      end;

    // Construct a new command
    stchaincommand:
      begin
         bezeichner := TString(line.GetObject(1)).getString();
         tc := TCommandClass(FindClass(bezeichner));
         command := TCommand(tc.newinstance);
         Chain.addCommand(command);
      end;

    // begin of a chaindef section
    stchaindef:
      begin
        chain := TChain.Create();
        chdefName := TString(line.GetObject(1)).getString();
        isChainDef := true;
      end;

    // Creates command in a chaindef section
    stchaindefcommand:
      begin
         bezeichner := TString(line.GetObject(1)).getString();
         tc := TCommandClass(FindClass(bezeichner));
         command := TCommand(tc.newinstance);
         chain.addCommand(command);
      end;

    // End of a chaindef section
    stchaindefend:
      begin
        chaindefs.PutValue(chdefname, chain);
        isChainDef := false;
      end;

    // References
    streferenz:
      begin
        refchain := TChain(chaindefs.getValue(TString(line.GetObject(1)).getString()));
        if (refchain = nil) then raise
          Exception.Create('Unable to load chainreference: ' +
                            TString(line.GetObject(1)).getString());
        if (ischain) then
        begin
          chain.addCommand(refChain);
        end else
        begin
          catalog.addCommand(TString(line.GetObject(1)).getString(), refChain);
        end;
        isReference := false;
      end;
    end; // case
  end; // for
end;

initialization
  avoidInstancing := true;

end.








