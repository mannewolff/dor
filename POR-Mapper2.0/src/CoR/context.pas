(* --------------------------------------------------------------------------

   Pointers Chain of Responsibilty Design Pattern (PCOR)
   A part of the Pointers OR - Generator (PORG) 
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/cor.html

   This package uses the Delphi Container Library
   (c) Jean-Philippe BEMPEL aka RDM rdm_30@yahoo.com
   Download http://sourceforge.net/projects/dclx/

   Version: 1.0
   $Id: context.pas,v 1.1.1.1 2010/02/28 12:15:10 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit context;

interface

uses HashMap, vector, strobj;

type

  TContext = class
  private
    map: TStrHashMap;
  public
    constructor Create; overload;
    destructor Destroy; override;
    function get(Name: string): TObject;
    procedure put(Name: string; AObject: TObject);
    function removeObject(Name: string): TObject;
    function getString(Name: string): string;
    procedure putString(Name: string; AObject: string);
    function getKeys(): TVector;
  end;


implementation

uses DCL_intf;

constructor TContext.Create;
begin
  inherited;
  map := TStrHashMap.Create(10, True);
end;

destructor TContext.Destroy;
begin
  map.Free;
  inherited;
end;

function TContext.get(Name: string): TObject;
begin
  Result := map.GetValue(Name);
end;

procedure TContext.put(Name: string; AObject: TObject);
begin
  map.putValue(Name, AObject);
end;

function TContext.getString(Name: string): string;
begin
  Result := TString(map.GetValue(Name)).getString();
end;

procedure TContext.putString(Name: string; AObject: string);
begin
  map.putValue(Name, TString.Create(AObject));
end;

function TContext.getKeys(): TVector;
var
  vec:  TVector;
  aset: IStrSet;
  It:   IStrIterator;
  s:    string;
begin
  aset := map.KeySet();
  vec  := TVector.Create();

  It := aset.First();
  while It.HasNext do
  begin
    s := It.Next;
    vec.Add(TString.Create(s));
  end;
  Result := vec;
end;

function TContext.removeObject(Name: string): TObject;
begin
  Result := TObject(map.remove(Name));
end;

end.

