(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: config.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit config;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XMLCfg;

type

  (*
    Struktur der XML-Konfiguration eines Projekts

            <config>
              <projektname name="">
          	  <templatedir pfad=""/>
          	  <unitdir pfad=""/>
          	  <sqldir pfad=""/>
          	  <unitfile datei=""/>
          	  <metadatenfile datei=""/>
          	  <lazarusunit wert=""/>
          	  <sqlscript wert=""/>
              </projektname>
          </config>

  *)

  {TConfig}
  TConfig = class
  private
     XMLConfig  : TXMLConfig;   // Lazarus XML Komponente
     Filename   : String;       // Dateiname der Konfigurationsdatei
     function getXMLStringValue(path : String) : String;
     function getXMLBoolValue(path : String) : Boolean;
  public
     constructor create(fname : String); overload;
     destructor Destroy; override;
     function getProjektname() : String;
     procedure setProjektname(aname : String);
     function getTemplateDir() : String;
     procedure setTemplateDir(aTemplateDir : String);
     function getUnitDir() : String;
     procedure setUnitDir(aUnitDir : String);
     function getSQLDir() : String;
     procedure setSQLDir(aSQLDir : String);
     function getUnitFile() : String;
     procedure setUnitFile(aUnitFile : String);
     function getMetaDatenFile() : String;
     procedure setMetaDatenFile(aMetaDatenFile : String);
     function getDatabasename() : String;
     procedure setDatabasename(aDatabasename : String);
     function getLazarusUnit() : Boolean;
     procedure setLazarusUnit(aLazarusUnit : Boolean);
     function getSQLScript() : Boolean;
     procedure setSQLScript(aSQLScript : Boolean);
     function getVersion() : String;
     procedure setVersion(aVersion : String);
  end;

implementation

(* ----------------------- Instanzmethoden public ----------------------------*)
(*                            Schnittstelle                                   *)
(* ---------------------------------------------------------------------------*)
function TConfig.getProjektname() : String;
begin
  result := getXMLStringValue('projektname/name');
end;

procedure TConfig.setProjektname(aname : String);
begin
  XMLConfig. SetValue('projektname/name', aname);
  XMLConfig.Flush();
end;

function TConfig.getTemplateDir() : String;
begin
  result := getXMLStringValue('projektname/templatedir/pfad');
end;

procedure TConfig.setTemplateDir(aTemplateDir : String);
begin
  XMLConfig. SetValue('projektname/templatedir/pfad', aTemplateDir);
  XMLConfig.Flush();
end;

function TConfig.getUnitDir() : String;
begin
  result := getXMLStringValue('projektname/unitdir/pfad');
end;

procedure TConfig.setUnitDir(aUnitDir : String);
begin
  XMLConfig. SetValue('projektname/unitdir/pfad', aUnitDir);
  XMLConfig.Flush();
end;

function TConfig.getSQLDir() : String;
begin
  result := getXMLStringValue('projektname/sqldir/pfad');
end;

procedure TConfig.setSQLDir(aSQLDir : String);
begin
  XMLConfig. SetValue('projektname/sqldir/pfad', aSQLDir);
  XMLConfig.Flush();
end;

function TConfig.getUnitFile() : String;
begin
  result := getXMLStringValue('projektname/unitfile/datei');
end;

procedure TConfig.setUnitFile(aUnitFile : String);
begin
  XMLConfig. SetValue('projektname/unitfile/datei', aUnitFile);
  XMLConfig.Flush();
end;

function TConfig.getMetaDatenFile() : String;
begin
  result := getXMLStringValue('projektname/metadatenfile/datei');
end;

procedure TConfig.setMetaDatenFile(aMetaDatenFile : String);
begin
  XMLConfig. SetValue('projektname/metadatenfile/datei', aMetaDatenFile);
  XMLConfig.Flush();
end;

function TConfig.getDataBaseName() : String;
begin
  result := getXMLStringValue('projektname/databasename/wert');
end;

procedure TConfig.setDatabasename(aDatabasename : String);
begin
  XMLConfig. SetValue('projektname/databasename/wert', aDatabasename);
  XMLConfig.Flush();
end;

function TConfig.getLazarusUnit() : Boolean;
begin
  result := getXMLBoolValue('projektname/lazarusunit/wert');
end;

procedure TConfig.setLazarusUnit(aLazarusUnit : Boolean);
begin
  if aLazarusUnit then
    XMLConfig. SetValue('projektname/lazarusunit/wert', 'true') else
    XMLConfig. SetValue('projektname/lazarusunit/wert', 'false');
  XMLConfig.Flush();
end;

function TConfig.getSQLScript() : Boolean;
begin
  result := getXMLBoolValue('projektname/sqlscript/wert');
end;

procedure TConfig.setSQLScript(aSQLScript : Boolean);
begin
  if aSQLScript then
    XMLConfig. SetValue('projektname/sqlscript/wert', 'true') else
    XMLConfig. SetValue('projektname/sqlscript/wert', 'false');
  XMLConfig.Flush();
end;

function TConfig.getVersion() : String;
begin
  result := getXMLStringValue('projektname/xversion/wert');
end;

procedure TConfig.setVersion(aVersion : String);
begin
  XMLConfig. SetValue('projektname/xversion/wert', aVersion);
  XMLConfig.Flush();
end;

(* -------------------- Konstruktion, Destruktion ----------------------------*)
constructor TConfig.create(fname : String);
begin
  inherited Create();
  Filename := fname;
  XMLConfig := TXMLConfig.Create(nil);
  XMLConfig.RootName := 'config';
  XMLConfig.Filename := fname;
end;

destructor TConfig.Destroy();
begin
   inherited;
   XMLConfig.Free();
end;

(* ---------------------- Instanzmethoden private ----------------------------*)
function TConfig.getXMLStringValue(path : String) : String;
var
  res : String;
begin
  res := XMLConfig.GetValue(path, '');
  result := res;
end;

function TConfig.getXMLBoolValue(path : String) : Boolean;
var
  res : String;
begin
  result := false;
  res := XMLConfig.GetValue(path, '');
  if (res = 'true') then result := true;
end;

end.

