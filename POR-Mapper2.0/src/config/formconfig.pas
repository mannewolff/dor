(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: formconfig.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit FormConfig;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls;

type

  { TFormConfiguration }

  TFormConfiguration = class(TForm)
    chkDelete: TCheckBox;
    (* -------------------------- Generieroptionen ---------------------------*)
    grpGenerieren    : TGroupBox;
    chkSetter        : TCheckBox;
    chkGetter        : TCheckBox;
    chk1zun          : TCheckBox;
    chknzu1          : TCheckBox;
    (* ---------------------------- Ungruppiert ------------------------------*)
    btnDefault       : TButton;
    btnOK            : TButton;
    Label1: TLabel;
    procedure btnDefaultClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  FormConfiguration: TFormConfiguration;

implementation

uses main;

{ TFormConfiguration }

(* ------------------------ Ereignismethoden  --------------------------------*)
procedure TFormConfiguration.btnOKClick(Sender: TObject);
begin
  Close();
end;

procedure TFormConfiguration.btnDefaultClick(Sender: TObject);
begin
   if chkSetter.Checked = true then
     MainFOrm.properties.put('defaultsetter', 'true') else
     MainFOrm.properties.put('defaultsetter', 'false');

   if chkGetter.Checked = true then
     MainFOrm.properties.put('defaultgetter', 'true') else
     MainFOrm.properties.put('defaultgetter', 'false');

   if chk1zun.Checked = true then
     MainFOrm.properties.put('default1zun', 'true') else
     MainFOrm.properties.put('default1zun', 'false');

   if chknzu1.Checked = true then
     MainFOrm.properties.put('defaultnzu1', 'true') else
     MainFOrm.properties.put('defaultnzu1', 'false');

   if chkDelete.Checked = true then
     MainFOrm.properties.put('detaultdelete', 'true') else
     MainFOrm.properties.put('detaultdelete', 'false');

   MainFOrm.properties.save();
end;

initialization
  {$I formconfig.lrs}

end.

