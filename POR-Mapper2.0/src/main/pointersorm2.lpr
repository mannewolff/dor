program pointersorm2;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main, FormConfig, config, analysedeclarations, analysedeclarationsdata,
  analyseglobals, analyserbasics, analysestatements, analysesystems, cleanup,
  fileloader, processglobal, processInline, beautifier,
  processtemplatedynamisch, fehler, properties, HashMap, strobj,
  command;

//{$IFDEF WINDOWS}{$R pointersorm2.rc}{$ENDIF}

{$R *.res}

begin
  Application.Title:='Pointers OR-Generator 2.0';
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFormConfiguration, FormConfiguration);
  Application.CreateForm(TTErrorForm, TErrorForm);
  Application.Run;
end.

