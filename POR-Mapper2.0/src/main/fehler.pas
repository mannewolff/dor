unit fehler;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls;

type

  { TTErrorForm }

  TTErrorForm = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  TErrorForm: TTErrorForm;

implementation

{ TTErrorForm }

procedure TTErrorForm.Button1Click(Sender: TObject);
begin
  Close();
end;

initialization
  {$I fehler.lrs}

end.

