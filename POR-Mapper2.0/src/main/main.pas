(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: main.pas,v 1.2 2010/02/28 16:55:12 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit main;

{$mode objfpc}
{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics,
  Dialogs, Menus, StdCtrls, EditBtn, ExtCtrls, ComCtrls, config, properties;

type

  { TMainForm }
  TMainForm = class(TForm)
    Bevel1: TBevel;
    chkRelative: TCheckBox;
    chkAllMetadata: TCheckBox;


    (* ---------------------------- Ungruppiert ------------------------------*)
    lblGlobalProjektname : TLabel;
    edtGlobalProjektname : TEdit;
    chklastprojekt       : TCheckBox;
    btnAbrechen          : TButton;
    btnGenerieren        : TButton;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    mnuNeuesProjekt: TMenuItem;
    mnuProjektLaden: TMenuItem;
    mnuProjektSpeichern: TMenuItem;
    mnuBeenden: TMenuItem;
    ProjektOpenDialog: TOpenDialog;
    ProjektSaveDialog: TSaveDialog;
    StatusBar            : TStatusBar;


    (* ------------------------ Dateien / Templates --------------------------*)
    grpFilenames         : TGroupBox;
    lblMetaTemplate      : TLabel;
    edtSteuerdatei      : TFileNameEdit;


    (* ------------------------ Generierte Ereignismethoden ----------------- *)
    procedure btnAbrechenClick(Sender: TObject);
    procedure btnGenerierenClick(Sender: TObject);
    procedure chklastprojektChange(Sender: TObject);
    procedure edtGlobalProjektnameChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure mnuProjektSpeichernClick(Sender: TObject);
    procedure mnuBeendenClick(Sender: TObject);
    procedure mnuKonfigurationClick(Sender: TObject);
    procedure mnuNeuesProjektClick(Sender: TObject);
    procedure mnuProjektLadenClick(Sender: TObject);

  private

    configuration  : TConfig;     // Konfigurationsdatei für das aktuelle Projekt
    mustsave       : Boolean;     // Projekt muss gespeichert werden
    semaphore      : Boolean;     // Semphore für OnChange Ereignisse
    lastProjektFileName : String; // Letzte geladene Projektdatei
    lastSelectedDir     : String; // Zuletzt selektiertes Directory

    procedure anwendungSchliessen();
    function  checkCanLoad(amessage : String) : Boolean;
    procedure checkEnabled();
    function checkValid() : boolean;
    procedure clean();
    procedure loadProjekt();
    function  newConfiguration() : boolean;
    procedure saveProjekt();
    procedure saveProperties();
    procedure setMustSave(value : boolean);
    function makeRelative(const s: String) : String;
  public
    properties     : TProperties; // Interne Programmeinstellungen
  end;

var
  MainForm: TMainForm;

implementation

uses FormConfig, catalogfactory, catalog, command, generatorcontext,
strutils, fehler;

{ TMainForm }
(* -------------------- Konstruktion, Destruktion ----------------------------*)
procedure TMainForm.FormCreate(Sender: TObject);
var
  exepfad  : String;
begin
  semaphore := true;

  // Initialisierung
  clean();
  setMustSave(false);

  // Basispfad finden
  exepfad := ParamStr(0);
  exepfad := ExtractFileDir(exepfad);

  // Properties laden
  properties := TProperties.Create();
  properties.load(exepfad + '\porm.properties');

  // evtl. Projekt automatisch laden
  if properties.get('loadLastProjekt') = 'true' then
  begin
    exepfad := properties.get('lastProjekt');
    if exepfad <> '' then
    begin
      lastProjektFileName := exepfad;
      if chkRelative.checked then
      StatusBar.Panels[2].text := makeRelative(lastProjektFileName) else
      StatusBar.Panels[2].text := lastProjektFileName;
      loadProjekt();
      chklastprojekt.Checked := true;
    end;
  end else
    chklastprojekt.Checked := false;

  semaphore := false;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  // Objekte aufräumen
  if assigned(configuration) then configuration.Free();
  properties.Free();
end;

procedure TMainForm.MenuItem1Click(Sender: TObject);
begin

end;

procedure TMainForm.mnuProjektSpeichernClick(Sender: TObject);
begin
  if not mustSave then exit;
  if lastProjektFileName = '' then
  begin
      if ProjektSaveDialog.Execute() then
      begin
         lastProjektFileName := ProjektSaveDialog.FileName;
      end else
      exit;
  end;
  saveProjekt();
  setMustSave(false);

end;


(* ------------------------ Ereignismethoden  --------------------------------*)
procedure TMainForm.mnuNeuesProjektClick(Sender: TObject);
var
  CanLoad : Boolean;
  default : String;
begin
  CanLoad := CheckCanLoad('Soll trotzdem ein neues Projekt erstellt werden?');

  if CanLoad then
    begin
      saveProperties();
      mustsave := false;
      lastProjektFileName := '';
      //radioProg1.checked := true;
      clean();

      // Defaultwerte für die FormConfig laden
      default := properties.get('defaultsetter');
      if default = 'true' then FormConfiguration.chkSetter.Checked := true else
                               FormConfiguration.chkSetter.Checked := false;
      default := properties.get('defaultgetter');
      if default = 'true' then FormConfiguration.chkGetter.Checked := true else
                               FormConfiguration.chkGetter.Checked := false;
      default := properties.get('default1zun');
      if default = 'true' then FormConfiguration.chk1zun.Checked := true else
                               FormConfiguration.chk1zun.Checked := false;
      default := properties.get('defaultnzu1');
      if default = 'true' then FormConfiguration.chknzu1.Checked := true else
                               FormConfiguration.chknzu1.Checked := false;
      default := properties.get('allmetadata');
      if default = 'true' then chkAllMetadata.Checked := true else
                               chkAllMetadata.Checked := false;
      default := properties.get('detaultdelete');
      if default = 'true' then FormConfiguration.chkDelete.Checked := true else
                               FormConfiguration.chkDelete.Checked := false;
      default := properties.get('absolutepath');
      if default = 'true' then chkRelative.Checked := true else
                               chkRelative.Checked := false;
      properties.save();

  end;
end;

procedure TMainForm.mnuProjektLadenClick(Sender: TObject);
var
  CanLoad : Boolean;
begin
  CanLoad := CheckCanLoad('Soll trotzdem ein neues Projekt geladen werden?');

  if CanLoad then
  if ProjektOpenDialog.Execute() then
  begin
    saveProperties();
    lastProjektFileName := ProjektOpenDialog.FileName;
    StatusBar.Panels[2].text := ExtractFileName(lastProjektFileName);
    loadProjekt();
    saveProperties();
    setMustSave(false);
  end;
end;


procedure TMainForm.mnuKonfigurationClick(Sender: TObject);
begin
  FormConfiguration.ShowModal();
end;

(* Ereignismethode zum schließen der Anwendung *)
procedure TMainForm.mnuBeendenClick(Sender: TObject);
begin
  Close();
end;

(* Ereignismethode wenn Anwendung über Close() geschlossen wird *)
procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  if mustsave then
  begin
    CanClose := false;
    anwendungSchliessen();
  end else
  CanClose := true;
end;

(* Generische Ereignismethode für das setzten des Modify Buttons *)
procedure TMainForm.edtGlobalProjektnameChange(Sender: TObject);
var
  edit : TComponent;
begin
  if semaphore then exit;
  edit := Sender as TComponent;
  if (edit.Name = 'edtTemplateDir') or
     (edit.Name = 'edtUnitDir') or
     (edit.Name = 'edtUnitDir') then
     begin
       lastSelectedDir := TDirectoryEdit(edit).Text;
     end;
  if chkRelative.checked then
  begin
    if edit.Name = 'edtSteuerdatei' then
      edtSteuerdatei.text := makeRelative(edtSteuerdatei.text);
  end;
  if semaphore then exit;
  setMustSave(true);
end;

procedure TMainForm.FormActivate(Sender: TObject);
const activated : Boolean = false;
var value : String;
begin
  if activated then exit;
  semaphore := true;
  activated := true;

  value := properties.get('defaultgetter');

(*
  if value = 'true' then FormConfiguration.chkGetter.Checked := true else
    FormConfiguration.chkGetter.Checked := false;

  value := properties.get('defaultsetter');
  if value = 'true' then FormConfiguration.chkSetter.Checked := true else
    FormConfiguration.chkSetter.Checked := false;

  value := properties.get('default1zun');
  if value = 'true' then FormConfiguration.chk1zun.Checked := true else
    FormConfiguration.chk1zun.Checked := false;

  value := properties.get('defaultnzu1');
  if value = 'true' then FormConfiguration.chknzu1.Checked := true else
    FormConfiguration.chknzu1.Checked := false;

  value := properties.get('detaultdelete');
  if value = 'true' then FormConfiguration.chkDelete.Checked := true else
    FormConfiguration.chkDelete.Checked := false;
*)
  value := properties.get('allmetadata');
  if value = 'true' then chkAllMetadata.Checked := true else
    chkAllMetadata.Checked := false;

  value := properties.get('allmetadata');
  if value = 'true' then chkRelative.Checked := true else
    chkRelative.Checked := false;

    value := properties.get('lastProjekt');
    if value <> '' then
    begin
      if chkRelative.checked then
      StatusBar.Panels[2].text := makeRelative(value) else
      StatusBar.Panels[2].text := value;
      loadProjekt();
    end;
  semaphore := false;
end;

procedure TMainForm.btnAbrechenClick(Sender: TObject);
begin
  Close();
end;

procedure TMainForm.btnGenerierenClick(Sender: TObject);
var
  //formdata   : TFormData;
  //generator  : TORGenerator;
  factory    : TCatalogFactory;
  command    : TCommand;
  catalog    : TCatalog;
  context    : TGeneratorContext;
  pfad       : String;
  I          : Integer;
  error      : TError;
  msg        : String;
  ms         : boolean;
  return     : boolean;
  searchpath : String;
  findpath   : String;
  datei      : TSearchRec;
begin
  if not checkValid() then exit;
  //btnGenerieren.enabled := false;
(*
  if radioProg1.checked then
  begin
  with formdata do
  begin
    database := edtDatabaseName.Text;
    atemplatename := edtUnitTemplate.Text;
    aunitdir := edtUnitDir.Text;
    asqldir := edtSQLDir.Text;
    ametadir := edtTemplateDir.Text;
    ametafile := ExtractFilename(edtSteuerdatei.Text);
    if chkLazarus.Checked then ageneratedelphi := true else
                               ageneratedelphi := false;
    if chkSQL.Checked then ageneratesql := true else
                               ageneratesql := false;
    if FormConfiguration.chkGetter.Checked then agenerateget := true else
                               agenerateget := false;
    if FormConfiguration.chkSetter.Checked then agenerateset := true else
                               agenerateset := false;
    if FormConfiguration.chkDelete.Checked then agendelete := true else
                               agendelete := false;
    if chkAllMetadata.checked then agenerateall := true else agenerateall := false;
  end;
  generator := TORGenerator.Create();
  generator.Execute(formdata);
  generator.Free();
  MessageDlg('Die Generierung ist beendet.',
                mtConfirmation, [mbOK], 0);
  end else
*)
  begin
    pfad := ExtractFilePath(paramstr(0));

    // Katalog laden
    factory := getCatalogFactory();
    factory.loadCatalogData(pfad + 'P-GEN.chain');
    catalog := factory.getCatalog('Default');
    command := catalog.getCommand('PreProcess');
    context := TGeneratorContext.Create();
    context.ctxdefault := edtSteuerdatei.Text;

    command.execute(context);
    FreeAndNil(Context);
    command := catalog.getCommand('all');

    if (chkAllMetadata.Checked) then
    begin
      searchpath := ExtractFileDir(edtSteuerdatei.Text);
      if searchpath[length(searchpath)] <> '\' then
        searchpath := searchpath + '\';
      findpath := searchpath + '*' + ExtractFileExt(edtSteuerdatei.Text);
      If FindFirst(findpath, fadirectory, datei) = 0 then
      begin
        context := TGeneratorContext.Create();
        context.analyser_filename := searchpath + datei.name;
        return := command.execute(context);
        context.Free();
        while Findnext(datei) = 0 do
        begin
          If datei.Attr and fadirectory = 0 then
          begin
            context := TGeneratorContext.Create();
            context.analyser_filename := searchpath + datei.name;
            return := command.execute(context);
            context.Free();
          end;
        end;
        FindClose(datei);
      end;
    end else
    begin
      context := TGeneratorContext.Create();
      context.analyser_filename  := edtSteuerdatei.Text;
      return := command.execute(context);
      TErrorForm.Memo1.Lines.Clear();
      ms := false;
      if context.analyser_errors.Size > 0 then
      begin
        ms := true;
        TErrorForm.Memo1.Lines.Add('Fehler des Analysers');
        TErrorForm.Memo1.Lines.Add('--------------------');
        for I := 0 to context.analyser_errors.Size - 1 do
        begin
          error := TError(context.analyser_errors.GetObject(I));
          msg := error.line;
          TErrorForm.Memo1.Lines.Add(msg);
        end;
      end;
      if context.processor_errors.Size > 0 then
      begin
        ms := true;
        TErrorForm.Memo1.Lines.Add('Fehler des Prozessors');
        TErrorForm.Memo1.Lines.Add('--------------------');
        for I := 0 to context.processor_errors.Size - 1 do
        begin
          error := TError(context.processor_errors.GetObject(I));
          msg := error.line;
          TErrorForm.Memo1.Lines.Add(msg);
        end;
      end;
      if ms then TErrorForm.ShowModal();

      if not return then
      MessageDlg('Die Generierung ist erfolgreich beendet worden.',
                  mtConfirmation, [mbOK], 0);
    end;
  end;
  btnGenerieren.enabled := true;
end;

procedure TMainForm.chklastprojektChange(Sender: TObject);
begin
  if semaphore then exit;
  edtGlobalProjektnameChange(Sender);
  saveProperties();
end;

(* ---------------------- Instanzmethoden private ----------------------------*)

(* Entscheidet, ob eine Anwendung geschlossen werden kann. *)
procedure TMainForm.anwendungSchliessen();
var
  CanClose : Boolean;
begin
  CanClose := CheckCanLoad('Soll die Anwendung trotzdem geschlossen werden?');
  if CanClose then
  begin
    saveProperties();
    setMustSave(false);
    Close();
  end;
end;

(* Entscheidet, ob ein Projekt geladen werden kann *)
function TMainForm.checkCanLoad(amessage : String) : Boolean;
var
  CanLoad : Boolean;
begin
  CanLoad := true;
  if mustsave then
  begin
     if MessageDlg('Das Projekt ist noch nicht gespeichert. ' + amessage,
                   mtConfirmation,
                   mbYesNoCancel,
                   0) <> mrYes then
     CanLoad := false;
  end;
  result := CanLoad;
end;

(* Entscheidet, ob Controls en- bzw. disabled werden müssen. *)
procedure TMainForm.checkEnabled();
begin
  if mustsave then
  Statusbar.Panels[1].Text := 'geändert' else
  Statusbar.Panels[1].Text := '';
end;

(* Entscheidet, ob alle Infos zur Generierung vorhanden sind. *)
function TMainForm.checkValid() : boolean;
var
   res : Boolean;
begin
   res := true;
   if res = true then
   begin
     edtSteuerdatei.Color := clWindow;
   end;
   result := res;
end;

(* Löscht alle Controls, setzt sie auf Default-Werte *)
procedure TMainForm.clean();
begin
  edtGlobalProjektname.Text := '';
  edtSteuerdatei.Text := '';
  //chkLazarus.Checked := false;
  //chkSQL.Checked := false;

  if assigned(configuration) then FreeAndNil(configuration);
end;

(* Läd ein Projekt *)
procedure TMainForm.loadProjekt();
begin
  if assigned(configuration) then FreeAndNil(configuration);
  configuration := TConfig.Create(lastProjektFileName);

  edtGlobalProjektname.Text := configuration.getProjektname();
  edtSteuerdatei.Text := configuration.getMetaDatenFile();
  //chkLazarus.Checked := configuration.getLazarusUnit();
  //chkSQL.Checked := configuration.getSQLScript();
  if configuration.getVersion() = '1.0' then
end;

(* Erzeugt eine leere Konfigurationsdatei *)
function TMainForm.newConfiguration() : boolean;
var
  exepfad : String;
begin
  result := true;
  exepfad := ParamStr(0);
  exepfad := ExtractFileDir(exepfad);

  if not CopyFile(exepfad + '\template.cfg', lastProjektFileName) then
  begin
     MessageDlg('Die Projektdatei konnte nicht angelegt werden.',
                mtError, [mbOK], 0);
     result := false;
  end else
  begin
    configuration := TConfig.create(lastProjektFileName);
  end;
end;

(* Speichert ein Projekt *)
procedure TMainForm.saveProjekt();
var
  configError : Boolean;
begin
  configError := false;
  if configuration = nil then
  begin
    if not (newConfiguration()) then configError := true;
  end;

  if not ConfigError then
  begin
    configuration.setProjektname(edtGlobalProjektname.Text);
    configuration.setMetaDatenFile(edtSteuerdatei.Text);
    //configuration.setLazarusUnit(chkLazarus.Checked);
    //configuration.setSQLScript(chkSQL.Checked);
    configuration.setVersion('2.1');
  end;
end;

(* Speichert Programmeinstellungen *)
procedure TMainForm.saveProperties();
begin
  if chklastprojekt.Checked then
     properties.put('loadLastProjekt', 'true') else
     properties.put('loadLastProjekt', 'false');
  if chkAllMetadata.Checked then
     properties.put('allmetadata', 'true') else
     properties.put('allmetadata', 'false');
  if chkRelative.checked then
     properties.put('absolutepath', 'true') else
     properties.put('absolutepath', 'false');
  properties.put('lastProjekt', lastProjektFileName);
  properties.save();
end;

(* Setzt das mustsave Flag *)
procedure TMainForm.setMustSave(value : boolean);
begin
  mustsave := value;
  checkEnabled();
end;

function TMainForm.makeRelative(const s: String) : String;
var
  p : String;
  r : String;
begin
  r := s;
  p := paramstr(0);
  while p[length(p)] <> '\' do
    p := copy(p, 1, length(p) - 1);
  r := AnsiReplaceStr(r, p, '.\');
  result := r;
end;

initialization
  {$I main.lrs}

end.

