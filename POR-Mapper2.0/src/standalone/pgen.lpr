program pgen;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp,
  command, catalogfactory, context, catalog, generatorcontext
  { you can add units after this };

type

  { TMyApplication }

  TMyApplication = class(TCustomApplication)
  protected
    procedure DoRun; override;
  private
    isall : Boolean;
    fname : String;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TMyApplication }

procedure TMyApplication.DoRun;
var
  ErrorMsg: String;
var
  factory    : TCatalogFactory;
  acommand   : TCommand;
  acatalog   : TCatalog;
  acontext   : TGeneratorContext;
  pfad       : String;
  return     : boolean;
  searchpath : String;
  findpath   : String;
  datei      : TSearchRec;
  list       : TStrings;
  I          : Integer;
begin
  // quick check parameters
  ErrorMsg := CheckOptions('h','help');
  ErrorMsg := CheckOptions('a','all');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h','help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  if HasOption('a','all') then isall := true;
  fname := paramstr(1);

  // Katalog laden
  factory := getCatalogFactory();
  factory.loadCatalogData(pfad + 'P-GEN.chain');
  acatalog := factory.getCatalog('Default');


  acommand := acatalog.getCommand('all');
  if (isall) then
    begin
      searchpath := ExtractFileDir(fname);
      if searchpath[length(searchpath)] <> '\' then
        searchpath := searchpath + '\';
      findpath := searchpath + '*' + ExtractFileExt(fname);
      If FindFirst(findpath, fadirectory, datei) = 0 then
      begin
        acontext := TGeneratorContext.Create();
        acontext.analyser_filename := searchpath + datei.name;
        writeln('processing : ' + searchpath + datei.name);
        return := acommand.execute(acontext);
        acontext.Free();
        while Findnext(datei) = 0 do
        begin
          If datei.Attr and fadirectory = 0 then
          begin
            acontext := TGeneratorContext.Create();
            acontext.analyser_filename := searchpath + datei.name;
            writeln('processing : ' + searchpath + datei.name);
            return := acommand.execute(acontext);
            acontext.Free();
          end;
        end;
        FindClose(datei);
      end;
      sleep(1000);
  end else
  begin
    acontext := TGeneratorContext.Create();
    acontext.analyser_filename  := fname;
    writeln('processing : ' + fname);
    acommand.execute(acontext);
    acontext.Free();
  end;
  // stop program loop
  Terminate;
end;

constructor TMyApplication.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TMyApplication.Destroy;
begin
  inherited Destroy;
end;

procedure TMyApplication.WriteHelp;
begin
  { add your help code here }
  writeln('Usage:  --help -h           Hilfestellung');
  writeln('        Steuerdatei -a      -a Alle Dateien im Pfad generieren');
end;

var
  Application: TMyApplication;

{$IFDEF WINDOWS}{$R pgen.rc}{$ENDIF}

begin
  Application:=TMyApplication.Create(nil);
  Application.Title:='My Application';
  Application.Run;
  Application.Free;
end.

