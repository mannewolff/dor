program fpcunitgenerator;

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, GuiTestRunner, LResources, ptrcalendar, TestCalendar;

{$IFDEF WINDOWS}{$R fpcunitgenerator.rc}{$ENDIF}

begin
  Application.Title:='Pointer OR Mapper';
  {$I fpcunitgenerator.lrs}
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

