unit testtemplate;

uses
    adresse,
    betrieb,
    recht,
  classes;

interface

type
  TTesttemplate = class
  private
    Name : String;
    Vorname : String;
  public
    function getName() : String;
    function getVorname() : String;
    function getAdressenListe() : TList;
    function getRecht() : TRecht;
    function getBetrieb() : TBetrieb;
    procedure first();
    procedure next();
  end;

implementation

function TTesttemplate.getName() : String;
begin
  result := Name;
end;
 
function TTesttemplate.getVorname() : String;
begin
  result := Vorname;
end;
 
procedure TTesttemplate.first();
begin
end;

procedure TTesttemplate.next();
begin
end;

end.
