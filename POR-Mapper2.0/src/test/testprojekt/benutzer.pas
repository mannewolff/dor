(******************************************************************************
 ******************************************************************************
    KOHNEPROGRAMM 2009
    (c) Wilhelm Kohne GmbH & Co Kg 2008-2010
    (c) Pointers - EDV auf den Punkt gebracht 2008-2010

    Realisiert durch:
    Pointers - EDV auf den Punkt gebracht
    http://www.pointers.de
    wolff(at)pointers.de

 ******************************************************************************
    Detail: Generierte Fachklasse @OBJ_NAME

 ******************************************************************************
  CVS/Subversion - Version Information:

  $Log: benutzer.pas,v $
  Revision 1.1.1.1  2010/02/28 12:15:10  manfred
  initial


 ******************************************************************************)

// ACHTUNG: DIESE UNIT WURDE GENERIERT. DESHALB DARF AN DEM QUELLTEXT
// NICHTS GE�NDERT WERDEN. FALLS ZUS�TZLICHE METHODEN NOTWENDIG WERDEN
// BITTE VON DIESER KLASSE ERBEN UND DIE METHODEN IN DER VERERBTEN KLASSE
// IMPLEMENTIEREN.
// Pointers OR - Generator 2.0

unit @UNITS;

interface

uses
_____rechteImpl,
_____adresse,
     SysUtils, Classes, persistenz, SqlExpr, HashMap;

type
   @OBJ_NAME = class(TPersistenz)
     private
      stringList : TStringList;
     public
______username : String;
______vorname : String;
______nachname : String;
______passwort : String;
______freigeschaltet : Integer;
______personalnummer : String;
______firma : Integer;
______anrede : Integer;
______titel : String;
______kuerzel : String;
______abteilung : String;
______funktion : String;
______geburtstag : String;
______kontoinhaber : String;
______kontonummer : String;
______bankleitzahl : String;
______bemerkung : String;
______info : String;
______FK_KONTAKT : Integer;
______FK_MANDATOR : Integer;
______initialized : Integer;
______profil : Integer;
      constructor Create(keep: boolean = false);overload;
      constructor Create(Id : Integer; keep: boolean = false);overload;
      function  getMETA() : TStringList; override;
      function  getTableName() : String; override;
      procedure setParams(bool : boolean); override;
      procedure refreshObject(var aObject: TPersistenz); override;
      function  createObject() : TPersistenz; override;
      procedure setFields(var obj : @OBJ_NAME);
      procedure queryToEditFields(map : TStrHashMap);override;
      // Assoziationen zu anderen Objekten
______function get~adresse() : TList;
______function get~recht() : TRechteImpl;

implementation

// =============================================================================
//                         Methoden von @OBJ_NAME
// =============================================================================

(*********************************************************************
 Constructor
 Baut die Attributliste auf.
 *********************************************************************)
constructor @OBJ_NAME.Create(keep : boolean = false);
begin
  inherited Create(keep);
  stringList := TStringList.Create();
__stringList.Add('username');
__stringList.Add('vorname');
__stringList.Add('nachname');
__stringList.Add('passwort');
__stringList.Add('freigeschaltet');
__stringList.Add('personalnummer');
__stringList.Add('firma');
__stringList.Add('anrede');
__stringList.Add('titel');
__stringList.Add('kuerzel');
__stringList.Add('abteilung');
__stringList.Add('funktion');
__stringList.Add('geburtstag');
__stringList.Add('kontoinhaber');
__stringList.Add('kontonummer');
__stringList.Add('bankleitzahl');
__stringList.Add('bemerkung');
__stringList.Add('info');
__stringList.Add('FK_KONTAKT');
__stringList.Add('FK_MANDATOR');
__stringList.Add('initialized');
__stringList.Add('profil');
end;

(*********************************************************************
 Constructor
 L�d ein Objekt per ID
 *********************************************************************)
constructor @OBJ_NAME.Create(Id : Integer; keep: boolean = false);
begin
  inherited Create(keep);
  loadPerId(id);
end;

(*********************************************************************
 Gibt die META-Daten f�r diese Klasse zur�ck.
 *********************************************************************)
function @OBJ_NAME.getMETA() : TStringList;
begin
  result := stringList;
end;

(*********************************************************************
Gibt den Datenbank-Tabellennamen f�r diese Klasse zur�ck.
 *********************************************************************)
function @OBJ_NAME.getTableName() : String;
begin
  result := '@TABLE_NAME';
end;

(*********************************************************************
Setzt die Werte, die in der Datenbank gespeichert sind in das Objekt.
 *********************************************************************)
procedure @OBJ_NAME.refreshObject(var aObject : TPersistenz);
var
  objects : @OBJ_NAME;
begin
   objects := aObject as @OBJ_NAME;
   setFields(objects);
   inherited refreshObject(aObject);
end;

(*********************************************************************
Setzt die Werte, die in der Datenbank gespeichert sind in das Objekt.
 *********************************************************************)
procedure @OBJ_NAME.setFields(var obj : @OBJ_NAME);
begin
__obj.username := query.Fields.FieldByName('username').AsString;
__obj.vorname := query.Fields.FieldByName('vorname').AsString;
__obj.nachname := query.Fields.FieldByName('nachname').AsString;
__obj.passwort := query.Fields.FieldByName('passwort').AsString;
__obj.freigeschaltet := query.Fields.FieldByName('freigeschaltet').AsInteger;
__obj.personalnummer := query.Fields.FieldByName('personalnummer').AsString;
__obj.firma := query.Fields.FieldByName('firma').AsInteger;
__obj.anrede := query.Fields.FieldByName('anrede').AsInteger;
__obj.titel := query.Fields.FieldByName('titel').AsString;
__obj.kuerzel := query.Fields.FieldByName('kuerzel').AsString;
__obj.abteilung := query.Fields.FieldByName('abteilung').AsString;
__obj.funktion := query.Fields.FieldByName('funktion').AsString;
__obj.geburtstag := query.Fields.FieldByName('geburtstag').AsString;
__obj.kontoinhaber := query.Fields.FieldByName('kontoinhaber').AsString;
__obj.kontonummer := query.Fields.FieldByName('kontonummer').AsString;
__obj.bankleitzahl := query.Fields.FieldByName('bankleitzahl').AsString;
__obj.bemerkung := query.Fields.FieldByName('bemerkung').AsString;
__obj.info := query.Fields.FieldByName('info').AsString;
__obj.FK_KONTAKT := query.Fields.FieldByName('FK_KONTAKT').AsInteger;
__obj.FK_MANDATOR := query.Fields.FieldByName('FK_MANDATOR').AsInteger;
__obj.initialized := query.Fields.FieldByName('initialized').AsInteger;
__obj.profil := query.Fields.FieldByName('profil').AsInteger;
end;

(*********************************************************************
Setzt die Werte, aus dem Objekt in die Datenbank.
 *********************************************************************)
procedure @OBJ_NAME.setParams(bool : boolean);
begin
  paramInt := 0;
__query.Params[paramInt].AsString := username; inc(paramInt);
__query.Params[paramInt].AsString := vorname; inc(paramInt);
__query.Params[paramInt].AsString := nachname; inc(paramInt);
__query.Params[paramInt].AsString := passwort; inc(paramInt);
__query.Params[paramInt].AsInteger := freigeschaltet; inc(paramInt);
__query.Params[paramInt].AsString := personalnummer; inc(paramInt);
__query.Params[paramInt].AsInteger := firma; inc(paramInt);
__query.Params[paramInt].AsInteger := anrede; inc(paramInt);
__query.Params[paramInt].AsString := titel; inc(paramInt);
__query.Params[paramInt].AsString := kuerzel; inc(paramInt);
__query.Params[paramInt].AsString := abteilung; inc(paramInt);
__query.Params[paramInt].AsString := funktion; inc(paramInt);
__query.Params[paramInt].AsString := geburtstag; inc(paramInt);
__query.Params[paramInt].AsString := kontoinhaber; inc(paramInt);
__query.Params[paramInt].AsString := kontonummer; inc(paramInt);
__query.Params[paramInt].AsString := bankleitzahl; inc(paramInt);
__query.Params[paramInt].AsString := bemerkung; inc(paramInt);
__query.Params[paramInt].AsString := info; inc(paramInt);
__query.Params[paramInt].AsInteger := FK_KONTAKT; inc(paramInt);
__query.Params[paramInt].AsInteger := FK_MANDATOR; inc(paramInt);
__query.Params[paramInt].AsInteger := initialized; inc(paramInt);
__query.Params[paramInt].AsInteger := profil; inc(paramInt);
  inherited setParams(bool);
end;

(*********************************************************************
Erzeugt ein Objekt vom Typ @OBJ_NAME.
 *********************************************************************)
function @OBJ_NAME.createObject() : TPersistenz;
var
   obj : @OBJ_NAME;
begin
  obj := @OBJ_NAME.Create();
  result := obj;
end;

(*********************************************************************
Setzt Werte aus der Query direkt in View-Editfields
 *********************************************************************)
procedure @OBJ_NAME.queryToEditFields(map : TStrHashMap);
begin
  inherited queryToEditFields(map);
end;

(*********************************************************************
 Generierte 1:1 Zugriffsmethode (wenn vorhanden)
 *********************************************************************)
function @OBJ_NAME.get~recht() : TRechteImpl;
__var
____recht : TRechteImpl;
begin
__recht := TRechteImpl.Create();
__recht.loadPerFilter('WHERE FK_USER = ' + IntToStr(id));
__result := recht;
end;

(*********************************************************************
 Generierte 1:1 L�schmethode (wenn vorhanden)
 *********************************************************************)
procedure @OBJ_NAME.delete~recht;
var
__recht : TRechteImpl;
begin
__recht := get~recht();
__recht.delete;
end;

(*********************************************************************
 Generierte 1:N Zugriffsmethode (wenn vorhanden)
 *********************************************************************)
function @OBJ_NAME.get~adresseList() : TList;
var
__adresse : TAdresse;
__list : TList;
begin
__adresse := TAdresse.Create();
__list := adresse.loadListPerFilter(format('FK_USER= %d', [self.id]));
__result := list;
end;

(*********************************************************************
 Generierte 1:1 (N:1) Revert Zugriffsmethode (wenn vorhanden)
 *********************************************************************)

(*********************************************************************
 Generierte 1:1 (N:1) Revert L�schmethode (wenn vorhanden)
 *********************************************************************)

end.
