unit testmeta;

uses
    adresse,
    betrieb,
    recht,
  classes;

interface

type
  TTestmeta = class
  private
    Name : String;
    Vorname : String;
  public
    function getName() : String;
    function getVorname() : String;
    function getAdressenListe() : TList;
    function getRecht() : TRecht;
    function getBetrieb() : TBetrieb;
    procedure first();
    procedure next();
  end;

implementation

function TTestmeta.getName() : String;
begin
  result := Name;
end;
 
function TTestmeta.getVorname() : String;
begin
  result := Vorname;
end;
 
procedure TTestmeta.first();
begin
end;

procedure TTestmeta.next();
begin
end;

end.
