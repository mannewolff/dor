unit @UNITS;

interface

type
  @OBJ_NAME = class
  private
    @Attributlist
  public
    @Attributgetter
    procedure first();
    procedure next();
  end;

implementation

@Attributgetterimpl
procedure @OBJ_NAME.first();
begin
end;

procedure @OBJ_NAME.next();
begin
end;

end.
