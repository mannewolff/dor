(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalyserdeclarationsdata.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyserdeclarationsdata;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyserDeclarationsData= class(TTestCase)
  published
    // TAnalyser
    procedure TestAnalyserPrepareStatements();
  end;

implementation

uses analyserobjects, analysedeclarations, analysedeclarationsdata, Vector,
generatorContext, analyserbasics;


procedure TTestAnalyserDeclarationsData.TestAnalyserPrepareStatements();
var
  declarations : TAnalyseDeclarations;
  declarationsdata : TAnalyseDeclarationsData;
  basics : TAnalyserBasics;
  ctx : TGeneratorContext;
begin
  ctx := TGeneratorContext.Create();
  ctx.analyser_filename := 'Z:\POR-Mapper2.0\templates_2.0\kohne.metax';


  basics := TAnalyserBasics.Create();
  basics.Execute(ctx);
  FreeAndNil(basics);

  declarations := TAnalyseDeclarations.Create();
  declarations.execute(ctx);
  declarations.Free();

  declarationsdata := TAnalyseDeclarationsData.Create();
  declarationsdata.execute(ctx);
  declarations.Free();

end;

initialization
  RegisterTest(TTestAnalyserDeclarationsData);
end.

