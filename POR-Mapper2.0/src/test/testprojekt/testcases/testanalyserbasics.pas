(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalyserbasics.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyserbasics;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyserBasics= class(TTestCase)
  published
    // TAnalyser
    procedure TestExecute();

    // Analysertools
    procedure TestAnalysertoolsLoadfile();
    procedure TestAnalysertoolsTokenize();
    procedure TestAnalysertoolsExtractTokenIntoBraces();
  end;

implementation

uses AnalyserBasics, AnalyserTools, Hashmap, Properties, Vector,
     generatorContext;

procedure TTestAnalyserBasics.TestExecute();
var
  context   : TGeneratorContext;
  analyser  : TAnalyserBasics;
  fname     : String;
  comment   : Char;
begin
  fname := ExtractFileDir(paramstr(0)) + '\metainformation\metafile.meta';

  context := TGeneratorContext.Create();
  context.analyser_filename := fname;

  analyser := TAnalyserBasics.Create();
  analyser.execute(context);
  analyser.Free();

  // Kommentar muss
  comment := context.global_comment;
  AssertEquals(comment, '#');

  // Whitespace muss
  comment := context.global_whitespace;
  AssertEquals(comment, '_');

  // Test von FindValue
  fname := findValue(context, '@rootdier');
  AssertEquals(fname, 'C:\');

  // Test von @rootdir
  fname := context.global_rootpath;
  AssertEquals(fname, ExtractFileDir(paramstr(0)) + '\');

  FreeAndNil(context);
end;

(*
 Lad die Datei metafile.meta, in der die Datei gensetter.inc inkludiert ist.
 Vergleicht sie mit der Datei expectedresult.meta, in der das erwartete
 Ergebnis gespeichert ist.
*)
procedure TTestAnalyserBasics.TestAnalysertoolsLoadfile();
var
  fname    : String;
  return   : TStrVector;
  I        : Integer;
  list     : TStringList;
  listexp  : TStringList;
  exp, expe : String;
  prop      : TProperties;
  value     : String;
begin
  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');
  value := prop.get('metafile');
  fname := value;

  return := loadfile(fname, nil, '#');
  if return.size = 0 then
    Fail('Datei wurde nicht geladen');
  list := TStringList.Create();
  for I := 0 to return.Size - 1 do
  begin
    fname := return.GetString(I);
    if fname <> '' then
    list.Add(fname);
  end;

  listexp := TStringList.Create();
  value := prop.get('expectedresult');
  listexp.LoadFromFile(value);

  AssertEquals(list.Count, listexp.Count);
  For I := 0 to list.Count -1 do
  begin
    exp  := list.Strings[I];
    expe := listexp.Strings[I];
    AssertEquals(exp, expe);
  end;

  prop.Free;
  list.Free;
  listexp.Free;
  return.Free;
end;

(*
 Testet den Tokenizer mit verschiedenen Konfigurationen.
*)
procedure TTestAnalyserBasics.TestAnalysertoolsTokenize();
var
  inStr  : String;
  outStr : String;
  outvec : TStrVector;
begin
  // Leerer String
  inStr := '';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 0);
  FreeAndNil(outvec);

  // String mit Länge 1
  inStr := 'EinToken';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 1);
  outStr := outvec.GetString(0);
  AssertEquals('EinToken', outStr);
  FreeAndNil(outvec);

  inStr := 'EinToken:';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 1);
  outStr := outvec.GetString(0);
  AssertEquals('EinToken', outStr);
  FreeAndNil(outvec);

  inStr := ':EinToken';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 2);
  outStr := outvec.GetString(1);
  AssertEquals('EinToken', outStr);
  FreeAndNil(outvec);

  inStr := ':EinToken:';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 2);
  outStr := outvec.GetString(1);
  AssertEquals('EinToken', outStr);
  FreeAndNil(outvec);

  inStr := 'Auch ein Token';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 1);
  outStr := outvec.GetString(0);
  AssertEquals('Auch ein Token', outStr);
  FreeAndNil(outvec);

  // String mit Länge n (3);
  inStr := 'EinToken#ZweiToken#DreiToken';
  outvec := tokenize(inStr, '#');
  AssertTrue(outvec.Size = 3);
  outStr := outvec.GetString(0);
  AssertEquals('EinToken', outStr);
  FreeAndNil(outvec);

  inStr := 'EinToken ZweiToken DreiToken ';
  outvec := tokenize(inStr, ' ');
  AssertTrue(outvec.Size = 3);
  outStr := outvec.GetString(1);
  AssertEquals('ZweiToken', outStr);
  FreeAndNil(outvec);

  inStr := ':EinToken:ZweiToken:DreiToken';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 4);
  outStr := outvec.GetString(3);
  AssertEquals('DreiToken', outStr);
  FreeAndNil(outvec);

  inStr := ':EinToken:ZweiToken:DreiToken:';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 4);
  outStr := outvec.GetString(2);
  AssertEquals('ZweiToken', outStr);
  FreeAndNil(outvec);

  inStr := 'Ein Token:Zwei Token:Drei Token ganze Token';
  outvec := tokenize(inStr, ':');
  AssertTrue(outvec.Size = 3);
  outStr := outvec.GetString(2);
  AssertEquals('Drei Token ganze Token', outStr);
  FreeAndNil(outvec);

end;

procedure TTestAnalyserBasics.TestAnalysertoolsExtractTokenIntoBraces();
var
  teststring,
  resstring   : String;
begin
  teststring := 'Test Braces[Inside] extract.';
  resstring  := extractTokenIntoBraces(teststring, 1, '[', ']');
  assertequals(resstring, 'Inside');
end;

initialization
  RegisterTest(TTestAnalyserBasics);
end.

