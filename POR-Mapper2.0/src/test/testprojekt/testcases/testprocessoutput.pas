(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testprocessoutput.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testprocessOutput;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestProcessOutput = class(TTestCase)
  published
    procedure TestprocessOutput();
  end;

implementation

uses properties, processglobal, generatorContext, fileloader, processIndentifiers, processOutput;

procedure TTestProcessOutput.TestprocessOutput();
var
  processfileloadcommand: TFileLoader;
  processoutput: TProcessOutput;
  prop:     TProperties;
  context:  TGeneratorContext;
  templatefile: string;
  list:     TStringList;
  processidentcommand   : TProcessIdentifiers;
  metafile : String;

begin
  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');

  metafile := prop.get('metaprocessglobal');
  context := TGeneratorContext.Create();

  processfileloadcommand := TFileloader.Create();
  templatefile := prop.get('templateprocessglobal');
  context.global_template[1] := templatefile;
  processfileloadcommand.Execute(context);
  processfileloadcommand.Free();

  context.global_outfile[1] := 'Z:\POR-Mapper2.0\src\test\testprojekt\metainformation\templateprocessglobal.pas';
  processoutput := TProcessOutput.Create();
  processoutput.Execute(context);
  processoutput.Free();

  list := TStringList.Create();
  list.LoadFromFile('Z:\POR-Mapper2.0\src\test\testprojekt\metainformation\templateprocessglobal.pas');
  AssertEquals(list.Count, 26);

end;



initialization

  RegisterTest(TTestProcessOutput);
end.

