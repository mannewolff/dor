(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testfileloader.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testfileloader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestFileloader = class(TTestCase)
  published
    procedure TestFileloader();
  end;

implementation

uses properties, fileloader, Vector, generatorContext;


procedure TTestFileloader.TestFileloader();
var
  fileloadCommand : TFileLoader;
  output          : TStrVector;
  prop            : TProperties;
  templatefile    : String;
  context         : TGeneratorContext;
begin
  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');

  context := TGeneratorContext.Create();
  templatefile := prop.get('templateprocessglobal');
  context.global_template[1] := templatefile;

  fileloadCommand := TFileLoader.Create();
  fileloadCommand.execute(context);
  fileloadCommand.Free();

  output := context.global_workingmap[1];

  AssertEquals(output.Size, 26);
end;



initialization

  RegisterTest(TTestFileloader);
end.

