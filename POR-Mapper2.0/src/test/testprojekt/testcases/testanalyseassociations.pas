(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalyseassociations.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyseassociations;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyseAssociations= class(TTestCase)
  published
    procedure TestAnalyserPrepareAssociations();
  end;

implementation

uses analyseassociations, analyserobjects, Vector;

procedure TTestAnalyseAssociations.TestAnalyserPrepareAssociations();
var
  analyser  : TAnalyseAssociations;
  map       : TVector;
  s         : String;
  attr      : TAssociation;
begin
  analyser := TAnalyseAssociations.Create();
  map := TVector.Create();

  s := '1zuN:AdresseList:TAdresse:FK_PERSON:PersonZuAdressen:adresse';
  analyser.prepareAssociations(s, map);

  attr := TAssociation(map.Items[0]);
  AssertEquals(attr.Typ, '1zuN');
  AssertEquals(attr.Objekt, 'TAdresse');
  AssertEquals(attr.Comment, 'PersonZuAdressen');
  AssertEquals(attr.ForeignKey, 'FK_PERSON');
  AssertEquals(attr.Identifier, 'AdresseList');
  AssertEquals(attr.Packages, 'adresse');

  FreeAndNil(map);
  FreeAndNil(analyser);

end;

initialization
  RegisterTest(TTestAnalyseAssociations);
end.

