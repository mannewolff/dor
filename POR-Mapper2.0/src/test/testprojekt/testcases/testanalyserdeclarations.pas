(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalyserdeclarations.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyserdeclarations;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyserDeclarations= class(TTestCase)
  published
    // TAnalyser
    procedure TestAnalyserPrepareStatements();
  end;

implementation

uses analyserobjects, analysedeclarations, Vector;


procedure TTestAnalyserDeclarations.TestAnalyserPrepareStatements();
var
  analyser  : TAnalyseDeclarations;
  map       : TVector;
  s         : String;
  item      : TDeclaration;
  data      : TStrVector;
begin

  analyser := TAnalyseDeclarations.Create();
  map := TVector.Create();
  s := '@define @Attribute {*attributes}';
  analyser.prepareDefines(s, map);
  s := '@Identifier';
  analyser.prepareDefines(s, map);
  s := '@Typ';
  analyser.prepareDefines(s, map);
  s := ' @Subtyp ';
  analyser.prepareDefines(s, map);
  s := '@DBTyp';
  analyser.prepareDefines(s, map);
  s := '@Additional';
  analyser.prepareDefines(s, map);
  s := '@Comment';
  analyser.prepareDefines(s , map);
  s := '@End';
  analyser.prepareDefines(s , map);

  s := '@define @Association {*associations}';
  analyser.prepareDefines(s, map);
  s := '@Typ';
  analyser.prepareDefines(s, map);
  s := '@Identifier';
  analyser.prepareDefines(s, map);
  s := ' @Objekt ';
  analyser.prepareDefines(s, map);
  s := '@ForeignKey';
  analyser.prepareDefines(s, map);
  s := '@Comment';
  analyser.prepareDefines(s, map);
  s := '@Packages';
  analyser.prepareDefines(s , map);
  s := '@End';
  analyser.prepareDefines(s , map);

  AssertEquals(map.Size, 2);
  item := TDeclaration(map.items[0]);
  AssertEquals(item.Itterable, '@Attribute');
  AssertEquals(item.Section, '*attributes');
  data := item.data;

  AssertEquals(data.Size, 6);
  AssertEquals(data.GetString(0), '@Identifier');
  AssertEquals(data.GetString(1), '@Typ');
  AssertEquals(data.GetString(2), '@Subtyp');
  AssertEquals(data.GetString(3), '@DBTyp');

  FreeAndNil(analyser);
end;

initialization
  RegisterTest(TTestAnalyserDeclarations);
end.

