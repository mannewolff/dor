(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testalles.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testalles;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTesteAlles= class(TTestCase)
  published
    procedure TestAlles;
  end; 

implementation

uses catalogfactory, catalog, command, generatorContext, analysertools;

procedure TTesteAlles.TestAlles;
var
  factory : TCatalogFactory;
  command : TCommand;
  catalog : TCatalog;
  context : TGeneratorContext;
  pfad    : String;
begin
  pfad := ExtractFilePath(paramstr(0));

  // Katalog laden
  factory := getCatalogFactory();
  factory.loadCatalogData(pfad + 'testeAllesChain.chain');
  catalog := factory.getCatalog('Default');

  // Context preparieren
  context := TGeneratorContext.Create();
  context.analyser_filename  := pfad + 'testmeta.meta';
  context.global_template[1] := pfad + 'testtemplate.meta';

  command := catalog.getConmmand('all');
  command.execute(context);

  printStrVector(context.global_workingmap[1], 'C:\hallo.txt');

  // auswerten
end;



initialization

  RegisterTest(TTesteAlles);
end.

