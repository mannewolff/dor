(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalyseattributes.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyseattributes;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyseAttributes= class(TTestCase)
  published
    procedure TestAnalyserPrepareAttributes();
  end;

implementation

uses AnalyseAttributes, analyserobjects, Vector;

procedure TTestAnalyseAttributes.TestAnalyserPrepareAttributes();
var
  analyser  : TAnalyseAttributes;
  map       : TVector;
  s         : String;
  attr      : TAttribut;
begin
  analyser := TAnalyseAttributes.Create();
  map := TVector.Create();

  s := 'Name:String:AsString:VarChar(50):default 0:Der Name';
  analyser.prepareAttributes(s, map);

  attr := TAttribut(map.Items[0]);
  AssertEquals(attr.Identifier, 'Name');
  AssertEquals(attr.Typ, 'String');
  AssertEquals(attr.Subtyp, 'AsString');
  AssertEquals(attr.DBTyp, 'VarChar(50)');
  AssertEquals(attr.Additional, 'default 0');
  AssertEquals(attr.Comment, 'Der Name');

  FreeAndNil(map);
  FreeAndNil(analyser);

end;

initialization
  RegisterTest(TTestAnalyseAttributes);
end.

