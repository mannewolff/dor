(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalyserstatements.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyserstatements;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyserStatements= class(TTestCase)
  published
    // TAnalyser
    procedure TestAnalyserPrepareStatements();
  end;

implementation

uses analyserobjects, analysestatements, Vector;



procedure TTestAnalyserStatements.TestAnalyserPrepareStatements();
var
  analyser  : TAnalyserStatements;
  map       : TVector;
  s         : String;
  statement : TTemplateDefinition;
  statements : TStrVector;
begin
  analyser := TAnalyserStatements.Create();
  map := TVector.Create();
  analyser.actStatementState := 100;
  s := '@Attributlist = ';
  analyser.prepareStatements(s, map);
  statement := TTemplateDefinition(map.items[0]);
  s := statement.TemplateName;
  AssertEquals('@Attributlist', s);


  s := '@foreach @Attribute @do';
  analyser.prepareStatements(s, map);
  s := statement.Control;
  AssertEquals('@foreach', s);
  s := statement.Liste;
  AssertEquals('@Attribute', s);

  s := '__  @Attribute.Name : @Attribute.Typ;';
  analyser.prepareStatements(s, map);
  s := '__  @Attribute.Vorname : @Attribute.Typ;';
  analyser.prepareStatements(s, map);
  s := '@End';
  analyser.prepareStatements(s, map);

  // Auswerten
  statements := statement.Statements;
  s := statements.items[0];
  AssertEquals('__  @Attribute.Name : @Attribute.Typ;', s);
  s := statements.items[1];
  AssertEquals('__  @Attribute.Vorname : @Attribute.Typ;', s);

  FreeAndNil(analyser);
end;

initialization
  RegisterTest(TTestAnalyserStatements);
end.

