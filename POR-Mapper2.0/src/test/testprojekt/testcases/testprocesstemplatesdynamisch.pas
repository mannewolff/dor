(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testprocesstemplatesdynamisch.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testprocesstemplatesdynamisch;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestProcessTemplateDynamic= class(TTestCase)
  published
    procedure TestprocessTemplates();
  end;

implementation

uses analyserbasics, processtemplateDynamisch, Vector,
     generatorcontext, analysedeclarations, analysedeclarationsdata,
     fileloader, analysestatements, processOutput;

procedure TTestProcessTemplateDynamic.TestprocessTemplates;
var
  context                : TGeneratorContext;
  basics                 : TAnalyserBasics;
  declarations           : TAnalyseDeclarations;
  declarationsdata       : TAnalyseDeclarationsData;
  templates              : TProcessTemplateDynamic;
  floader                : TFileLoader;
  statements             : TAnalyserStatements;
  poutput                : TProcessOutput;
begin
  context := TGeneratorContext.Create();
  context.analyser_filename := 'Z:\POR-Mapper2.0\templates_2.0\benutzer.metax';
  context.global_template[1] := 'Z:\POR-Mapper2.0\templates_2.0\kohnetemplate.metax';
  context.global_outfile[1] := 'benutzer.pas';

  basics := TAnalyserBasics.Create();
  basics.Execute(context);
  FreeAndNil(basics);

  declarations := TAnalyseDeclarations.Create();
  declarations.execute(context);
  declarations.Free();

  declarationsdata := TAnalyseDeclarationsData.Create();
  declarationsdata.execute(context);
  declarations.Free();

  floader := TFileLoader.Create();
  floader.execute(context);
  floader.free();

  statements := TAnalyserStatements.Create();
  statements.Execute(context);
  statements.Free();

  templates := TProcessTemplateDynamic.Create();
  templates.execute(context);
  templates.Free();

  poutput := TProcessOutput.Create();
  poutput.execute(context);
  poutput.Free();

end;



initialization
  RegisterTest(TTestProcessTemplateDynamic);
end.

