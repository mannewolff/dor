(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testcleanup.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testcleanup;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestCleanup = class(TTestCase)
  published
    procedure TestCleanup();
  end;

implementation

uses analyserbasics, properties, cleanup, fileloader, Vector, generatorContext,
     analysertools;


procedure TTestCleanup.TestCleanup();
var
  fileloadCommand : TFileloader;
  cleanupCommand  : TCleanup;
  analysercommand : TAnalyserBasics;
  prop            : TProperties;
  context         : TGeneratorContext;
  item            : String;
  output          : TStrVector;
begin
  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');

  context := TGeneratorContext.Create();

  analysercommand := TAnalyserBasics.Create();
  context.analyser_filename := prop.get('metaprocessglobal');
  analysercommand.execute(context);
  analysercommand.Free();

  context.global_template[1] := prop.get('templatewithwhitespaces');
  fileloadCommand := TFileLoader.Create();
  fileloadCommand.execute(context);
  fileloadCommand.Free();

  cleanupCommand := TCleanup.Create();
  cleanupCommand.execute(context);
  cleanupCommand.Free();

  output := context.global_workingmap[1];
  item := output.items[0];
  AssertEquals(item, '  unit @UNITS;');
  item := output.items[14];
  AssertEquals(item, 'ImplemenTation');

  AssertEquals(output.Size, 26);
end;



initialization

  RegisterTest(TTestCleanup);
end.

