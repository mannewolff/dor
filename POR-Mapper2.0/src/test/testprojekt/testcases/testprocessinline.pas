(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testprocessinline.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testprocessInline;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestProcessInline= class(TTestCase)
  published
    procedure TestprocessInline();
  end;

implementation

uses AnalyserBasics,
     Catalog,
     CatalogFactory,
     Command,
     GeneratorContext,
     ProcessInline,
     ProcessOutput;

procedure TTestProcessInline.TestprocessInline;
var
  context                : TGeneratorContext;
  poutput                : TProcessOutput;
  inlines                : TProcessInline;
  pfad                   : String;
  factory                : TCatalogFactory;
  catalog                : TCatalog;
  command                : TCommand;
begin
  context := TGeneratorContext.Create();
  pfad := ExtractFilePath(paramstr(0));
  context.analyser_filename := pfad + '..\..\..\java.templates\user.metax';
  context.global_template[1] := pfad + '..\..\..\java.templates\beantemplate.java';

  // Katalog laden
  factory := getCatalogFactory();
  factory.loadCatalogData(pfad + 'testeInlineChain.chain');
  catalog := factory.getCatalog('Default');

  command := catalog.getConmmand('all');
  command.execute(context);

  inlines := TProcessInline.Create();
  inlines.execute(context);
  inlines.Free();

  poutput := TProcessOutput.Create();
  poutput.execute(context);
  poutput.Free();

  context.Free();
end;

initialization
  RegisterTest(TTestProcessInline);
end.

