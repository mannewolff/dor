(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testanalysesystems.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalysesystems;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyseSystems= class(TTestCase)
  published
    // TAnalyser
    procedure TestAnalyserPrepareSystems();
  end;

implementation

uses AnalyseSystems, Hashmap, strobj;

procedure TTestAnalyseSystems.TestAnalyserPrepareSystems();
var
  s         : String;
  v         : String;
  analyser  : TAnalyseSystems;
  map       : TStrHashmap;
begin
  analyser := TAnalyseSystems.Create();
  map := TStrHashmap.Create();

  s := '@fname = hallo.pas';
  analyser.prepareSystems(s, map);
  v := TString(map.GetValue('@fname')).getString();
  AssertEquals(v, 'hallo.pas');

  s := '@output : hallo.pas : nirwana';
  analyser.prepareSystems(s, map);
  v := TString(map.GetValue('@output')).getString();
  AssertEquals(v, 'hallo.pas');

  FreeAndNil(map);
  FreeAndNil(analyser);
end;




initialization
  RegisterTest(TTestAnalyseSystems);
end.

