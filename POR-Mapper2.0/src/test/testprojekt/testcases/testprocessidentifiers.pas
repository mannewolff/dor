(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testprocessidentifiers.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testprocessIdentifiers;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestProcessIdentifiers = class(TTestCase)
  published
    procedure TestprocessIdentifiers();
  end;

implementation

uses properties, processglobal, generatorContext, fileloader,
     processIndentifiers, analysesystems, analyseglobals,
     analysertools, strobj;


procedure TTestProcessIdentifiers.TestprocessIdentifiers();
var
  identcommand          : TProcessIdentifiers;
  prop                  : TProperties;
  context               : TGeneratorContext;
  fileloader            : TFileLoader;
  analysersystems       : TAnalyseSystems;
  analyserglobals       : TAnalyseGlobals;
  outstr                : String;
begin
  context := TGeneratorContext.Create();

  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');

  context := TGeneratorContext.Create();
  context.analyser_filename := prop.get('metaprocessglobal');
  context.global_template[1] := prop.get('templateprocessglobal');

  analysersystems := TAnalyseSystems.Create();
  analysersystems.execute(context);
  analysersystems.Free();

  analyserglobals := TAnalyseGlobals.Create();
  analyserglobals.execute(context);
  analyserglobals.Free();

  fileloader := TFileLoader.Create();;
  fileloader.execute(context);
  fileloader.Free();

  context.global_template[1] := 'C:\templates\person.meta';
  context.global_template[5] := 'C:\ein\pfad\hallo.txt';

  identcommand := TProcessIdentifiers.Create();
  identcommand.execute(context);
  identcommand.Free();

  AssertEquals(context.global_outfile[1], 'C:\templates\person.pas');
  AssertEquals(context.global_filename[1], 'person');
  AssertEquals(context.global_filepath[1], 'C:\templates\');
  AssertEquals(context.global_fileext[1], '.meta');
  AssertEquals(context.global_outfile[5], 'C:\ein\pfad\hallo.xxx');
  AssertEquals(context.global_filename[5], 'hallo');
  AssertEquals(context.global_filepath[5], 'C:\ein\pfad\');
  AssertEquals(context.global_fileext[5], '.txt');
  AssertEquals(context.isStatus('ProcessIdentifiers'), true);

  outstr := TString(context.analyser_globals.GetValue('@UNITS')).getString();
  AssertEquals(outstr, 'person');
  outstr := TString(context.analyser_globals.GetValue('@OBJ_NAME')).getString();
  AssertEquals(outstr, 'T~person');
  outstr := TString(context.analyser_globals.GetValue('@TABLE_NAME')).getString();
  AssertEquals(outstr, 'hallo');
end;



initialization

  RegisterTest(TTestProcessIdentifiers);
end.

