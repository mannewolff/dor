(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testprocesstemplates.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testprocesstemplates;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestProcessTemplate= class(TTestCase)
  published
    procedure TestprocessTemplates();
  end;

implementation

uses analyserbasics, fileloader, processtemplate, properties, Vector,
     generatorcontext, analysestatements, analyseattributes;

procedure TTestProcessTemplate.TestprocessTemplates;
var
  analyserstatements     : TAnalyserStatements;
  processfileloadcommand : TFileloader;
  processtemplatecommand : TProcessTemplate;
  processattrcommand     : TAnalyseAttributes;
  prop                   : TProperties;
  context                : TGeneratorContext;
  metafile               : String;
  templatefile           : String;
  expectedoutput         : String;
  list                   : TStringList;
  listexp                : TStrVector;
  I                      : Integer;
  exp, expe              : String;
begin
  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');

  metafile := prop.get('metaprocessglobal');
  context := TGeneratorContext.Create();
  context.analyser_filename := metafile;

  analyserstatements := TAnalyserStatements.Create();
  analyserstatements.execute(context);
  analyserstatements.Free();

  templatefile := prop.get('templateprocessglobal');
  context.global_template[1] := templatefile;

  processattrcommand := TAnalyseAttributes.Create();
  processattrcommand.execute(context);
  processattrcommand.Free();

  processfileloadcommand := TFileLoader.Create();;
  processfileloadcommand.execute(context);
  processfileloadcommand.Free();

  processtemplatecommand  := TProcessTemplate.Create();
  processtemplatecommand.execute(context);
  processtemplatecommand.Free();

  expectedoutput := prop.get('expectedtemplateprocesstemplate');
  prop.Free();

  list := TStringList.Create();
  list.LoadFromFile(expectedoutput);
  listexp := context.global_workingmap[1];

  AssertEquals(list.Count, listexp.Size);
  For I := 0 to list.Count -1 do
  begin
    exp  := list.Strings[I];
    expe := listexp.Items[I];
    AssertEquals(exp, expe);
  end;
end;



initialization
  RegisterTest(TTestProcessTemplate); 
end.

