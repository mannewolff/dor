unit TestCalendar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry; 

type

  TTestCalendar= class(TTestCase)
  published
    procedure TestTimeStrToIntern;
    procedure TestDateStrToIntern;
    procedure TestDateDiff;
    procedure TestTimeDiff;
    procedure TestTimeDiffStr;
  end;

implementation

uses ptrcalendar;

procedure TTestCalendar.TestTimeStrToIntern;
var
  c : TPtrCalendar;
  i : LongInt;
  s : String;
begin
  c := TPtrCalendar.Create;

  i := c.TimeStrToIntern('15:13');
  s := c.InternToTimeStr(i);
  AssertEquals(s, '15:13');

  i := c.TimeStrToIntern('0:0');
  s := c.InternToTimeStr(i);
  AssertEquals(s, '00:00');

  i := c.TimeStrToIntern('00:1');
  s := c.InternToTimeStr(i);
  AssertEquals(s, '00:01');

  i := c.TimeStrToIntern('23:1');
  s := c.InternToTimeStr(i);
  AssertEquals(s, '23:01');

  i := c.TimeStrToIntern('23:59');
  s := c.InternToTimeStr(i);
  AssertEquals(s, '23:59');
end;

procedure TTestCalendar.TestDateStrToIntern;
var
  c : TPtrCalendar;
  i : Double;
  s : String;
begin
  c := TPtrCalendar.Create;

  i := c.DateStrToIntern('12.12.2010');
  s := c.InternToDateStr(i);
  AssertEquals(s, '12.12.2010');

  i := c.DateStrToIntern('1.5.2001');
  s := c.InternToDateStr(i);
  AssertEquals(s, '01.05.2001');

  i := c.DateStrToIntern('01.06.1922');
  s := c.InternToDateStr(i);
  AssertEquals(s, '01.06.1922');

  i := c.DateStrToIntern('29.02.2012');
  s := c.InternToDateStr(i);
  AssertEquals(s, '29.02.2012');
end;

procedure TTestCalendar.TestDateDiff;
var
  c : TPtrCalendar;
  d : Double;
begin
  c := TPtrCalendar.Create;
  d := c.DateDiff('12.03.2010', '15.03.2010');
  AssertEquals(d, 3);

  d := c.DateDiff('15.03.2010', '12.03.2010');
  AssertEquals(d, 3);

  d := c.DateDiff('24.12.2010', '05.01.2011');
  AssertEquals(d, 12);
end;

procedure TTestCalendar.TestTimeDiff;
var
  c : TPtrCalendar;
  l : LongInt;
begin
  c := TPtrCalendar.Create;

  l := c.TimeDiff('08:32', '9:22');
  AssertEquals(l, 50);

  l := c.TimeDiff('15:32', '0:22');
  AssertEquals(l, 530);
  AssertEquals(c.InternToTimeStr(l), '08:50');

  l := c.TimeDiff('07:05', '16:22');
  AssertEquals(l, 557);
  AssertEquals(c.InternToTimeStr(l), '09:17');
end;

procedure TTestCalendar.TestTimeDiffStr;
var
  c : TPtrCalendar;
begin
  c := TPtrCalendar.Create;
  AssertEquals(c.TimeDiffStr('15:32', '0:22'), '08:50');
  AssertEquals(c.TimeDiffStr('07:05', '16:22'), '09:17');
end;

initialization
  RegisterTest(TTestCalendar); 
end.

