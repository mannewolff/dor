(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: testprocessglobal.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testprocessglobal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  TTestProcessGlobal = class(TTestCase)
  published
    procedure TestprocessGlobals();
  end;

implementation

uses properties, processglobal, Vector, analyseglobals,
     generatorContext, fileloader, analysertools;


procedure TTestProcessGlobal.TestprocessGlobals();
var
  processglobalcommand  : TProcessGlobal;
  processfileloadcommand: TFileLoader;
  anaglobals            : TAnalyseGlobals;
  prop                  : TProperties;
  context               : TGeneratorContext;
  expectedoutput        : String;
  list                  : TStringList;
  listexp               : TStrVector;
  exp,
  expe    : String;
  I       : Integer;
begin
  prop := TProperties.Create();
  prop.load(ExtractFileDir(ParamStr(0)) + '\test.properties');

  context := TGeneratorContext.Create();

  context.analyser_filename := prop.get('metaprocessglobal');
  context.global_template[1] := prop.get('templateprocessglobal');

  anaglobals := TAnalyseGlobals.Create();
  anaglobals.execute(context);
  anaglobals.Free();

  processfileloadcommand  := TFileloader.Create();
  processfileloadcommand.execute(context);
  processfileloadcommand.Free();

  // Hier beginnt der eigentliche Test
  processglobalcommand := TProcessGlobal.Create();
  processglobalcommand.execute(context);
  processglobalcommand.Free();

  expectedoutput := prop.get('expectedglobal');
  prop.Free();

  list := TStringList.Create();
  list.LoadFromFile(expectedoutput);
  listexp := context.global_workingmap[1];

  AssertEquals(list.Count, listexp.Size);
  For I := 0 to list.Count -1 do
  begin
    exp  := list.Strings[I];
    expe := listexp.Items[I];
    AssertEquals(exp, expe);
  end;

end;



initialization

  RegisterTest(TTestProcessGlobal);
end.

