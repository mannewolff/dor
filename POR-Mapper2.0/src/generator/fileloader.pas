(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit fileloader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, Vector;

type
  (**
    * Läd die Templates (bis zu 10). In den Templates sind die statischen
    * Teile des Artefakts sowie die Makros und Inline-Commands. Sie gilt
    * Als "Workingmap" für jedes Command, was Ersetzungen im Template vornimmt.
    *)
  TFileLoader = class(TCommand)
  public
    function execute(AContext: TContext) : boolean; override;
  end;

implementation

uses
  generatorContext;

function TFileLoader.execute(AContext: TContext) : boolean;
var
  I,J          : Integer;
  s            : String;
  ctx          : TGeneratorContext;
  templatename : String;
  template     : TStringList;
  output       : TStrVector;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandFileLoader then exit;

  For I := 1 to 10 do
  begin
    templatename := ctx.global_template[I];
    if templatename = '' then continue;

    template := TStringList.Create();
    template.LoadFromFile(templatename);
    output := TStrVector.Create();
    for J := 0 to template.Count - 1 do
    begin
      s := template.Strings[J];
      output.Add(s);
    end;
    ctx.global_workingmap[I] := output;
    template.Free();
  end;
  result := false;
end;

initialization
  RegisterClass(TFileLoader);

end.

