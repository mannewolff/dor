(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit testanalyseglobals;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry;

type

  // Testklasse für die units analyser und analysertools
  TTestAnalyseGlobals= class(TTestCase)
  published
    // TAnalyser
    procedure TestAnalyserPrepareGlobals();
  end;

implementation

uses AnalyseGlobals, Hashmap, strobj;

procedure TTestAnalyseGlobals.TestAnalyserPrepareGlobals();
var
  s         : String;
  v         : String;
  analyser  : TAnalyseGlobals;
  map       : TStrHashmap;
begin
  analyser := TAnalyseGlobals.Create();
  map := TStrHashmap.Create();

  // Doppelpunkt als Trenner
  s := '@UNIT : testanalyser';
  analyser.prepareGlobals(s, map);
  v := TString(map.GetValue('@UNIT')).getString();
  AssertEquals(v, 'testanalyser');

  // Gleichheitszeichen als Trenner
  s := '@UNIT = testanalyser';
  analyser.prepareGlobals(s, map);
  v := TString(map.GetValue('@UNIT')).getString();
  AssertEquals(v, 'testanalyser');

  // Space als Trenner
  s := '@UNIT testanalyser';
  analyser.prepareGlobals(s, map);
  v := TString(map.GetValue('@UNIT')).getString();
  AssertEquals(v, 'testanalyser');

  // Nur die ersten beiden Token sind signifikant
  s := '@TABLE : testanalyser : fehler';
  analyser.prepareGlobals(s, map);
  v := TString(map.GetValue('@TABLE')).getString();
  AssertEquals(v, 'testanalyser');

  FreeAndNil(analyser);
  FreeAndNil(map);
end;


initialization
  RegisterTest(TTestAnalyseGlobals);
end.

