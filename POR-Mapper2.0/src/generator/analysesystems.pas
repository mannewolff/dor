(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analysesystems;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, GeneratorContext, Hashmap, Vector;

type
  (**
   * Analysiert die Teile, die in der *systems Sektion ausgezeichnet sind. Dies
   * sind die verschiedenen Ausgabefiles für die Artefakte @outfile[] und die
   * Lage der Templates für die Ausgabefile @template[].
   *)
  TAnalyseSystems = class(TCommand)
  private
    procedure setContext(ctx : TGeneratorContext);
  public
    procedure prepareSystems(ctx: TGeneratorContext; line: string);
    function  execute(AContext: TContext) : boolean; override;
  end;

implementation

uses
  analysertools, strobj;

procedure TAnalyseSystems.setContext(ctx : TGeneratorContext);
var
  map : TStrHashMap;
begin
  map := ctx.analyser_systems;

  if map.ContainsKey('@commandAnalyseDeclarations') then
  ctx.commandAnalyseDeclarations :=
     transformTrueFalse(TString(map.GetValue('@commandAnalyseDeclarations')).getString());

  if map.ContainsKey('@commandAnalyseDeclarationsData') then
  ctx.commandAnalyseDeclarationsData :=
     transformTrueFalse(TString(map.GetValue('@commandAnalyseDeclarationsData')).getString());

  if map.ContainsKey('@commandAnalyseGlobals') then
  ctx.commandAnalyseGlobals :=
     transformTrueFalse(TString(map.GetValue('@commandAnalyseGlobals')).getString());

  if map.ContainsKey('@commandAnalyserStatements') then
  ctx.commandAnalyserStatements :=
     transformTrueFalse(TString(map.GetValue('@commandAnalyserStatements')).getString());

  if map.ContainsKey('@commandProcessIdentifiers') then
  ctx.commandProcessIdentifiers :=
     transformTrueFalse(TString(map.GetValue('@commandProcessIdentifiers')).getString());

  if map.ContainsKey('@commandFileLoader') then
  ctx.commandFileLoader :=
     transformTrueFalse(TString(map.GetValue('@commandFileLoader')).getString());

  if map.ContainsKey('@commandProcessInline') then
  ctx.commandProcessInline :=
     transformTrueFalse(TString(map.GetValue('@commandProcessInline')).getString());

  if map.ContainsKey('@commandProcessTemplateDynamic') then
  ctx.commandProcessTemplateDynamic :=
     transformTrueFalse(TString(map.GetValue('@commandProcessTemplateDynamic')).getString());

  if map.ContainsKey('@commandProcessGlobal') then
  ctx.commandProcessGlobal :=
     transformTrueFalse(TString(map.GetValue('@commandProcessGlobal')).getString());

  if map.ContainsKey('@commandCleanup') then
  ctx.commandCleanup :=
     transformTrueFalse(TString(map.GetValue('@commandCleanup')).getString());

  if map.ContainsKey('@commandBeautifier') then
  ctx.commandBeautifier :=
     transformTrueFalse(TString(map.GetValue('@commandBeautifier')).getString());

  if map.ContainsKey('@commandProcessOutput') then
  ctx.commandProcessOutput :=
     transformTrueFalse(TString(map.GetValue('@commandProcessOutput')).getString());
end;

(**
 * Erzeugt die globalen Ersetzungen, die in *systems definiert sind.
 *)
procedure TAnalyseSystems.prepareSystems(ctx: TGeneratorContext;
                                         line: string);
var
  tokens      : TStrVector;
  key, Value  : string;
  trenner     : char;
begin
  if ctx.analyser_systems = nil then
    exit;

  trenner := ctx.global_assignment;

  try
    tokens := tokenize(line, trenner);
    if tokens.Size < 2 then
      exit;
    key := tokens.GetString(0);
    if key[1] <> '@' then
      exit;

    Value := tokens.GetString(1);
    ctx.analyser_systems.PutValue(key, TString.Create(Value));
  finally
    FreeAndNil(tokens);
  end;
end;

(**
 * Executefunktion des Commands.
 *)
function TAnalyseSystems.execute(AContext: TContext) : boolean;
var
  ctx        : TGeneratorContext;
  metadaten  : TStrVector;
  proceed    : Boolean;
  fname      : String;
  I          : Integer;
  line       : String;
begin
  ctx := TGeneratorContext(AContext);
  proceed := false;

  // Gucken, ob die Metadaten schon geladen wurden.
  metadaten := ctx.analyser_metadata;
  if metadaten = nil then
  begin
    fname    := ctx.analyser_filename;
    ctx.analyser_metadata := loadfile(fname, nil,ctx.global_comment);
  end;

  // Gucken ob es schon Systems gibt
  if ctx.analyser_systems = nil then
     ctx.analyser_systems := TStrHashmap.Create();

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin

    line := ctx.analyser_metadata.GetString(I);

    // Interessant sind nur die *systems Sektionen
    if pos('*systems', AnsiLowerCase(line)) > 0 then
      proceed := true else
    // Bis eine andere *xxx Anweisung gefunden wird
      if (line <> '') and (line[1]= '*') then proceed := false;

    if ((line <> '') and
        (line[1] <> '*') and
        (line[1] <> ctx.global_comment) and
        (proceed)) then
      prepareSystems(ctx, line);
  end;
  setContext(ctx);
  ctx.setStatus('AnalyseSystems');
  result := false;
end;

initialization
  RegisterClass(TAnalyseSystems);

end.

