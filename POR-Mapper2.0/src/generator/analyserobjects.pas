(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analyserobjects;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Vector, HashMap;

  (*
     Reservierter Wörter:

     @include       Einbinden von anderen Dateien, rekursiv
     @comment       Kommentarzeichen für diese Datei
     @filename[I]   Dateiname der Metadatei ohne ext
     @filedir[I]    Dateipfad der Metadatei
     @fileext[I]    Dateiextension der Metadatei
     @outfile[I]    Dateiname der Zieldatei.
     @rootpath      Basispfad (default Pfad des Generators)
     @Attribute     Ein Attribut in einer Attributliste
     @Association   Eine Assoziation in einer Asso-Liste

  *)
type
  TAttribut = class
  public
    Identifier: string; //@Attribut.Identifier
    Typ:     string;    //@Attribut.Typ
    Subtyp:  string;    //@Attribut.Subtyp
    DBTyp:   string;    //@Attribut.DBTyp
    Additional: string; //@Attribut.Additional
    Comment: string;    //@Attribut.Comment
  end;

  TAssociation = class
  public
    Typ: string;         //@Association.Typ
    Identifier: string;  //@Association.Identifier
    Objekt:     string;  //@Association.Object
    ForeignKey:  string; //@Association.ForeignKey
    Comment: string;     //@Association.Comment
    Packages:string;     //@Association.Packages
  end;

  TTemplateDefinition = class
  public
    TemplateName: string;
    Control:    string;
    Liste:      string;
    Statements: TStrVector;
  end;

  TDeclaration = class
     Itterable   : String;
     Section     : String;
     itentifiers : TStrVector;
     data      : TVector;
  end;

  TDeclarationData = class
     data      : TStrHashMap;
  end;


implementation

end.

