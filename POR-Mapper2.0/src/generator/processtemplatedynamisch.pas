(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit processtemplateDynamisch;

{$mode objfpc}{$H+}

interface

uses  Classes, SysUtils, Command, Context, Vector, generatorcontext, analyserobjects;

type

  (*
     Schnittstelle:
     inout : global.workmap      TStrVector = Bereits in Teilen veränderte Datei
     in    : analyser.statements TVector    = Statements als Vector von TTTemplateDefinition
     in    : analyser.attributes TVector    = Attribute als Vector von TAttribute
   *)
  TProcessTemplateDynamic = class(TCommand)
  private
    input:      TStrVector;
    output:     TStrVector;
    statements: TVector;
    function checkParameter(const parameter, comperator, Value: string): boolean;
    procedure proceedDynamic(templatedef: TTemplateDefinition;
      declaration: TDeclaration; AContext: TGeneratorContext;
      typestring: string; parameter: string; aoutput: TStrVector);
  public
    function Execute(AContext: TContext): boolean; override;
  end;

implementation

uses
  strUtils, analysertools, DCL_intf, strobj;

function TProcessTemplateDynamic.checkParameter(
  const parameter, comperator, Value: string): boolean;
var
  anweisung, attribut: string;
  doout: boolean;
begin
  doout := True;
  if (parameter = '') then
    exit;

  splitparameter(parameter, anweisung, attribut);

  if (anweisung = 'notempty') and (attribut = comperator) and (Value = '') then
    doout := False;

  Result := doout;
end;

procedure TProcessTemplateDynamic.proceedDynamic(templatedef: TTemplateDefinition;
  declaration: TDeclaration; AContext: TGeneratorContext; typestring: string;
  parameter: string; aoutput: TStrVector);
var
  vec:    TStrVector;
  att:    TDeclarationData;
  attvec: TVector;
  I, J:   integer;
  s:      string;
  aset:   IStrSet;
  it:     IStrIterator;
  key:    string;
  mkey:   string;
  Value:  string;
  doout:  boolean;
begin
  if AnsiLowerCase(templatedef.control) = '@foreach' then
  begin
    attvec := declaration.Data;
    if attvec = nil then
      exit;
    // Für jedes Attribut
    for I := 0 to attvec.size - 1 do
    begin
      att := TDeclarationData(attvec.GetObject(I));
      vec := templatedef.Statements;
      // Für jedes Statement

      // Behandlung des Typestring []
      Value := '';

      if att.Data.ContainsKey(AContext.global_type) then
        Value := TString(att.Data.GetValue(AContext.global_type)).getString();

      if (Value = '') or (typestring = Value) or (typestring = '') then
      begin
        for J := 0 to vec.size - 1 do
        begin
          s    := vec.GetString(J);
          aset := att.Data.keySet;
          It   := aset.First();
          doout := true;
          while It.HasNext do
          begin
            key  := It.Next;
            mkey := key;
            Delete(mkey, 1, 1);
            mkey  := declaration.Itterable + '.' + mkey;
            Value := TString(att.Data.GetValue(key)).getString();
            s     := AnsiReplaceStr(s, mkey, Value);

            // Der Output finden nur statt, wenn nicht eingeschränkt wurde.
            if doout and (parameter <> '') then
              doout := checkParameter(parameter, mkey, Value);
          end;
          if doout then
            aoutput.Add(s);
        end;
      end;
    end;
  end;
end;

function TProcessTemplateDynamic.Execute(AContext: TContext): boolean;
var
  I, J, K, L: integer;
  loop:    integer;
  s, cs:   string;
  token:   TStrVector;
  templateDef: TTemplateDefinition;
  proceed: boolean;
  ctx:     TGeneratorContext;
  typstring: string;
  p1, p2:  integer;
  parameter: string;

  declaration: TDeclaration;
  declList:    TVector;
begin
  result := false;
  ctx     := TGeneratorContext(AContext);
  if not ctx.commandProcessTemplateDynamic then exit;

  proceed := False;

  for loop := 1 to 10 do
  begin
    if ctx.global_noclearing[loop] = true then continue;
    input := ctx.global_workingmap[loop];
    if input = nil then
      continue;

    output     := TStrVector.Create();
    statements := ctx.analyser_statements[loop];

    for I := 0 to input.size - 1 do
    begin
      proceed := False;
      s  := input.GetString(I);
      cs := trim(s);

      // gucken, ob das @... statement definiert wurde
      //if pos('@', s) > 0 then

      // Beispiel @attributelist
      if (cs <> '') and (cs[1] = '@') then
      begin
        token := tokenize(s, ' ');
        for J := 0 to statements.Size - 1 do
        begin
          templateDef := TTemplateDefinition(statements.GetObject(J));
          if templateDef.TemplateName = token.GetString(0) then
          begin
            // Hier gucken, ob templateDef.Liste in den Deklarationen
            // vorhanden ist.
            declList := ctx.analyser_defines;
            for K := 0 to declList.Size - 1 do
            begin
              declaration := TDeclaration(declList.GetObject(K));
              if pos(declaration.Itterable, templateDef.Liste) > 0 then
              begin
                // Typeinschränkung
                p1 := pos('[', templateDef.Liste);
                p2 := pos(']', templateDef.Liste);
                if (p1 > 0) and (p2 > 0) then
                  typstring := copy(templateDef.Liste, p1 + 1, p2 - p1 - 1)
                else
                  typstring := '';
                // parameter
                if token.Size > 1 then
                begin
                  for L := 1 to token.Size - 1 do
                    parameter := parameter + ' ' + token.GetString(L);
                end
                else
                  parameter := '';
                  proceedDynamic(templatedef,
                  declaration,
                  TGeneratorContext(AContext),
                  typstring,
                  parameter,
                  output);
                proceed := True;
              end;
            end;
          end;
        end;
        FreeAndNil(token);
      end;
      if not proceed then
        output.Add(s);
    end;

    FreeAndNil(input);
    ctx.global_workingmap[loop] := output;
  end;
  Result := False;
end;

initialization
  RegisterClass(TProcessTemplateDynamic);

end.

