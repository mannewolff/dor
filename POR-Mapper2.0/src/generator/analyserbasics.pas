(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analyserbasics;

{$mode delphi}

interface

uses
  Classes, SysUtils,
  Command, Context, GeneratorContext;

type
  TAnalyserBasics = class(TCommand)
  public
    function Execute(AContext: TContext): boolean; override;
  end;

implementation

uses AnalyserTools, Vector;

function TAnalyserBasics.Execute(AContext: TContext): boolean;
var
  fname     : String;             // Root-File to Analyse
  rootdir   : String;             // Root Dir
  ctx       : TGeneratorContext;  // Context für diesen Generator
begin
  ctx := TGeneratorContext(AContext);
  result := true;

  // Hack: Wg. Henne-Ei Problematik wird die Datei zweimal eingelesen.
  //       findComment braucht die Metadaten, die Metadaten brauchen
  //       das Kommentarzeichen.

  // Fehlerspeicher initialisieren.
  ctx.analyser_errors := TVector.Create();
  ctx.processor_errors := TVector.Create();
  fname := ctx.analyser_filename;

  if not FileExists(fname) then
  begin
    makeErrorAnalyser(ctx, fname, 'Fehler beim Laden der Metadaten.', 0, fname);
    exit;
  end;

  // Metadaten laden.
  if ctx.analyser_metadata = nil then
  begin
    ctx.analyser_metadata := loadfile(fname, nil, ctx.global_comment);
  end;

  // PGEN Einstellungen

  // - @assignment
  findassigmentOperator(ctx);
  // - @comment
  findComment(ctx);
  // - @whitespace
  findWhitespace(ctx);
  // - @type[]
  findType(ctx);
  // - @typeseperator
  findTypeSeparator(ctx);
  // - @all
  //findall(ctx);
  // - @templatecomment
  findTemplateComment(ctx);

  // - @whitespaceescape
  findWhitespaceEscape(ctx);

  // Metadaten ein zweites Mal laden
  FreeAndNil(ctx.analyser_metadata);
  if ctx.analyser_metadata = nil then
  begin
    ctx.analyser_metadata := loadfile(fname, nil, ctx.global_comment);
  end;

  // Rootpath setzen
  rootdir := ParamStr(0);
  while (rootdir[length(rootdir)] <> '\') do
    rootdir := copy(rootdir, 1, length(rootdir) -1);
  findRootDir(ctx, rootdir);

  ctx.setStatus('AnalyseBasics');
  result := false;
end;

initialization
  RegisterClass(TAnalyserBasics);

end.

