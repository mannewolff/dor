(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id:  $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit beautifier;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, generatorContext, vector;

type

 TBeauItem = class
     sektion : String;
     bezvec  : TVector;
     lenvec  : TVector;
  end;

  TBeautifier = class(TCommand)
  private
    Steuerdatei    : String;
    SteuerdateiVec : TVector;
    beautyfiles    : String;
    beautytarget   : String;
    function   readSteuerdatei() : TVector;
    function   findBeauty(search : String) : TBeauItem;
    procedure  formatFile(fname : String; dest: String);
    function   formatLine(instr: String; beau : TBeauItem) : String;
    function   formatHeaderBlank(beau : TBeauItem) : String;
    function   formatHeader(beau : TBeauItem) : String;
  public
    function execute(AContext: TContext) : boolean; override;
  end;

implementation

uses analysertools, strobj;

function TBeautifier.readSteuerdatei() : TVector;
var
  tokens   : TStrVector;
  item     : String;
  ee       : Boolean;
  beauItem : TBeauItem;
  I        : Integer;
  return   : TVector;
  vec      : TStrVector;
begin
  if not FileExists(Steuerdatei) then exit;
  vec := loadfile(Steuerdatei, nil, '#');

  ee := false;
  return := TVector.Create();

  for I := 0 to vec.Size - 1 do
  begin
    item := vec.GetString(I);

    if pos('@beautyfiles', AnsiLowerCase(item)) > 0 then
    begin
      tokens := tokenize(item, '=');
      beautyfiles := tokens.GetString(1);
      FreeAndNil(tokens);
    end;

    if pos('@beautytarget', AnsiLowerCase(item)) > 0 then
    begin
      tokens := tokenize(item, '=');
      beautytarget := tokens.GetString(1);
      FreeAndNil(tokens);
    end;

    if pos('@beautifier', AnsiLowerCase(item)) > 0 then
    begin
      ee := true;
      beauItem := TBeauItem.Create();
      tokens := tokenize(item, ' ');
      beauItem.sektion := tokens.GetString(1);
      beauItem.bezvec := TVector.Create();
      beauItem.lenvec := TVector.Create();
      FreeAndNil(tokens);
      continue;
    end;

    if ee and (pos('@end', AnsiLowerCase(item)) > 0) then
    begin
      ee := false;
      return.Add(BeauItem);
      continue;
    end;

    if ee then
    begin
       tokens := tokenize(item, ':');
       beauItem.bezvec.Add(TString.Create(Trim(tokens.GetString(0))));
       beauItem.lenvec.Add(TString.Create(Trim(tokens.GetString(1))));
    end;
  end;
  FreeAndNil(vec);
  result := return;
end;


function TBeautifier.formatHeader(beau : TBeauItem) : String;
var
  I    : integer;
  s    : string;
  res  : string;
  len  : Integer;
  strlen : String;
  bez    : String;
begin
  res := '#';

  for I := 0 to beau.lenvec.Size - 1 do
  begin
    strlen := TString(beau.lenvec.GetObject(I)).getString();
    bez := TString(beau.bezvec.GetObject(I)).getString();
    delete(strlen, 1,1);
    delete(strlen, length(strlen),1);
    if TryStrToInt(strlen, len) then
    begin
      len := StrToInt(strlen);
      if I = 0 then dec(len);
      s := format(bez, len, 0, ' ');
      res := res + s + '|';
    end;
  end;
  Result := res;
end;


function TBeautifier.formatHeaderBlank(beau : TBeauItem) : String;
var
  I    : integer;
  s    : string;
  res  : string;
  len  : Integer;
  strlen : String;
begin
  res := '#';

  for I := 0 to beau.lenvec.Size - 1 do
  begin
    strlen := TString(beau.lenvec.GetObject(I)).getString();
    delete(strlen, 1,1);
    delete(strlen, length(strlen),1);
    if TryStrToInt(strlen, len) then
    begin
      len := StrToInt(strlen);
      if I = 0 then dec(len);
      s := format('', len, 0, '-');
      res := res + s + '+';
    end;
  end;
  Result := res;
end;


function TBeautifier.formatLine(instr: String; beau : TBeauItem) : String;
var
  vec  : TStrVector;
  I    : integer;
  s    : string;
  res  : string;
  len  : Integer;
  strlen : String;
begin
  res := '';
  vec := tokenize(instr, ':');

  for I := 0 to vec.size - 1 do
  begin
    s := vec.GetString(I);
    strlen := TString(beau.lenvec.GetObject(I)).getString();
    delete(strlen, 1,1);
    delete(strlen, length(strlen),1);
    if TryStrToInt(strlen, len) then
    begin
      len := StrToInt(strlen);
      s := format(s, len, 0, ' ');
      res := res + s + '|';
    end;
  end;
  Result := res;
end;

function TBeautifier.findBeauty(search : String) : TBeauItem;
var
  I      : Integer;
  beau   : TBeauItem;
begin
  result := nil;
  for I := 0 to SteuerdateiVec.Size - 1 do
  begin
    beau := TBeauItem(SteuerdateiVec.GetObject(I));
    if beau.sektion = search then
    begin
       result := beau;
       exit;
    end;
  end;
end;

procedure TBeautifier.formatFile(fname : String; dest: String);
var
  lists     : TStringList;
  listd     : TStringList;
  I         : integer;
  beau      : TBeauItem;
  s         : String;
  insektion : Boolean;
begin
  lists := TStringList.Create();
  listd := TStringList.Create();
  listd.Clear;
  insektion := false;
  lists.LoadFromFile(fname);

  for I := 0 to lists.Count - 1 do
  begin
    s := lists.Strings[I];

    if (not insektion) and (s <> '') then
       beau := findBeauty(s);

    if not insektion and (beau <> nil) then
    begin
       insektion := true;
       listd.Add(s);
       s := formatHeaderBlank(beau);
       listd.Add(s);
       s := formatHeader(beau);
       listd.Add(s);
       s := formatHeaderBlank(beau);
       listd.Add(s);
       continue;
    end else

    if insektion and (beau <> nil) and (s = '') then
    begin
      s := formatHeaderBlank(beau);
      listd.Add(s);
      listd.Add('');
      insektion := false;
      beau := nil;
      continue;
    end else

    if insektion and (s <> '') then
    begin
      s := formatLine(s, beau);
      listd.Add(s);
      continue;
    end;

    listd.Add(s);
  end;

  if insektion and (beau <> nil) then
  begin
    s := formatHeaderBlank(beau);
    listd.Add(s);
  end;

  listd.SaveToFile(dest + ExtractFileName(fname) + 'x');
end;

function TBeautifier.execute(AContext: TContext) : boolean;
var
  ctx        : TGeneratorContext;
  searchpath : String;
  searchname : String;
  datei      : TSearchRec;
  source,
  dest       : String;
begin
  result := false;
  ctx := TGeneratorContext(AContext);

  // Net wirklich sauber. Normalerweise sollte dies über einen Analyser
  // ausgelesen werden. Nun gut ..... Dieses Command läuft eh einwenig
  // Orthogonal zu den anderen, und sollte in der Kette als Tools eingetragen
  // werden.
  Steuerdatei := ctx.ctxdefault;
  SteuerdateiVec := readSteuerdatei();

  source := beautyfiles;
  dest := beautytarget;
  if dest[length(dest)] <> '\' then
    dest := dest + '\';

  searchpath := ExtractFileDir(source);
  searchname := ExtractFileName(source);
  if searchpath[length(searchpath)] <> '\' then
    searchpath := searchpath + '\';

  If FindFirst(searchpath + searchname, fadirectory, datei) = 0 then
  begin
    formatFile(searchpath + datei.name, dest);

    while Findnext(datei) = 0 do
    begin
      If datei.Attr and fadirectory = 0 then
      begin
        formatFile(searchpath + datei.name, dest);
      end;
    end;
    FindClose(datei);
  end;
  FreeAndNil(SteuerdateiVec);

  result := false;
end;

initialization
  RegisterClass(TBeautifier);

end.

