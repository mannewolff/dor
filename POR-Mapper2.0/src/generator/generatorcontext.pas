(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit generatorContext;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Context, Vector, HashMap;

type
  TError = class
    line  : String;
  end;

  TGeneratorContext = class(TContext)

    ctxdefault : String; // Zur allgemeinen Verwendung
    filesprocessed : Boolean;

    // Jedes Command gibt sein Bearbeitung bekannt
    status:              TStrVector;

    // Globale Inhalte
    global_template:     Array[1..10] of string;     // original Templatename
    global_workingmap:   Array[1..10] of TStrVector; // Arbeitskopie des Outputs

    global_comment:          Char;       // Kommentare  @comment
    global_whitespace:       Char;       // Whitespace  @whitespace
    global_rootpath:         String;     // Rootdir     @rootdir
    global_type:             String;     // @type[] derektive
    global_typeseparator:    Char;       // @typeseparator derektive
    global_assignment:       Char;       // Zeichen für Zuweisung
    global_templatecoment:   Char;       // Zeichen für einen Templatekommentar
    global_whitespacescape : String;     // true oder false

    global_outfile:      Array[1..10] of String;      // generierte Dateien
    global_concat:       Array[1..10] of boolean;     // concatiniert
    global_once:         Array[1..10] of boolean;     // file nur einma generieren
    global_noclearing:   Array[1..10] of boolean;     // kein Clearing erwünscht
    global_filename:     String;                      // aus analyser_filename
    global_filepath:     String;                      // aus analyser_filename
    global_fileext:      String;                      // aus analyser_filename

    // Artefakte vom Analyser
    analyser_filename:   string;      // Dateiname der META-Datei
    analyser_metadata:   TStrVector;  // Die kompletten Metadaten als Zeilen
    analyser_globals:    TStrHashMap; // Globale Ersetzungsstring
    analyser_systems:    TStrHashMap; // Systemeinstellungen
    analyser_attributes: TVector;     // Attribute als Vector von TAttribute
    analyser_associations: TVector;   // Beziehungen als Vector von TAssociation
    analyser_statements: Array[1..10] of TVector; // Statements als Vector von
                                                  // TTTemplateDefinition
    analyser_defines:    TVector;       // Defines als Vector von TDeclaration
    analyser_defines_data: TStrHashmap; // Data of the defines

    analyser_errors:     TVector;     // Fehler beim Analyser
    processor_errors:    TVector;     // Fehler beim Processor

    // Commands en- disablen
    commandAnalyserBasics          : Boolean;
    commandAnalyseDeclarations     : Boolean;
    commandAnalyseDeclarationsData : Boolean;
    commandAnalyseSystems          : Boolean;
    commandAnalyseGlobals          : Boolean;
    commandAnalyserStatements      : Boolean;

    commandProcessIdentifiers      : Boolean;
    commandFileLoader              : Boolean;
    commandProcessInline           : Boolean;
    commandProcessTemplateDynamic  : Boolean;
    commandProcessGlobal           : Boolean;

    commandCleanup                 : Boolean;
    commandBeautifier              : Boolean;
    commandProcessOutput           : Boolean;

    constructor Create(); overload;
    destructor Destroy(); override;
    procedure setStatus(astatus : String);
    function  isStatus(astatus : String) : boolean;
  end;

implementation

constructor TGeneratorContext.Create();
var
  I : Integer;
begin
  analyser_filename   := '';
  analyser_metadata   := nil;
  analyser_globals    := nil;
  analyser_systems    := nil;
  analyser_attributes := nil;
  status := TStrVector.Create();

  for I := 1 to 10 do
  begin
    analyser_statements[I] := nil;
    global_template[I]     := '';
    global_workingmap[I]   := nil;
  end;
  commandAnalyserBasics := true;
  commandAnalyseDeclarations := true;
  commandAnalyseDeclarationsData := true;
  commandAnalyseSystems := true;
  commandAnalyseGlobals := true;
  commandAnalyserStatements := true;

  commandProcessIdentifiers  := true;
  commandFileLoader := true;
  commandProcessInline := true;
  commandProcessTemplateDynamic := true;
  commandProcessGlobal := true;

  commandCleanup := true;
  commandBeautifier := true;
  commandProcessOutput := true;

  filesprocessed := false;
end;

destructor TGeneratorContext.Destroy();
var
  I : Integer;
begin
  analyser_filename := '';
  FreeAndNil(analyser_metadata);
  FreeAndNil(analyser_globals);
  FreeAndNil(analyser_systems);
  FreeAndNil(analyser_attributes);
  FreeAndNil(analyser_associations);
  FreeAndNil(analyser_defines);
  FreeAndNil(analyser_defines_data);
  FreeAndNil(analyser_errors);
  FreeAndNil(processor_errors);
  FreeAndNil(status);

  for I := 1 to 10 do
  begin
    FreeAndNil(analyser_statements[I]);
    global_template[I] := '';
    FreeAndNil(global_workingmap[I]);
  end;
end;

procedure TGeneratorContext.setStatus(astatus : String);
begin
  status.Add(astatus);
end;

function  TGeneratorContext.isStatus(astatus : String) : boolean;
var
  I      : Integer;
  st     : String;
begin
  result := false;
  for I := 0 to status.Size - 1 do
  begin
    st := status.GetString(I);
    if st = astatus then
    begin
      result := true;
      break;
    end;
  end;
end;

end.

