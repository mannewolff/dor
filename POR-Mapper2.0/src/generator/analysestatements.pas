(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analysestatements;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  analyserobjects,  Command, Context, Vector, generatorContext;

type
  (*
    Analysiert die *template[]-Sektionen und speichert die Makros in
    TTemplateDefinition-Objekte. Gespeichert wird der Makroname, die
    Art der Iteration (@foreach) und die Datenstruktur, die an dem
    Makro beteiligt ist.
   *)
  TAnalyserStatements = class(TCommand)

  private
    state:          Integer;
    actTemplateDef: TTemplateDefinition;
    actstatements:  TStrVector;

    procedure setState(const s: string);
  public
    actStatementState: integer;
    function Execute(AContext: TContext): boolean; override;
    procedure prepareStatements(line: string; astatements: TVector);
  end;

implementation

uses analysertools, analyserbasics;

const
  // States der Statemaschine
  stTemplates = 1000; // 1001..1010

function TAnalyserStatements.Execute(AContext: TContext): boolean;
var
  fname     : string;             // Root-File to Analyse
  ctx       : TGeneratorContext;  // Context für diesen Generator
  I         : Integer;
  comment   : Char;
  line      : String;
begin
  actStatementState := 100;
  state  := 0;
  result := false;

  ctx      := TGeneratorContext(AContext);
  if not ctx.commandAnalyserStatements then exit;

  // Gucken, ob die Metadaten schon geladen wurden.
  if ctx.analyser_metadata = nil then
  begin
    fname    := ctx.analyser_filename;
    ctx.analyser_metadata := loadfile(fname, nil, ctx.global_comment);
  end;

  for I := 1 to 10 do ctx.analyser_statements[I] := TVector.Create();

  findComment(ctx);
  findWhitespace(ctx);
  comment := ctx.global_comment;

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin

    line := ctx.analyser_metadata.GetString(I);
    if (line <> '') and (line[1] = '*') then
    setState(line);

    if ((line <> '') and
        (line[1] <> '*') and
        (line[1] <> comment)) then
    begin
      if state > stTemplates then
      begin
        prepareStatements(line, ctx.analyser_statements[state - stTemplates]);
      end;
    end;
  end;
  ctx.setStatus('AnalyseStatements');
end;

procedure TAnalyserStatements.prepareStatements(line: string; astatements: TVector);
const
  stdefinition = 100;
  stcontrol    = 101;
  ststatement  = 102;
var
  tokens: TStrVector;
  p:      integer;
  key, Value: string;
begin

  case actStatementState of
    stdefinition:
    begin
      p := pos('=', line);
      if (p > 0) then
      begin
        actTemplateDef := TTemplateDefinition.Create();
        astatements.add(actTemplateDef);
        actTemplateDef.TemplateName := Trim(Copy(line, 1, p - 1));
        actStatementState := stcontrol;
      end;
    end;

    stcontrol:
    begin
      tokens := tokenize(line, ' ');
      if tokens.Size < 3 then
        exit;
      key   := tokens.GetString(0);
      Value := tokens.GetString(1);
      actTemplateDef.Control := key;
      actTemplateDef.Liste := Value;
      actStatementState := ststatement;
      FreeAndNil(tokens);
    end;

    ststatement:
    begin
      // Zeilen 1:1 in den Statementvector schreiben
      if actstatements = nil then
        actstatements := TStrVector.Create();

      if AnsiLowerCase(line) = '@end' then
      begin
        actTemplateDef.Statements := actstatements;
        actStatementState := stdefinition;
        actstatements     := nil;
      end
      else
      begin
        actstatements.Add(line);
      end;
    end;
  end;

end;

(**
 * Analysiert eine Zeile, ob ein neuer State gesetzt wird. Wenn ja,
 * dann wird die Variable state entsprechende gesetzt.
 *)
procedure TAnalyserStatements.setState(const s: string);
var
  astatestring : string;
  I            : Integer;
begin
  astatestring := trim(s);
  for I := 1 to 10 do
  begin
    if ((AnsiLowerCase(astatestring) = '*template[' + IntToStr(I) + ']') or
        (AnsiLowerCase(astatestring) = '*template[' + IntToStr(I) + ']:'))
        then state := stTemplates + I;
  end;

  if copy(AnsiLowerCase(astatestring),1, 9) <> '*template' then
   state := 0;

end;

initialization
  RegisterClass(TAnalyserStatements);

end.

