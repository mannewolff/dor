(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analyseattributes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, Vector;

type
  (*
     Benötigte Commands: ---

     Schnittstelle:
     in    : analyser_filename
     inout : analyser_metadata TStrHashMap Werden bei Bedarf erzeugt.
   *)
  TAnalyseAttributes = class(TCommand)
  public
    procedure prepareAttributes(line: string; aattributes: TVector);
    function  execute(AContext: TContext) : boolean; override;
  end;


implementation

uses
  analysertools, analyserobjects, generatorContext;

(*
  Damit diese Methode auch getestet werden kann, ist sie public. Eigentlich
  bräuchte sie es nicht.
  --> Analysiert eine Zeile und schreibt die globalen key-value Paare in
      die Map aglobals
*)

(*
  Erzeugt die Attributliste
*)
procedure TAnalyseAttributes.prepareAttributes(line: string; aattributes: TVector);
var
  tokens:   TStrVector;
  item:     string;
  attribut: TAttribut;
begin
  if aattributes = nil then
    exit;
  attribut := TAttribut.Create();

  try
    tokens := tokenize(line, ':');

    if tokens.Size > 0 then
    begin
      item := tokens.GetString(0);
      attribut.Identifier := item;
    end;

    if tokens.Size > 1 then
    begin
      item := tokens.GetString(1);
      attribut.Typ := item;
    end;

    if tokens.Size > 2 then
    begin
      item := tokens.GetString(2);
      attribut.Subtyp := item;
    end;

    if tokens.Size > 3 then
    begin
      item := tokens.GetString(3);
      attribut.DBTyp := item;
    end;

    if tokens.Size > 4 then
    begin
      item := tokens.GetString(4);
      attribut.Additional := item;
    end;

    if tokens.Size > 5 then
    begin
      item := tokens.GetString(5);
      attribut.Comment := item;
    end;

    aattributes.Add(attribut);

  finally
    FreeAndNil(tokens);
  end;
end;


function TAnalyseAttributes.execute(AContext: TContext) : boolean;
var
  ctx        : TGeneratorContext;
  metadaten  : TStrVector;
  proceed    : Boolean;
  fname      : String;
  I          : Integer;
  line       : String;
begin
  result := false;
  ctx := TGeneratorContext(AContext);

  proceed := false;

  // Gucken, ob die Metadaten schon geladen wurden.
  metadaten := ctx.analyser_metadata;
  if metadaten = nil then
  begin
    fname    := ctx.analyser_filename;
    ctx.analyser_metadata := loadfile(fname, nil,ctx.global_comment);
  end;

  // Gucken ob es schon Systems gibt
  if ctx.analyser_attributes = nil then
     ctx.analyser_attributes := TVector.Create();

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin

    line := ctx.analyser_metadata.GetString(I);
    // Interessant sind nur die *systems Sektionen
    if pos('*attributes:', line) > 0 then
      proceed := true else

    if (line <> '') and (line[1]= '*') then proceed := false;

    if ((line <> '') and
        (line[1] <> '*') and
        (line[1] <> ctx.global_comment) and
        (proceed)) then
      prepareAttributes(line, ctx.analyser_attributes);
  end;
  ctx.setStatus('AnalyseAttributes');
  result := false;
end;

initialization
  RegisterClass(TAnalyseAttributes);

end.

