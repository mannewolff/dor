(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analysedeclarations;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  Classes, SysUtils,
  AnalyserObjects, Command, Context, GeneratorContext, Vector;

type
  (*
   Aufgaben:
   - Auswertung der *defines: Sektion
   *)
  TAnalyseDeclarations = class(TCommand)
  private
    actDeclaration : TDeclaration; // Aktuelle Deklaration
    ee             : boolean;      // Fehlerdetektor: @End expected
  public
    function prepareDefines(ctx: TGeneratorContext; line: string; ln: Integer) : boolean;
    function  execute(AContext: TContext) : boolean; override;
  end;

implementation

uses
  AnalyserTools;

(*
  Erzeugt die Attributliste
*)
function TAnalyseDeclarations.prepareDefines(ctx: TGeneratorContext;
                                              line: string; ln: Integer): boolean;
var
  tokens:   TStrVector;
  item:     string;
begin
  result := true;
  if ctx.analyser_defines = nil then
    exit;

  result := false;

  if pos('@define', AnsiLowerCase(line)) > 0 then
  begin
    ee := true;
    actDeclaration := TDeclaration.Create();
    tokens := tokenize(line, ' ');
    if tokens.Size < 3 then
    begin
       result := makeErrorAnalyser(ctx, line,
           '@define hat zu wenig Parameter', ln-1, ctx.analyser_filename);
       exit;

    end;

    actDeclaration.Itterable := tokens.GetString(1);
    if (actDeclaration.Itterable <> '') and
       (actDeclaration.Itterable[1] <> '@') then
       begin
         result := makeErrorAnalyser(ctx, line,
             '@define: Bezeichner muss mit einem @ beginnen.',
             ln-1, ctx.analyser_filename);
         exit;
       end;
    item := tokens.GetString(2);
    if (item <> '') and
       (item[1] <> '{') then
       begin
         result := makeErrorAnalyser(ctx, line,
         '@define: Fehler in der Sektionsbeschreibung ({ erwartet).',
         ln-1, ctx.analyser_filename);
         exit;
       end;
    if (item <> '') and
       (item[2] <> '*') then
       begin
       result := makeErrorAnalyser(ctx, line,
       '@define: Fehler in der Sektionsbeschreibung (* erwartet).',
       ln-1, ctx.analyser_filename);
       exit;
       end;

    if item[1] = '{' then item := copy(item, 2, length(item) -1);
    if item[length(item)] = '}' then item := copy(item, 1, length(item) -1);
    actDeclaration.Section := item;
    actDeclaration.itentifiers := TStrVector.Create();
    actDeclaration.data := TVector.Create();
    ee := true;
  end else

  if (pos('@end', AnsiLowerCase(line)) > 0) and ee then
  begin
    ctx.analyser_defines.Add(actDeclaration);
    actDeclaration := nil;
    ee := false;
  end else
  begin
    if ee then
    actDeclaration.itentifiers.Add(trim(line));
  end;
end;


function TAnalyseDeclarations.execute(AContext: TContext) : boolean;
var
  ctx        : TGeneratorContext;
  metadaten  : TStrVector;
  proceed    : Boolean;
  fname      : String;
  I          : Integer;
  line       : String;
  attributes : boolean;
  asso       : boolean;
  decl       : TDeclaration;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandAnalyseDeclarations then exit;

  proceed        := false;
  actDeclaration := nil;
  ee             := false;

  // Gucken, ob die Metadaten schon geladen wurden.
  metadaten := ctx.analyser_metadata;
  if metadaten = nil then
  begin
    fname    := ctx.analyser_filename;
    ctx.analyser_metadata := loadfile(fname, nil,ctx.global_comment);
  end;

  if ctx.analyser_defines = nil then
  begin
    ctx.analyser_defines := TVector.Create();
  end;

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin
    line := ctx.analyser_metadata.GetString(I);

    // Interessant sind nur die *defines Sektionen
    if pos('*defines', line) > 0 then
    begin
      proceed := true;
    end else
      if (line <> '') and (line[1]= '*') then
      begin
        proceed := false;
        if ee then
        begin
          result := makeErrorAnalyser(ctx, line,
          '*defines: @END erwartet', I-2, ctx.analyser_filename);
          ee := false;
          exit;
        end;
      end;
    if ((line <> '') and
        (line[1] <> '*') and
        (line[1] <> ctx.global_comment) and
        (proceed)) then
      if prepareDefines(ctx, line, I) then
      begin
        result := true;
        exit;
      end;
  end;

  attributes := false;
  asso       := false;

  // Gucken, ob die Standarddeklarationen geladen sind (abwärtskompatibilität)
  for I := 0 to ctx.analyser_defines.Size - 1 do
  begin
     decl :=  TDeclaration(ctx.analyser_defines.GetObject(I));
     if AnsiLowerCase(decl.Itterable) = '@attribute' then attributes := true;
     if AnsiLowerCase(decl.Itterable) = '@association' then asso := true;
  end;
  if not attributes then
  begin
     actDeclaration := TDeclaration.Create();
     actDeclaration.Itterable := '@Attribute';
     actDeclaration.Section := '*attributes';
     actDeclaration.itentifiers := TStrVector.Create();
     actDeclaration.itentifiers.Add('@Identifier');
     actDeclaration.itentifiers.Add('@Typ');
     actDeclaration.itentifiers.Add('@Subtyp');
     actDeclaration.itentifiers.Add('@DBTyp');
     actDeclaration.itentifiers.Add('@Additional');
     actDeclaration.itentifiers.Add('@Comment');
     actDeclaration.data := TVector.Create();
     ctx.analyser_defines.Add(actDeclaration);
  end;
  if not asso then
  begin
     actDeclaration := TDeclaration.Create();
     actDeclaration.Itterable := '@Association';
     actDeclaration.Section := '*associations';
     actDeclaration.itentifiers := TStrVector.Create();
     actDeclaration.itentifiers.Add('@Typ');
     actDeclaration.itentifiers.Add('@Identifier');
     actDeclaration.itentifiers.Add('@Object');
     actDeclaration.itentifiers.Add('@ForeignKey');
     actDeclaration.itentifiers.Add('@Comment');
     actDeclaration.itentifiers.Add('@Packages');
     actDeclaration.data := TVector.Create();
     ctx.analyser_defines.Add(actDeclaration);
  end;

  ctx.setStatus('AnalyseDeclarations');
  result := false;
end;

initialization
  RegisterClass(TAnalyseDeclarations);

end.

