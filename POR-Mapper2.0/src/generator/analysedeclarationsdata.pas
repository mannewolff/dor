(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analysedeclarationsdata;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  Classes, SysUtils,
  AnalyserObjects, Command, Context, GeneratorContext, Hashmap, Vector;

type
  TAnalyseDeclarationsData = class(TCommand)
  private
    actDeclaration : TDeclaration;
  public
    function prepareDefinesData(line: string;
                                 section: String;
                                 ctx : TGeneratorContext) : boolean;
    function  execute(AContext: TContext) : boolean; override;
  end;


implementation

uses
  analysertools, strobj;

(*
  Erzeugt die Attributliste
*)
function TAnalyseDeclarationsData.prepareDefinesData(line: string;
                                   section: String;
                                   ctx : TGeneratorContext) : Boolean;
var
  token      : TStrVector;
  define     : TDeclaration;
  definedata : TDeclarationData;
  I          : Integer;
  seperator  : char;
  temp       : String;
begin
  result := false;
  seperator := ctx.global_typeseparator;
  token := tokenize(line, seperator);
  define := nil;

  // Definitionsobjekt für diese Sektion laden
  for I := 0 to ctx.analyser_defines.Size -1 do
  begin
     define := TDeclaration(ctx.analyser_defines.GetObject(I));
     if define.Section = section then break;
  end;

  if define = nil then
  begin
    result := makeErrorAnalyser(ctx, section,
    'Kann @define für Sektion nicht finden', 0, '');
    exit;
  end;
  definedata := TDeclarationData.Create();
  definedata.data := TStrHashMap.Create();
  for I := 0 to define.itentifiers.Size -1 do
  begin
    temp := token.GetString(I);
    if temp = ctx.global_whitespace then
      temp := '';
    definedata.data.PutValue(define.itentifiers.GetString(I),
                             TString.Create(temp));
  end;
  define.data.Add(definedata);
end;


function TAnalyseDeclarationsData.execute(AContext: TContext) : boolean;
var
  ctx               : TGeneratorContext;
  proceed, lines    : Boolean;
  I, J              : Integer;
  line              : String;
  saveSection       : String;
begin
  result := false;
  ctx            := TGeneratorContext(AContext);
  if not ctx.commandAnalyseDeclarationsData then exit;

  proceed        := false;
  lines          := false;
  actDeclaration := nil;

  if not ctx.isStatus('AnalyseDeclarations') then
  begin
    result := makeErrorAnalyser(ctx, 'AnalyseDeclarations fehlt.',
        'Fehler bei der technischen Konfiguration', 0, '');
    exit;
  end;

  // Gucken, ob die Metadaten schon geladen wurden.
  if ctx.analyser_metadata = nil then
    ctx.analyser_metadata := loadfile(ctx.analyser_filename,
        nil,ctx.global_comment);

  if ctx.analyser_defines_data = nil then
    ctx.analyser_defines_data := TStrHashMap.Create();


  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin

    line := ctx.analyser_metadata.GetString(I);

    // Interessant sind nur die Sektionen, die indefines definiert sind
    if (line <> '') and (line[1] = '*') then
    for J := 0 to ctx.analyser_defines.Size - 1 do
    begin
      if pos(TDeclaration(ctx.analyser_defines.GetObject(J)).Section, line) > 0 then
      begin
        lines := true;
        break;
      end else
        lines := false;
    end;
    if lines then begin
        proceed := true;
        if saveSection <>  TDeclaration(ctx.analyser_defines.GetObject(J)).Section then
        begin
         ;
        end;
        saveSection := TDeclaration(ctx.analyser_defines.GetObject(J)).Section;
    end else
    begin
      if (line <> '') and (line[1]= '*') then
      begin
        proceed := false;
        lines := false;
      end;
    end;

    if ((line <> '') and
        (line[1] <> '*') and
        (line[1] <> ctx.global_comment) and
        (proceed) and
        (lines))
      then
      prepareDefinesdata(line, saveSection, ctx);
  end;
  ctx.setStatus('AnalyseDeclarationsData');
  result := false;
end;

initialization
  RegisterClass(TAnalyseDeclarationsData);

end.

