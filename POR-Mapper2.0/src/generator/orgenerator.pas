(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 1.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit orgenerator;

{$mode delphi}
{$H+}

interface

uses
  SysUtils, Classes;

type
  // Record für die Metadaten, die aus der Datei *.meta extrahiert werden
  TMeta = Record
    unitname : String;
    classname : String;
    tablename : String;
    attrList  : TList;
    assList   : TList;
    usesList  : TStringList;
  end;

  // Record für die Attributliste, die aus der Datei *.meta extrahiert werden
  PAtt = ^TAtt;
  TAtt = Record
    name : String;
    typ  : String;
    ass  : String;
    db   : String;
  end;

  // Record für die Assoziationen, die aus der Datei *.meta extrahiert werden
  PAss = ^TAss;
  TAss = Record
     typ : String;
     bez : String;
     obj : String;
     FK  : String;
  end;

  // Übergabe der Daten aus einer GUI oder einer Kommandozeile
  TFormData = Record
    database,
    atemplatename,
    aunitdir,
    asqldir,
    ametadir,
    ametafile         : String;
    agendelete,
    agenerateall,
    ageneratedelphi,
    ageneratesql,
    agenerateget,
    agenerateset      : Boolean;
  end;

  (* Generiert aus einer DSL sowohl eine Delphi Unit als auch das SQL zu der
     entsprechenden Fachklasse
  *)
  TORGenerator = class
  private
    database,                     // Name der Datenbank zum Generieren
    templatename,                 // Directory zu den Templates
    unitdir,                      // Directory zum Schreiben der Delphi Unit
    sqldir,                       // Directory zum Schreiben des SQLs
    metadir,                      // Directory, in dem die META Dateien liegen
    metafile         : String;    // Aktuelle Metadatei
    gendelete,                    // Delete Methoden generieren
    generateall,                  // Alle Metadaten generieren
    generatedelphi,               // Delphi Datei soll generiert werden
    generatesql,                  // SQL Datei soll generiert werden
    generateget,                  // Getter sollen generiert werden
    generateset      : Boolean;   // Setter sollen generiert werden
    METAlist : TStringList;       // Metadaten als Stringlist
    function leseDatei(filename : String) : TStringList;
    function analyseMETA() : TMeta;
    function writeDelphiUnit() : boolean;
    function writeSQLFile() : boolean;
    function replaceStrings(var w: TextFile; s : String; meta : TMeta) : String;
    procedure setParameter(const data : TFormData);
    procedure goDatei();
  public
    procedure Execute(const data : TFormData);
  end;


var
  FormMain: TORGenerator;

implementation

{$WARN UNIT_PLATFORM OFF }
{$define lazarus}

uses  strUtils;

(* ---------------------------------------------------------------------------
   Öffentliche Methoden, Interface
 --------------------------------------------------------------------------- *)
(*
  Generiert was gewünscht wird.
*)
procedure TORGenerator.Execute(const data : TFormData);
var
  searchpath : String;
  datei      : TSearchRec;
begin
  setParameter(data);

  if data.agenerateall then
  begin
    searchpath := data.ametadir;
    searchpath := searchpath + '\*.meta';
    If FindFirst(searchpath, fadirectory, datei) = 0 then
    begin
      metafile := datei.name;
      goDatei;
      while Findnext(datei) = 0 do
      begin
        If datei.Attr and fadirectory = 0 then
        begin
          metafile := datei.name;
          goDatei;
        end;
      end;
      FindClose(datei);
    end;
  end else
  begin
    goDatei();
  end;
end;

procedure TORGenerator.goDatei();
begin
    if generatedelphi then
       writeDelphiUnit();
    if generatesql then
       writeSQLFile;
end;

(* ---------------------------------------------------------------------------
   Construction, Destruction
 --------------------------------------------------------------------------- *)

(* ---------------------------------------------------------------------------
   Private Methoden
 --------------------------------------------------------------------------- *)

(* Setzt alle benötigten Informationen per API *)
procedure TORGenerator.setParameter(const data : TFormData);
begin
  templatename   := data.atemplatename;
  unitdir        := data.aunitdir;
  sqldir         := data.asqldir;
  metadir        := data.ametadir;
  metafile       := data.ametafile;
  generatedelphi := data.ageneratedelphi;
  generatesql    := data.ageneratesql;
  generateget    := data.agenerateget;
  generateset    := data.agenerateset;
  database       := data.database;
  generateall    := data.agenerateall;
  gendelete      := data.agendelete;
end;

(*
Ersetzt die Zeichenketten, die in dem Template mit @ bezeichnet sind. Dieses
können einfache Stringsersetzungen sein oder komplizierte Ersetzungen.
*)
function TORGenerator.replaceStrings(var w: TextFile; s :
                                     String; meta : TMeta) : String;
var
  st    : String;
  temp  : String;
  I     : Integer;
  attr  : PAtt;
  asso  : PAss;
  anz   : Integer;
begin
  anz := 0;
  st := s;
  result := st;

  while pos('@', st) > 0 do
  begin
    inc(anz);
    if anz > 10000 then
    begin
      // TODO
      exit;
    end;

    if pos('@USES', st) > 0 then
    begin
       temp := '';
       for I := 0 to meta.usesList.Count - 1 do
       begin
          temp := temp + ',' + meta.usesList.Strings[I];
       end;
       st := AnsireplaceStr(st, '@USES', temp);
    end;

    if pos('@UNITS', st) > 0 then
       st := AnsireplaceStr(st, '@UNITS', meta.unitname);

    if pos('@OBJ_NAME', st) > 0 then
       st := AnsireplaceStr(st, '@OBJ_NAME', meta.classname);

    if pos('@TABLE', st) > 0 then
       st := AnsireplaceStr(s, '@TABLE', meta.tablename);

    if pos('@ATTR_LIST', st) > 0 then
    begin
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w, '      ' + attr.name + ' : ' + attr.typ + ';');
      end;
      st := '%';
    end;

    if pos('@GEN_ASSOZIATIONEN', st) > 0 then
    begin
      WriteLn(w, '      // Assoziationen zu anderen Objekten');
      for I := 0 to meta.assList.Count - 1 do
      begin
         asso := meta.assList.Items[I];
         if (asso.typ = '1zu1') or (asso.typ = '1zu1revert')  then
         begin
            WriteLn(w, '      function get' + asso.bez + '() : ' + asso.obj + ';');
            if gendelete then
              WriteLn(w, '      procedure delete' + asso.bez + ';');
         end;
         if asso.typ = '1zuN'  then
            WriteLn(w, '      function get' + asso.bez + 'List() : TList;');
      end;
      st := '%';
    end;

    if pos('@GEN_GETTER', st) > 0 then
    begin
      if generateget then
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w, '      function get' + attr.name + '() : ' + attr.typ + ';');
      end;
      st := '%';
    end;

    if pos('@GEN_SETTER', st) > 0 then
    begin
      if generateset then
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w, '      procedure set' + attr.name + '(a' + attr.name + ' : ' + attr.typ + ');');
      end;
      st := '%';
    end;
    if pos('@GEN_IMPL_GETTER', st) > 0 then
    begin
      if generateget then
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w, 'function ' + meta.classname + '.get' + attr.name + '() : ' + attr.typ + ';');
         WriteLn(w, 'begin');
         WriteLn(w, '  result := ' + attr.name + ';');
         WriteLn(w, 'end;');
         WriteLn(w, '');
      end;
      st := '%';
    end;

    if pos('@GEN_IMPL_SETTER', st) > 0 then
    begin
      if generateset then
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w, 'procedure ' + meta.classname + '.set' + attr.name + '(a' + attr.name + ' : ' + attr.typ + ');');
         WriteLn(w, 'begin');
         WriteLn(w, '  ' + attr.name + ' := a' + attr.name + ';');
         WriteLn(w, 'end;');
         WriteLn(w, '');
      end;
      st := '%';
    end;

    if pos('@GEN_IMPL_ASSOZIATIONEN', st) > 0 then
    begin
      for I := 0 to meta.assList.Count - 1 do
      begin
         asso := meta.assList.Items[I];
         if asso.typ = '1zu1'  then
         begin
           WriteLn(w, '(*------------------------------------------------------------------------------');
           WriteLn(w, 'Get Methode für den Foreign Key ' + asso.FK);
           WriteLn(w, '------------------------------------------------------------------------------*)');

           WriteLn(w, 'function ' + meta.classname + '.get' + asso.bez + '() : ' + asso.obj + ';');
           WriteLn(w, 'var ' + asso.bez + ' : ' + asso.obj + ';');
           WriteLn(w, 'begin');
           Writeln(w, '  ' + asso.bez + ' := ' + asso.obj + '.Create();');
           Writeln(w, '  ' + asso.bez + '.loadPerFilter(''WHERE ' + asso.FK + ' = '' + IntToStr(id));');
           Writeln(w, '  result := ' + asso.bez + ';');
           WriteLn(w, 'end;');
           WriteLn(w, '');
           if gendelete then
           begin
             WriteLn(w, '(*------------------------------------------------------------------------------');
             WriteLn(w, 'Delete Methode für den Foreign Key ' + asso.FK);
             WriteLn(w, '------------------------------------------------------------------------------*)');
             WriteLn(w, 'procedure ' + meta.classname+ '.delete' + asso.bez + ';');
             WriteLn(w, 'var ' + asso.bez + ' : ' + asso.obj + ';');
             WriteLn(w, 'begin');
             WriteLn(w, '  ' + asso.bez + ' := get' + asso.bez+ '();');
             WriteLn(w, '  ' + asso.bez + '.delete;');
             WriteLn(w, 'end;');
             WriteLn(w, '');
           end;
         end else
         if asso.typ = '1zu1revert'  then
         begin
           WriteLn(w, '(*------------------------------------------------------------------------------');
           WriteLn(w, 'Get Methode für den Foreign Key ' + asso.FK);
           WriteLn(w, '------------------------------------------------------------------------------*)');

           WriteLn(w, 'function ' + meta.classname + '.get' + asso.bez + '() : ' + asso.obj + ';');
           WriteLn(w, 'var ' + asso.bez + ' : ' + asso.obj + ';');
           WriteLn(w, 'begin');
           Writeln(w, '  ' + asso.bez + ' := ' + asso.obj + '.Create();');
           Writeln(w, '  ' + asso.bez + '.loadPerFilter(''WHERE id = '' + intToStr('+ asso.FK + '));');
           Writeln(w, '  result := ' + asso.bez + ';');
           WriteLn(w, 'end;');
           WriteLn(w, '');
           if gendelete then
             begin
               WriteLn(w, '(*------------------------------------------------------------------------------');
               WriteLn(w, 'Delete Methode für den Foreign Key ' + asso.FK);
               WriteLn(w, '------------------------------------------------------------------------------*)');
               WriteLn(w, 'procedure ' + meta.classname+ '.delete' + asso.bez + ';');
               WriteLn(w, 'var ' + asso.bez + ' : ' + asso.obj + ';');
               WriteLn(w, 'begin');
               WriteLn(w, '  ' + asso.bez + ' := get' + asso.bez+ '();');
               WriteLn(w, '  ' + asso.bez + '.delete;');
               WriteLn(w, 'end;');
               WriteLn(w, '');
             end;
           end else
         if asso.typ = '1zuN'  then
         begin
           WriteLn(w, '(*------------------------------------------------------------------------------');
           WriteLn(w, 'Gibt eine Liste von ' + asso.obj + '-Objekten zurück');
           WriteLn(w, '------------------------------------------------------------------------------*)');
           WriteLn(w, 'function ' + meta.classname + '.get' + asso.bez + 'List() : TList;');
           WriteLn(w, 'var ' + asso.bez + ' : ' + asso.obj + ';');
           WriteLn(w, '  list : TList;');
           WriteLn(w, 'begin');
           Writeln(w, '  ' + asso.bez + ' := ' + asso.obj + '.Create();');
           Writeln(w, '  list := ' + asso.bez + '.loadListPerFilter(format(''' + asso.fk + '= %d'', [self.id]));');
           Writeln(w, '  result := list;');
           WriteLn(w, 'end;');
           WriteLn(w, '');
         end;
      end;
      st := '%';
    end;

    if pos('@CREATE_STRINGLIST', st) > 0 then
    begin
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w, '  stringList.Add(''' + attr.name + ''');');
      end;
      st := '%';
    end;

    if pos('@CREATE_FIELDSET', st) > 0 then
    begin
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w,
         '  obj.' + attr.name + ' := query.Fields.FieldByName(''' +
         attr.name + ''').' + attr.ass + ';'
         );
      end;
      st := '%'
    end;

    if pos('@CREATE_PARAMSET', st) > 0 then
    begin
      for I := 0 to meta.attrList.Count - 1 do
      begin
         attr := meta.attrList.Items[I];
         WriteLn(w,
         '  query.Params[paramInt].' + attr.ass + ' := ' + attr.name +
         '; inc(paramInt);'
         );
      end;
      st := '%'
    end;
  end;
  result := st;
end;

(*
  Schreibt das SQL Script zum Anlegen des Fachobjekts.
*)
function TORGenerator.writeSQLFile() : boolean;
var
  W        : TextFile; // Zu erzeugende Delphi-Datei
  meta     : TMeta;    // Metainformationen
  attr     : PAtt;     // Attributinformationen
  I        : Integer;  // Laufvariable
begin
  result := true;
  meta := analyseMETA();

  AssignFile(w, SQLDir + '\' + meta.unitname + '.sql');
  rewrite(w);

  Writeln(w, 'USE ' + database + ';');

  Writeln(w, 'CREATE TABLE ' + meta.unitname + ' (');

  // Default Felder füllen. Später mal administrierbar machen !!
  Writeln(w, 'Id int(11) NOT NULL auto_increment,');
  Writeln(w, 'createuser varchar(50) NOT NULL default 0,');
  Writeln(w, 'createdate varchar(50) NOT NULL default 0,');
  Writeln(w, 'modifyuser varchar(50) NOT NULL default 0,');
  Writeln(w, 'modifydate varchar(50) NOT NULL default 0,');
  Writeln(w, 'version int(11) NOT NULL default 0,');
  Writeln(w, 'uniqueString varchar(50) default NULL,');
  Writeln(w, 'active int(1) NOT NULL default 1,');

  for I := 0 to meta.attrList.Count - 1 do
  begin
      attr := meta.attrList.Items[I];
      Writeln(w, attr.name + ' ' + attr.db + ',');
  end;

  Writeln(w, 'PRIMARY KEY  (Id)');
  Writeln(w, ') ENGINE=InnoDB DEFAULT CHARSET=utf8 ');

  CloseFile(w);

  // Garbage Collection
  for I := 0 to meta.attrList.Count - 1 do
  begin
    attr := meta.attrList.Items[I];
    dispose(attr);
  end;
  meta.attrList.Free;
end;

(*
Schreibt die Delphi Unit für das Fachobjekt
*)
function TORGenerator.writeDelphiUnit() : boolean;
var
  F        : TextFile; // Template
  W        : TextFile; // Zu erzeugende Delphi-Datei
  s        : String;   // Hilfsstring
  meta     : TMeta;    // Metadaten
  attr     : PAtt;     // Attributdaten
  ass      : PAss;     // Assoziationsdaten
  I        : Integer;  // Laufvariable
begin
  result := true;
  s := templatename;
  meta := analyseMETA();

  AssignFile(w, unitdir + '\' + meta.unitname + '.pas');
  rewrite(w);

  AssignFile(f, s);
  reset(f);
  while not EOF(f) do
  begin
    ReadLn(f, s);
    s := replaceStrings(w, s, meta);
    if s <> '%' then
       WriteLn(w, s);
  end;
  CloseFile(f);
  CloseFile(w);
  for I := 0 to meta.attrList.Count - 1 do
  begin
    attr := meta.attrList.Items[I];
    dispose(attr);
  end;
  meta.attrList.Free;
  for I := 0 to meta.assList.Count - 1 do
  begin
    ass := meta.assList.Items[I];
    dispose(ass);
  end;
  meta.assList.Free;
  meta.usesList.Free;
end;



(*
Endlicher Automat, der die Metadatei ausliest und struktuiert ist das
Record TMeta ablegt. Die Daten werden sowohl für die Generierung der
Delphi-Unit als auch für das SQL benötigt.
*)
function TORGenerator.analyseMETA() : TMeta;
const
  stBegin = 0;
  stunit  = 1;
  stclass = 2;
  sttable = 3;
  stAttr  = 4;
  stAttrK = 5;
  stAsso  = 6;
  stAssoK = 7;
  stEnd   = 8;
var
  state    : Integer;  // State der Statemaschine
  I        : Integer;  // Laufvariabel über alle Textdateizeilen
  s        : String;   // Eine Zeile in der Textdatei
  metaData : TMeta;    // Metadaten in struktuierter Form
  pattr    : PAtt;     // Zeiger auf ein Attributset
  passo    : PAss;     // Zeiger auf ein Attributset
  position : Integer;  // Position eines Teilstrings
  subStr   : String;   // Ein Teilstring
  attList  : TList;    // Attributliste
  assList  : TLIst;    // Assoziationsliste
begin
  state := stBegin;
  METAlist := leseDatei(metadir + '\' + metafile);
  attList := TList.Create();
  assList := TLIst.Create();
  metadata.usesList := TStringList.Create();

  for I := 0 to METAList.Count - 1 do
  begin
    s := METAlist.Strings[I];
    Trim(s);
    if AnsiStartsStr('begin definition', s) then
    state := stbegin;
    if AnsiStartsStr('begin attribute', s) then
    state := stAttr;
    if AnsiStartsStr('begin assoziation', s) then
    state := stAsso;
    if AnsiStartsStr('end definition', s) then
    state := stEnd;
    if copy(s, 1, 1) <> '#' then
    case state of
    stbegin: state := stunit;
    stunit:
      begin
        if AnsiStartsStr('unit', s) then
        begin
          Delete(s, 1, 5);
          Trim(s);
          metaData.unitname := s;
        end;
        state := stclass;
      end;
    stclass:
      begin
        if AnsiStartsStr('class', s) then
        begin
          Delete(s, 1, 6);
          Trim(s);
          metaData.classname := s;
        end;
        state := sttable;
      end;
    sttable:
      begin
        if AnsiStartsStr('table', s) then
        begin
          Delete(s, 1, 6);
          Trim(s);
          metaData.tablename := s;
        end;
        state := stAttr;
      end;
    stAttr: state := stAttrK;
    stAttrK:
      begin
        if AnsiStartsStr('end attribute', s) then
        state := stAsso else
        begin
          pAttr := new(PAtt);
          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAttr.name := subStr;
          delete(s, 1, position);

          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAttr.typ := subStr;
          delete(s, 1, position);

          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAttr.ass := subStr;
          delete(s, 1, position);
          pAttr.db := s;

          attList.Add(pAttr);
        end;
      end;
    stAsso: state := stAssoK;
    stAssoK:
      begin
        if AnsiStartsStr('end assoziation', s) then
        state := stEnd else
        begin
          pAsso := new(PAss);
          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAsso.typ := subStr;
          delete(s, 1, position);

          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAsso.bez := subStr;
          delete(s, 1, position);

          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAsso.obj := subStr;
          delete(s, 1, position);

          position := pos(':', s);
          subStr := copy(s, 1, position - 1);
          pAsso.FK := subStr;
          delete(s, 1, position);

          assList.Add(pAsso);
          metadata.usesList.Add(s);
        end;
      end;
    stEnd:
      begin
        metaData.attrList := attList;
        metaData.assList  := assList;
      end;
    end;
  end;
  result := metaData;
end;

function TORGenerator.leseDatei(filename : String) : TStringList;
var
  list    : TStringList;
begin
  list := TStringList.Create();
  list.LoadFromFile(filename);
  result := list;
end;

end.
