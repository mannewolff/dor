(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit processOutput;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, Vector, generatorContext;

type
  (*
     Schnittstelle:
   *)
  TProcessOutput = class(TCommand)
  public
    function execute(AContext: TContext) : boolean; override;
  end;


implementation


function TProcessOutput.execute(AContext: TContext) : boolean;
var
  ctx             : TGeneratorContext;
  I, J            : Integer;
  List            : TStringList;
  item            : String;
  output          : TStrVector;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandProcessOutput then exit;

  For I := 1 to 10 do
  begin
    output := ctx.global_workingmap[I];
    if output = nil then continue;

    List := TStringList.Create();
    if (ctx.global_concat[I]) then
    begin
       List.LoadFromFile(ctx.global_outfile[I]);
       List.Add(ctx.global_comment + ' Am ' + DateToStr(now()) + ' Hinzugefügt.');
    end;
    For J := 0 to output.Size - 1 do
    begin
      item := output.GetString(J);
      List.Add(item);
    end;

    // Schreiben nur, wenn once == false oder bei once == true, wenn die
    // Datei noch nicht existiert

    if (not FileExists(ctx.global_outfile[I])) or
       (FileExists(ctx.global_outfile[I]) and (ctx.global_once[i] = false))
    then
       List.SaveToFile(ctx.global_outfile[I]);
    List.Free();
  end;
  result := false;
end;

initialization
  RegisterClass(TProcessOutput);

end.

