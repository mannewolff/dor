(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit processInline;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  Analyserobjects,
  Command,
  Context,
  GeneratorContext,
  Vector;

type
  TProcessInline = class(TCommand)
  private
    ctx          : TGeneratorContext;  // Kontext
    input        : TStrVector;         // Inputdatei
    output       : TStrVector;         // Outputdatei
    actLine      : Integer;            // Aktuelle Zeilennummer
    workstring   : String;             // Aktuelle Zeile
    typstring    : String;             // Einschränkung @type[]

    function  evaluateIfStatement(tokenpart : String;
                                  declarationdata : TDeclarationData) : boolean;
    function  evaluatePosStatement(tokenpart : String;
                                  declarationdata : TDeclarationData) : boolean;
    function  processForEach(statements : TStrVector) : Boolean;
    function  processForFile(statements : TStrVector) : Boolean;
    function  processCopy(ActFile, tempstring : String) : Boolean;
    procedure processStatementsForEach(declaration     : TDeclaration;
                                       declarationdata : TDeclarationData;
                                       statements: TStrVector);
    function  getDeclaration(ident : String) : TDeclaration;
  public
    function execute(AContext: TContext) : boolean; override;
  end;

implementation

uses
  Analysertools,
  DCL_intf,
  strobj,
  strUtils;

(*
  Gibt das Deklarationsobjekt zurück, welches zu dem gesuchten
  Iterationstyp passt z.B. @Assoziation oder @Attribute
*)
function TProcessInline.getDeclaration(ident : String) : TDeclaration;
var
  I : Integer;
  D : TDeclaration;
begin
  result := nil;
  for I := 0 to ctx.analyser_defines.Size - 1 do
  begin
    D := TDeclaration(ctx.analyser_defines.GetObject(I));
    if D.Itterable = ident then
    begin
      result := D;
      break;
    end;
  end;
end;

function  TProcessInline.evaluateIfStatement(tokenpart : String;
                                  declarationdata : TDeclarationData) : boolean;
var
  token         : TStrVector;
  aoperator     : String;
  expression    : String;
  left, right   : String;
  Value         : String;        // Wert eines konkreten Attributs (@Typ) für [] Einschr.
  aset          : IStrSet;       // Keyset der Datenhashmap für eine Datendeklaration
  it            : IStrIterator;  // Iterator auf dem Keyset
  key           : string;        // Schlüssel der Hashmap
  apos          : Integer;
begin
  result := false;

  aoperator := trim(tokenpart);
  delete(aoperator, 1, 3);
  aoperator := trim(aoperator);

  expression := extractTokenIntoBraces(aoperator, -1, '(', ')');
  token := tokenize(expression, ' ');

  left := token.GetString(0);
  aoperator := token.GetString(1);
  right := token.GetString(2);
  FreeAndNil(token);

  // Auflösen von Left
  apos := pos('.', left);
  left := '@' + copy(left, apos + 1, length(left) - apos);


  aset := declarationdata.Data.keySet;
  It   := aset.First();
  while It.HasNext do
  begin
    key  := It.Next;
    if key = left then
    begin
      Value := TString(declarationdata.Data.GetValue(key)).getString();
      break;
    end;
  end;

  if right = '''''' then right := '';
  if right = '""' then right := '';

  if aoperator = '<>' then result := (Value <> right);
  if aoperator = '==' then result := (Value = right);
  if aoperator = '=' then result := (Value = right);
  if aoperator = '<' then result := (Value < right);
  if aoperator = '>' then result := (Value > right);
end;

function  TProcessInline.evaluatePosStatement(tokenpart : String;
                                  declarationdata : TDeclarationData) : boolean;
var
  token         : TStrVector;
  aoperator     : String;
  expression    : String;
  Value         : String;        // Wert eines konkreten Attributs (@Typ) für [] Einschr.
  aset          : IStrSet;       // Keyset der Datenhashmap für eine Datendeklaration
  it            : IStrIterator;  // Iterator auf dem Keyset
  key           : string;        // Schlüssel der Hashmap
  apos          : Integer;

  xxx, yyy      : String;
  y, z          : Integer;
  tmp           : String;

begin
  result := false;

  aoperator := trim(tokenpart);

  // Wir suchen ein @if (copy xxx  y  z == yyy));
  delete(aoperator, 1, pos('(', aoperator) - 1);
  //(copy(xxx y z == yyy);
  aoperator := trim(aoperator);
  apos := pos('copy', aoperator);
  delete(aoperator, 1, apos + 4);
  aoperator := trim(aoperator);
  // (xxx  y  z == yyy)
  if pos('(', aoperator) > 0 then
  delete(aoperator, pos('(', aoperator), 1);
  if pos(')', aoperator) > 0 then
  delete(aoperator, pos(')', aoperator), 1);
  // xxx  y  z == yyy
  token := tokenize(Trim(aoperator), ' ');

  xxx := token.GetString(0);
  y := StrToInt(token.GetString(1));
  z := StrToInt(token.GetString(2));
  expression := token.GetString(3);
  yyy := token.GetString(4);
  FreeAndNil(token);

  apos := pos('.', xxx);
  xxx := '@' + copy(xxx, apos + 1, length(xxx) - apos);

  aset := declarationdata.Data.keySet;
  It   := aset.First();
  while It.HasNext do
  begin
    key  := It.Next;
    if key = xxx then
    begin
      Value := TString(declarationdata.Data.GetValue(key)).getString();
      break;
    end;
  end;

  if (expression = '==') then
  result := copy(value, y, z) = yyy;
  if (expression = '<>') then
  result := copy(value, y, z) <> yyy;
end;

(*
  Processiert alle @ForEach Statements (innerhalb einer Deklaration) mit einer
  bestimmen Datendeklaration.
*)
procedure TProcessInline.processStatementsForEach(declaration     : TDeclaration;
                                                  declarationdata : TDeclarationData;
                                                  statements: TStrVector);
var
  I          : Integer;
  Value      : String;       // Wert eines konkreten Attributs (@Typ) für [] Einschr.
  aset       : IStrSet;      // Keyset der Datenhashmap für eine Datendeklaration
  it         : IStrIterator; // Iterator auf dem Keyset
  key        : string;       // Schlüssel der Hashmap
  mkey       : string;       // Getrimmter Schlüssel
  tempstring : String;       // Arbeitsstring des Statements
  ifres      : Boolean;      // If Statement ist entweder true oder false
  cs, a, b   : String;       // copystring, Kopie einer Zeile
begin
  Value := '';
  ifres := true;

  // Gucken ob eine Type[] Einschränkung vorhanden ist.
  if declarationdata.data.ContainsKey(ctx.global_type) then
      value := TString(declarationdata.Data.GetValue(ctx.global_type)).getString();

  if value <> '' then
  begin
    tempstring := statements.GetString(0);
    typstring := extractTokenIntoBraces(tempstring, 1, '[', ']');
  end;

  For I := 1 to statements.Size -2 do // ohne @foreach und @end
  begin
    tempstring := statements.GetString(I);
    cs := trim(tempstring);

    // Statements nur adden, wenn sie nicht durch ein
    // @if ausgeschlossen werden.

    if (pos('@if', AnsiLowerCase(cs)) > 0) and
    (cs[1] = '@')  then
    begin
      if (pos('and', cs) > 0) then
      begin
        a := copy(cs, 1, pos('and', cs) - 1);
        a := copy(a, pos('(', a) + 1, length(a) - pos('(', a));
        b := copy(cs, pos('and', cs) + 3, length(cs) - pos('and', cs) - 3);
        ifres := evaluateIfStatement('@if'+ a, declarationdata) and
                  evaluateIfStatement('@if'+b, declarationdata);
        continue;
      end else
      if (pos('copy', cs) > 0) then
      begin
        ifres := evaluatePosStatement(tempstring, declarationdata);
        continue;
      end else
      begin
        ifres := evaluateIfStatement(tempstring, declarationdata);
        continue;
      end;
    end else
    if (pos('@endif', AnsiLowerCase(cs)) > 0) and (cs[1] = '@')  then
    begin
      ifres := true;
      continue;
    end else

    if ifres then
    if (Value = '') or (typstring = Value) or (typstring = '') then
    begin
      aset := declarationdata.Data.keySet;
      It   := aset.First();
      while It.HasNext do
      begin
        key  := It.Next;
        mkey := key;
        Delete(mkey, 1, 1);
        mkey  := declaration.Itterable + '.' + mkey;
        Value := TString(declarationdata.Data.GetValue(key)).getString();
        tempstring := AnsiReplaceStr(tempstring, mkey, Value);
      end;
      output.Add(tempstring);
     end;
  end;
end;

function getAllFiles(filepattern : String) : TStrVector;
var
  return     : TStrVector;
  searchpath : String;
  searchname : String;
  datei      : TSearchRec;
begin
  searchpath := ExtractFileDir(filepattern);
  searchname := ExtractFileName(filepattern);
  return := TStrVector.Create();

  if searchpath[length(searchpath)] <> '\' then
    searchpath := searchpath + '\';

  If FindFirst(searchpath + searchname, fadirectory, datei) = 0 then
  begin
    return.Add(searchpath + datei.name);
    while Findnext(datei) = 0 do
      If datei.Attr and fadirectory = 0 then
         return.Add(searchpath + datei.name);
    FindClose(datei);
  end;
  result := return;
end;

function TProcessInline.processCopy(ActFile, tempstring : String) : Boolean;
var
  list      : TStringList;
  I         : Integer;
  token     : TStrVector;
  beg, ende : String;
  line      : String;
  outline   : Boolean;
begin
  result := false;
  token := tokenize(tempstring, ' ');
  beg  := token.GetString(1);
  ende := token.GetString(2);
  token.Free();

  outline := false;

  list := TStringList.Create();
  list.LoadFromFile(ActFile);

  For I := 0 to list.Count - 1 do
  begin
    line := list.Strings[I];
    if pos(beg, line) > 0 then
       outline := true;
    if pos(ende, line) > 0 then
       outline := false;
    if outline then
    begin
      output.Add(line);
    end;
  end;
  list.Free();
end;

function TProcessInline.processForFile(statements : TStrVector) : Boolean;
var
  token       : TStrVector;
  I, files    : Integer;
  tokenpart   : String;
  filepattern : String;
  filelist    : TStrVector;
  tempString  : String;
  actFile     : String;
  t           : String;
begin
  result  := true;
  tokenpart := statements.GetString(0);

  token := tokenize(tokenpart, '$');
  if token.Size < 3 then
  begin
    // Fehler nicht genug Parameter
    result := makeErrorAnalyser(ctx, workstring,
              '@forfile - Nicht genügend Parameter.', actLine - 1, '');
    exit;
  end;

  filepattern := token.GetString(1);
  filepattern := Trim(AnsiReplaceStr(filepattern, '''', ' '));

  token.Free();

  // Für alle Dateien prozessieren
  filelist := getAllFiles(filepattern);

  for files := 0 to filelist.Size - 1 do
  begin
    ActFile := filelist.GetString(files);
    t := ExtractFileName(ActFile);
    while t[length(t)] <> '.' do
      delete(t,length(t), 1);
    delete(t,length(t), 1);
    For I := 1 to statements.Size -2 do // ohne @forfile und @end
    begin
      tempstring := statements.GetString(I);
      if pos('@copy', tempstring) > 0 then
      begin
         processCopy(ActFile, tempstring);
      end else
      begin
        tempstring := AnsiReplaceStr(tempstring, '@FILE_REAL_NAME', t);
        output.Add(tempstring);
      end;
    end;
  end;
end;

// Version 2.0 -> Keine Schachtelung möglich, straight down
function TProcessInline.processForEach(statements : TStrVector) : Boolean;
var
  token     : TStrVector;
  decl      : TDeclaration;
  I         : Integer;
  attvec    : TDeclarationdata;
  tokenpart : String;
begin
  result  := true;

  tokenpart := statements.GetString(0);

  // String in Bestandteile zerlegen.
  token := tokenize(tokenpart, ' ');
  if token.Size < 3 then
  begin
    // Fehler nicht genug Parameter
    result := makeErrorAnalyser(ctx, workstring,
              '@foreach - Nicht genügend Parameter.', actLine - 1, '');
    exit;
  end;

  // Datenstruktur laden
  decl := getDeclaration(token.GetString(1));
  if decl = nil then
  begin
    // Fehler nicht genug Iterable nicht definiert
    result := makeErrorAnalyser(ctx, workstring,
              '@foreach - Datenstruktur ' + token.GetString(1) +
              ' wurde nicht definiert.', actLine - 1, '');
    exit;
  end;

  // Für jedes Iterable muss prozessiert werden.
  for I := 0 to decl.data.Size - 1 do
  begin
    attvec := TDeclarationData(decl.data.GetObject(I));
    processStatementsForEach(decl, attvec, statements);
  end;
  token.Free();
end;

function TProcessInline.execute(AContext: TContext) : boolean;
var
  status          : boolean;
  loop            : Integer;
  statements      : TStrVector;
  cs              : String;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandProcessInline then exit;

  status := ctx.isStatus('AnalyseSystems');
  if not status then raise Exception.Create('Erwarte AnalyseSystems.');
  status := ctx.isStatus('AnalyseDeclarationsData');
  if not status then raise Exception.Create('Erwarte AnalyseGlobals.');

  for loop := 1 to 10 do
  begin
    input  := ctx.global_workingmap[loop];
    if input = nil then continue;

    output := TStrVector.Create();
    actLine := 0;

    while actLine < input.Size do
    begin
      workstring := input.GetString(actLine);
      cs := trim(Workstring);


      if (pos('@foreach', AnsiLowerCase(cs)) > 0) and
         (cs[1] = '@')  then
      begin
        // Das gesamte Statement von @foreach - @end aufsammeln
        statements := TStrVector.Create();
        while (pos('@endforeach', AnsiLowerCase(workstring)) = 0) do
        begin
           statements.Add(workstring);
           inc(actLine);
           workstring := input.GetString(actLine);
        end;
        statements.Add(workstring);
        processForEach(statements);
        FreeAndNil(statements);
      end else

      if (pos('@forfile', AnsiLowerCase(cs)) > 0) and
         (cs[1] = '@')  then
      begin
        // Das gesamte Statement von @forfile - @end aufsammeln
        statements := TStrVector.Create();
        while (pos('@endforfile', AnsiLowerCase(workstring)) = 0) do
        begin
           statements.Add(workstring);
           inc(actLine);
           workstring := input.GetString(actLine);
        end;
        statements.Add(workstring);
        if not ctx.filesprocessed then
        begin
          processForFile(statements);
          ctx.filesprocessed := true;
        end;
        FreeAndNil(statements);
      end else
      output.Add(workstring);
      inc(actLine);
    end;
    FreeAndNil(input);
    ctx.global_workingmap[loop] := output;
  end;

  ctx.setStatus('ProcessInline');
  result := false;
end;

initialization
  RegisterClass(TProcessInline);
end.
