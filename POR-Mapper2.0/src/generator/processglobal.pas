(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit processglobal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, Hashmap, Vector;

type
  TProcessGlobal = class(TCommand)
  private
    globals      : TStrHashMap;
    input        : TStrVector;
    output       : TStrVector;
  public
    function execute(AContext: TContext) : boolean; override;
  end;


implementation

uses
  strobj, strUtils, DCL_intf, generatorContext;

function TProcessGlobal.execute(AContext: TContext) : boolean;
var
  I          : Integer;
  loop       : Integer;
  workstring : String;
  key, value : String;
  aset       : IStrSet;
  it         : IStrIterator;
  ctx        : TGeneratorContext;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandProcessGlobal then exit;

  globals := ctx.analyser_globals;

  for loop := 1 to 10 do
  begin
    if ctx.global_noclearing[loop] = true then continue;
    input  := ctx.global_workingmap[loop];
    if input = nil then continue;
    output := TStrVector.Create();
    for I := 0 to input.Size - 1 do
    begin
      workstring := input.GetString(I);
      aset := globals.KeySet;
      It := aset.First();
        while It.HasNext do
        begin
          key := It.Next;
          value := TString(globals.GetValue(key)).getString();
          workstring := AnsiReplaceStr(workstring, key, value);
        end;
       output.Add(workstring);
    end;
    FreeAndNil(input);
    ctx.global_workingmap[loop] := output;
  end;
  result := false;
end;

initialization
  RegisterClass(TProcessGlobal);

end.

