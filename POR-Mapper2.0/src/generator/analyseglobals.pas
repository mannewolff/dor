(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analyseglobals;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, GeneratorContext, Hashmap, Vector;

type
  (*
     Benötigte Commands: ---

     Schnittstelle:
     in    : analyser_filename
     inout : analyser_metadata TStrHashMap Werden bei Bedarf erzeugt.
   *)
  TAnalyseGlobals = class(TCommand)
  public
    procedure prepareGlobals(ctx: TGeneratorContext; line: string);
    function  execute(AContext: TContext) : boolean; override;
  end;


implementation

uses
  analysertools, strobj;

(*
  --> Analysiert eine Zeile und schreibt die globalen key-value Paare in
      die Map aglobals
*)
procedure TAnalyseGlobals.prepareGlobals(ctx: TGeneratorContext; line: string);
var
  tokens     : TStrVector; // Temporär um eine Zeile zu tokenisieren
  key, Value : string;     // Schlüssel- / Wertepaar
  trenner    : Char;
begin

  if ctx.analyser_globals = nil then
    exit;

  trenner := ctx.global_assignment;

  try
    tokens := tokenize(line, trenner);
    if tokens.Size < 2 then
      exit;

    key := tokens.GetString(0);
    if key[1] <> '@' then
      exit;

    Value := tokens.GetString(1);

    ctx.analyser_globals.PutValue(key, TString.Create(Value));

  finally
    FreeAndNil(tokens);
  end;
end;


function TAnalyseGlobals.execute(AContext: TContext) : boolean;
var
  ctx        : TGeneratorContext;
  metadaten  : TStrVector;
  proceed    : Boolean;
  fname      : String;
  I          : Integer;
  line       : String;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandAnalyseGlobals then exit;

  proceed := false;

  // Gucken, ob die Metadaten schon geladen wurden.
  metadaten := ctx.analyser_metadata;
  if metadaten = nil then
  begin
    fname    := ctx.analyser_filename;
    ctx.analyser_metadata := loadfile(fname, nil,ctx.global_comment);
  end;

  // Gucken ob es schon Globals gibt
  if ctx.analyser_globals = nil then
     ctx.analyser_globals := TStrHashmap.Create();

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin

    line := ctx.analyser_metadata.GetString(I);
    // Interessant sind nur die *globals Sektionen
    if pos('*globals', line) > 0 then
      proceed := true else

    if (line <> '') and (line[1]= '*') then proceed := false;

    if ((line <> '') and
        (line[1] <> '*') and
        (line[1] <> ctx.global_comment) and
        (proceed)) then
      prepareGlobals(ctx, line);
  end;
  ctx.setStatus('AnalyseGlobals');
  result := false;
end;

initialization
  RegisterClass(TAnalyseGlobals);

end.

