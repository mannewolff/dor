(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
 *)
unit cleanup;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, generatorContext, Vector;

type
  TCleanup = class(TCommand)
  private
    function testGlobal(ctx : TGeneratorContext; s : String) : boolean;
  public
    function execute(AContext: TContext) : boolean; override;
  end;


implementation

uses
  strUtils, strobj, DCL_intf, HashMap;

(**
  * Testet, ob eine globale Variable vom Wert s gesetzt ist. Gesetzt ist
  * sie genau dann, wenn sie
  *
  * a. definiert wurde
  * b. einen Wert hat (irgend einen wert)
  *
  * Nicht gesetzt ist sie, wenn sie explizit den Wert false hat.
  *)
function TCleanup.testGlobal(ctx : TGeneratorContext; s : String) : boolean;
var
  hm      : TStrHashMap;
  aset    : IStrSet;
  it      : IStrIterator;
  key     : string;
  Value   : string;
  search  : string;
begin
  search := '@' + Trim(s);
  result := false;
  hm := ctx.analyser_globals;
  aset := hm.keySet;
  It   := aset.First();
  while It.HasNext do
  begin
    key  := It.Next;
    Value := TString(hm.GetValue(key)).getString();
    if key = search then
    begin
      if Value <> '' then result := true;
      // Explizit false gilt auch als nicht gesetzt.
      if Value = 'false' then result := false;
      break;
    end;
  end;
end;

(**
  * Execute Methode dieses Commands.
  *)
function TCleanup.execute(AContext: TContext) : boolean;
var
  I,J          : Integer;
  ctx          : TGeneratorContext;
  line         : String;
  output       : TStrVector;
  input        : TStrVector;
  ws           : Char;
  p            : Integer;
  proceed      : boolean;
  proceedthis  : boolean;
  cs           : String;
  concat       : boolean;
  concatstring : String;
begin
  proceed      := true;
  proceedthis  := true;
  concat       := false;
  concatstring := '';

  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandCleanup then exit;

  For I := 1 to 10 do
  begin
    input := ctx.global_workingmap[I];
    if input = nil then continue;
    output := TStrVector.Create();
    ws     := ctx.global_whitespace;
    for J := 0 to input.Size - 1 do
    begin
      line := input.GetString(J);

      // Template-Kommentare ausblenden
      if ctx.global_noclearing[I] = false then
        if (line <> '') and (line[1] = ctx.global_templatecoment) then
          continue;

      // ifdef und ifndef behandeln
      cs := Trim(line);

      if (pos('@ifdef', AnsiLowerCase(cs)) > 0) then
      begin
        proceedthis := false;
        p := pos('@ifdef', AnsiLowerCase(cs));
        if testGlobal(ctx, copy(cs, p+6, length(cs) - 6)) then
           proceed := true else
           proceed := false;
      end;

      if (pos('@ifndef', AnsiLowerCase(cs)) > 0) then
      begin
        proceedthis := false;
        p := pos('@ifndef', AnsiLowerCase(cs));
        if not testGlobal(ctx, copy(cs, p+7, length(cs) - 7)) then
           proceed := true else
           proceed := false;
      end;

      if (pos('@enddef', AnsiLowerCase(cs)) > 0) then
      begin
        proceed := true;
        proceedthis := false;
      end;

      // ^ Leerbegrenzer
      begin
        p := pos('^',line);
        if p > 0 then
        begin
          delete(line, 1, p);
        end;
      end;

      // @whitespace
      if ctx.global_noclearing[I] = false then
        if ctx.global_whitespacescape = 'true' then
          line := AnsiReplaceStr(line, ws, ' ');

      // @tilde
      p := pos('~',line);
      while p > 0 do
      begin
        line[p+1] := UpperCase(line[p+1])[1];
        delete(line, p, 1);
        p := pos('~',line);
      end;

      // @date @time und @datetime
      line := AnsiReplaceStr(line, '@datetime', DateTimeToStr(now()));
      line := AnsiReplaceStr(line, '@date', DateToStr(now()));
      line := AnsiReplaceStr(line, '@time',  TimeToStr(now()));

      // @ die jetzt noch übriggeblieben sind
      if ctx.global_noclearing[I] = false then
      line := AnsiReplaceStr(line, '@',  ctx.global_whitespace);

      // concatzeichen |
      if ctx.global_noclearing[I] = false then
      begin
        if (cs <> '') and (cs[length(cs)] = '|') then
        begin
          concat := true;
          concatstring := concatstring + line;
        end else
          concat := false;
      end;

      if (concatstring <> '') and not concat then
      begin
        concatstring := concatstring + line;
        concatstring := AnsiReplaceStr(concatstring, '|', '');
        if (proceed and proceedthis) then
        output.Add(concatstring);
        concatstring := '';
      end else
      begin
        if (proceed and proceedthis and not concat) then
         output.Add(line);
      end;
      proceedthis := true;
    end;
    input.Free();
    ctx.global_workingmap[I] := output;
  end;
  result := false;
end;

initialization
  RegisterClass(TCleanup);

end.

