(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *

 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit analysertools;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, GeneratorContext, Vector;

function  tokenize(const s : String; const delemitter : char) : TStrVector;
function  findValue(ctx: TGeneratorContext; key : String) : String;
function  loadfile(const filename : String; buffer : TStrVector;
                   comment : char) : TStrVector;
procedure findTemplateComment(ctx: TGeneratorContext);
procedure findComment(ctx: TGeneratorContext);
procedure findWhitespace(ctx: TGeneratorContext);
procedure findType(ctx: TGeneratorContext);
procedure findTypeSeparator(ctx: TGeneratorContext);
procedure findassigmentOperator(ctx: TGeneratorContext);
procedure findWhitespaceEscape(ctx: TGeneratorContext);
procedure findRootDir(ctx: TGeneratorContext; default: String);
procedure splitparameter(const parameter : String; var anweisung,
                         attribut : String);
function  makeErrorAnalyser(ctx : TGeneratorContext;
                            const aline: String;
                            const amessage: String;
                            const line: Integer;
                            const fname: String) : Boolean;
function extractTokenIntoBraces(const workstring: String;
                                const position  : Integer;
                                const inbrace   : String;
                                const outbrace  : String) : String;

procedure printStrVector(const lst : TStrVector; const fname : String);
function transformTrueFalse(s : String) : Boolean;
function format(s: string; len: integer; justifiy: integer; c : char): string;



implementation

(**
 * Extrahiert einen Token, der zwischen zwei Klammern steht z.B.
 * (hallo) == hallo
 * [Welt]  == Welt
 * @param workstring: Der String, der zu analysieren ist
 * @param position: Welcher Token gesucht wird (-1 ist ersten Vorkommen im String)
 * @param inbrace: Klammer auf
 * @param outbrace: Klammer zu
 *)
function extractTokenIntoBraces(const workstring: String;
                                const position  : Integer;
                                const inbrace   : String;
                                const outbrace  : String) : String;
var
  token       : TStrVector;
  tempString  : String;
  resString   : String;
  p1, p2      : Integer;
begin
    resString := '';
    token     := nil;
    if position > -1 then
    begin
      token := tokenize(workstring, ' ');
      tempstring := token.GetString(position);
    end else
      tempstring := workstring;

    p1 := pos(inbrace, tempstring);
    p2 := pos(outbrace, tempstring);
    if (p1 > 0) and (p2 > 0) then
      resString := copy(tempstring, p1 + 1, p2 - p1 - 1);
    if assigned(token) then FreeAndNil(token);
    result := resString;
end;

(**
 * Formatiert eine Zeile, in dem Leerzeichen bis zur geforderten Länge
 * eingefügt werden.
 * Justify 0 == linksbündig.
 * Justify 1 == rechtsbündig.
 *)
function format(s: string; len: integer; justifiy: integer; c : char): string;
var
  st: string;
  I:  integer;
begin
  st := s;
  if justifiy = 0 then
  begin
    for I := 0 to len - length(st) do
      st := st + c;
  end;
  if justifiy = 1 then
  begin
    for I := 0 to len - length(st) do
      st := c + st;
  end;
  Result := st;
end;


// True, wenn abgebrochen werden muss, False, wenn es nur ein kleiner Fehler ist
function  makeErrorAnalyser(ctx : TGeneratorContext; const aline: String;
                            const amessage: String; const line: Integer;
                            const fname: String) : Boolean;
var
  error : TError;
begin
  error := TError.Create();
  error.line := 'Fehler in Zeile ' + intToStr(line) + ': ' + aline;
  ctx.analyser_errors.Add(error);
  error := TError.Create();
  error.line := amessage;
  ctx.analyser_errors.Add(error);
  result := true;
end;

(**
 * Splittet eine Einschränkung in die Anweisung und dem Attribut.
 * Beispiel @package {notempty @global}
 * anweisung == notempty
 * attribut  == @global
 *)
procedure splitparameter(const parameter : String; var anweisung, attribut : String);
var
  s : String;
  p : Integer;
begin
  s := trim(parameter);
  if s[1] = '{' then delete(s, 1, 1);
  if s[length(s)] = '}' then delete(s, length(s), 1);
  p := pos(' ', s);
  anweisung := trim(copy(s, 1, p - 1));
  delete(s, 1, p);
  attribut := trim(s);
end;

(**
 * Für Tests: Schreibt einen StringVector in eine Datei
 *)
procedure printStrVector(const lst : TStrVector; const fname : String);
var
  list : TStringList;
  I    : Integer;
  s    : String;
begin
  list := TStringList.Create();
  for I := 0 to lst.Size - 1 do
  begin
    s := lst.GetString(I);
    list.Add(s);
  end;
  list.SaveToFile(fname);
  list.Free();
end;

(**
 * Zerlegt eine Zeile in seine Bestandteile. Der delemitter gibt an,
 * wie Token erkannt werden.
 * @param s: Der zu untersuchende String
 * @param delemitter: Das Trennzeichen
 * @result: Ein StringVector mit den einzelnen Token
 *)
function tokenize(const s : String; const delemitter : char) : TStrVector;
var
  scopy      : String;
  line       : TStrVector;
  temp       : String;
  I          : Integer;
begin
  line := TStrVector.Create();
  scopy := s;
  scopy := Trim(scopy);

  if ((scopy <> '') and (scopy[length(scopy)] <> delemitter)) then
     scopy := scopy + delemitter;

  while pos(delemitter, scopy) > 0  do
  begin
    I := pos(delemitter, scopy) - 1;
    temp := copy(scopy, 1, pos(delemitter, scopy) - 1);
    temp := trim(temp);
    if (temp <> delemitter) then
    line.add(temp);
    I := pos(delemitter, scopy);
    delete(scopy, 1, pos(delemitter, scopy));
  end;
  result := line;
end;

(**
 * Läd eine Datei und legt die Zeilen in einen Vector ab.
 *)
function loadfile(const filename : String;
                  buffer : TStrVector; comment : char) : TStrVector;
var
  vecar : TStrVector;
  I     : Integer;
  s     : String;
  path  : String;
  fname : String;
  sl    : TStringList;
  p     : Integer;
begin
  fname := filename;

  if comment = #0 then
    comment := '#';

  if (buffer = nil) then
    vecar := TStrVector.Create() else
    vecar := buffer;

  sl := TStringList.Create();
  begin
    sl.LoadFromFile(fname);
    For I := 0 to sl.Count -1 do
    begin
      s := sl.Strings[I];
      s := trim(s);
      begin
        p := pos(comment, s);
        if (p > 1) and
           (pos('@comment', AnsiLowerCase(s)) = 0) and
           (pos('@templatecomment', AnsiLowerCase(s)) = 0)
        then
        begin
          s := copy(s, 1, p-1);
        end;
        if (pos('@include', AnsiLowerCase(s)) > 0) then
        begin
          fname := copy(s, 9, length(s) - 8);
          fname := trim(fname);
          path := ExtractFileDir(filename);
          loadfile(path + '\' + fname, vecar, comment);
          continue;
        end;
        if (s <> '') and (s[1] <> comment) then
        vecar.add(s);
      end;
    end;
  end;
  sl.Free();
  result := vecar;
end;

(**
 * Findet einen Wert. Die Trennung zwischen Schlüssel und Wert wird in
 * ctx.global_assignment festgelegt.
 *)
function findValue(ctx: TGeneratorContext; key : String) : String;
var
  I         : integer;
  line      : string;
  p         : integer;
  trenner   : char;
  tokens    : TStrVector;
begin
  result    := '';
  trenner   := ctx.global_assignment;
  if trenner = '' then trenner := '=';

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin
    line := ctx.analyser_metadata.GetString(I);
    p    := pos(AnsiLowerCase(key), AnsiLowerCase(line));
    if p > 0 then
    begin
      try
        tokens := tokenize(line, trenner);
        if tokens.Size < 2 then
        continue;
        result := tokens.GetString(1);
        break;
      finally
        FreeAndNil(tokens);
      end;
    end;
  end;
end;

(*
  Findet das Kommentarzeichen (hinter dem reservierten Wort @comment) aus
  der Metabeschreibung. Es wird nur die erste Definition gefunden (immutable)
*)
procedure findComment(ctx: TGeneratorContext);
var
  charvalue: char;
  return   : String;
begin
  charValue := '#';
  return := findValue(ctx, '@comment');
  if  return <> '' then
  charValue := return[1];
  ctx.global_comment := charValue;
end;

(*
  Findet das Whitespacezeichen (hinter dem reservierten Wort @whitespace) aus
  der Metabeschreibung. Es wird nur die erste Definition gefunden (immutable)
*)
procedure findWhitespace(ctx: TGeneratorContext);
var
  charvalue: char;
  return   : String;
begin
  charValue := '_';
  return := findValue(ctx, '@whitespace');
  if  return <> '' then
  charValue := return[1];
  ctx.global_whitespace := charValue;
end;

(*
  Findet das Whitespacezeichen (hinter dem reservierten Wort @whitespace) aus
  der Metabeschreibung. Es wird nur die erste Definition gefunden (immutable)
*)
procedure findTemplateComment(ctx: TGeneratorContext);
var
  charvalue: char;
  return   : String;
begin
  charValue := '#';
  return := findValue(ctx, '@templatecomment');
  if  return <> '' then
  charValue := return[1];
  ctx.global_templatecoment := charValue;
end;

(*
  Findet das Typezeichen (hinter dem reservierten Wort @type[]) aus
  der Metabeschreibung. Es wird nur die erste Definition gefunden (immutable)
*)
procedure findType(ctx: TGeneratorContext);
var
  stringvalue: string;
  return   : String;
begin
  stringvalue := '@Typ';
  return := findValue(ctx, '@type[]');
  if  return <> '' then
  stringvalue := return;
  ctx.global_type := stringvalue;
end;

(**
 * Findet den Operator für Zuweisungen. Der Defaultwert ist das
 * Gleichheitszeichen (=)
 *)
procedure findassigmentOperator(ctx: TGeneratorContext);
var
  I         : integer;
  line      : string;
  p         : integer;
  trenner   : char;
  tokens    : TStrVector;
begin
  trenner   := ' ';

  for I := 0 to ctx.analyser_metadata.Size - 1 do
  begin
    line := ctx.analyser_metadata.GetString(I);
    p    := pos(AnsiLowerCase('@assignment'), AnsiLowerCase(line));
    if p > 0 then
    begin
      try
        tokens := tokenize(line, trenner);
        if tokens.Size < 2 then
        continue;
        ctx.global_assignment := tokens.GetString(1)[1];
        break;
      finally
        FreeAndNil(tokens);
      end;
    end;
  end;
end;

(**
 * Findet den Typseparator (hinter dem reservierten Wort @typeseparator) aus
 * der Metabeschreibung. Es wird nur die erste Definition gefunden (immutable)
 *)
procedure findTypeSeparator(ctx: TGeneratorContext);
var
  charvalue: char;
  return   : String;
begin
  charValue := ':';
  return := findValue(ctx, '@typeseparator');
  if  return <> '' then
  charValue := return[1];
  ctx.global_typeseparator := charValue;
end;

(**
 * Findet das Rootdir (hinter dem reservierten Wort @rootdir) aus
 * der Metabeschreibung. Es wird nur die erste Definition gefunden (immutable)
 *)
procedure findRootDir(ctx: TGeneratorContext; default: String);
var
  StringValue  : String;
begin
  StringValue:= default;

  if StringValue[Length(StringValue)] <> '\' then
     StringValue := StringValue + '\';
  ctx.global_rootpath := StringValue;
end;

(**
 * Findet den whitespaceescape Bezeichner (true, false) der angibt, ob
 * whitespaces mit dem Generatoroperator (default @) escaped werden können.
 *)
procedure findWhitespaceEscape(ctx: TGeneratorContext);
var
  stringvalue: string;
  return   : String;
begin
  stringvalue := '';
  return := findValue(ctx, '@whitespaceescape');
  if  return = '' then
  stringvalue := 'true';
  ctx.global_whitespacescape := stringvalue;
end;

(**
  * Setzt den String true, false, wahr, falsch, ja, nein in einen Bool'schen
  * Wert um.
  *)
function transformTrueFalse(s : String) : Boolean;
begin
  result := true;
  if AnsiLowerCase(s) = 'true' then result := true;
  if AnsiLowerCase(s) = 'wahr' then result := true;
  if AnsiLowerCase(s) = 'ja' then result := true;
  if AnsiLowerCase(s) = 'false' then result := false;
  if AnsiLowerCase(s) = 'falsch' then result := false;
  if AnsiLowerCase(s) = 'nein' then result := false;
end;


end.

