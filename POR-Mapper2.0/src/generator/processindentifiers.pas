(* --------------------------------------------------------------------------

   Pointers - Generator (PGEN)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: analysesystems.pas,v 1.3 2010/03/05 17:23:44 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers - Generator                            *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit processIndentifiers;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Command, Context, generatorContext;

type
  TProcessIdentifiers = class(TCommand)
  private
    procedure processFilenames(const ctx: TGeneratorContext);
  public
    function execute(AContext: TContext) : boolean; override;
  end;

implementation

uses
  strobj, strUtils, Hashmap, DCL_intf;

procedure TProcessIdentifiers.processFilenames(const ctx: TGeneratorContext);
var
  I             : Integer;
  ext           : String;
  name          : String;
  path          : String;
  outfilename   : String;
  metafilename  : String;
  globals       : TStrHashmap;
  line          : String;
  aset          : IStrSet;
  it            : IStrIterator;
  key           : String;
  value         : TString;
  st            : String;
begin
  metafilename := ctx.analyser_filename;
  if metafilename = '' then exit; // Analyser kann nicht arbeiten

  path := ExtractFilePath(metafilename);
  name := ExtractFileName(metafilename);

  while name[Length(name)] <> '.' do
    name := copy(name, 1, length(name) - 1);

  name := copy(name, 1, length(name) - 1);
  ext  := ExtractFileExt(metafilename);

  ctx.global_filename := name;
  ctx.global_filepath := path;
  ctx.global_fileext  := ext;

  for I := 1 to 10 do
  begin
    metafilename := '@outfile[' + IntToStr(I) + ']';

    //@outfiles in System ersetzen
    if ctx.analyser_systems.ContainsKey('@outfile[' + IntToStr(I) + ']') then
    begin
       outfilename := TString(ctx.analyser_systems.GetValue(
          '@outfile[' + IntToStr(I) + ']')).getString();

       if pos('@noclearing', outfilename) > 0 then
       begin
          outfilename := AnsiReplaceStr(outfilename, '@noclearing', '');
          ctx.global_noclearing[I] := true;
       end;

       if pos('@concat', outfilename) > 0 then
       begin
          outfilename := AnsiReplaceStr(outfilename, '@concat', '');
          // Globales @concat laden
          if ctx.analyser_globals.ContainsKey('@CONCAT') then
          begin
            value := TString(ctx.analyser_globals.GetValue('@CONCAT'));
            st := value.getString();
            if AnsiLowerCase(value.getString()) = 'true' then
             ctx.global_concat[I] := true;
          end else
             ctx.global_concat[I] := false;
       end else
          ctx.global_concat[I] := false;

      if pos('@once', outfilename) > 0 then
      begin
         outfilename := AnsiReplaceStr(outfilename, '@once', '');
         ctx.global_once[I] := true;
      end else
         ctx.global_once[I] := false;


       outfilename := AnsiReplaceStr(outfilename, '@filedir', path);
       outfilename := AnsiReplaceStr(outfilename, '@filename', name);
       outfilename := AnsiReplaceStr(outfilename, '@fileext', ext);
       outfilename := AnsiReplaceStr(outfilename, '@rootpath', ctx.global_rootpath);
       ctx.global_outfile[I] := outfilename;
    end;

    // @filename in System ersetzen
    if ctx.analyser_systems.ContainsKey('@template[' + IntToStr(I) + ']') then
    begin
       outfilename := TString(ctx.analyser_systems.GetValue(
          '@template[' + IntToStr(I) + ']')).getString();

       outfilename := AnsiReplaceStr(outfilename, '@filedir', path);
       outfilename := AnsiReplaceStr(outfilename, '@filename', name);
       outfilename := AnsiReplaceStr(outfilename, '@fileext', ext);
       outfilename := AnsiReplaceStr(outfilename, '@rootpath', ctx.global_rootpath);
       ctx.global_template[I] := outfilename;
    end;
   end;

    // @filename in Globals ersetzen
    globals := ctx.analyser_globals;
    aset := globals.KeySet;
    It := aset.First();
    while It.HasNext do
    begin
      key := It.Next;
      value := TString(globals.GetValue(key));
      line := value.getString();
      line := AnsiReplaceStr(line, '@filename', ctx.global_filename);
      line := AnsiReplaceStr(line, '@filepath', ctx.global_filepath);
      line := AnsiReplaceStr(line, '@fileext', ctx.global_fileext);
      value.replace(line);
    end;
end;

function TProcessIdentifiers.execute(AContext: TContext) : boolean;
var
  ctx             : TGeneratorContext;
  status          : boolean;
begin
  result := false;
  ctx := TGeneratorContext(AContext);
  if not ctx.commandProcessIdentifiers then exit;

  status := ctx.isStatus('AnalyseGlobals');
  if not status then raise Exception.Create('Erwarte AnalyseGlobals.');

  status := ctx.isStatus('AnalyseSystems');
  if not status then raise Exception.Create('Erwarte AnalyseSystems.');

  processFilenames(ctx);
  ctx.setStatus('ProcessIdentifiers');
  result := false;
end;

initialization
  RegisterClass(TProcessIdentifiers);

end.

