(* --------------------------------------------------------------------------

   Pointers OR - Generator (PORG)
   Pointers - EDV auf den Punkt gebracht
   Dokumentation: http://pointers.de/delphi-or-mapper.html

   Version: 2.0
   $Id: properties.pas,v 1.1.1.1 2010/02/28 12:15:11 manfred Exp $
  ---------------------------------------------------------------------------
 *****************************************************************************
 *                                                                           *
 *  This file is part of the Pointers OR - Generator                         *
 *                                                                           *
 *  Copyright [2008-2010] Dipl.-Inf. Manfred Wolff                           *
 *                                                                           *
 *  Licensed under the Apache License, Version 2.0 (the "License");          *
 *  you may not use this file except in compliance with the License.         *
 *  You may obtain a copy of the License at                                  *
 *                                                                           *
 *      http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                           *
 *  Unless required by applicable law or agreed to in writing, software      *
 *  distributed under the License is distributed on an "AS IS" BASIS,        *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 *  See the License for the specific language governing permissions and      *
 *  limitations under the License.                                           *
 *                                                                           *
 *****************************************************************************
*)
unit properties;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, HashMap;

type
  TProperties = class
  private
    properties : TStrHashMap;  // Abgespeicherte Key-/Valuepaare
    list       : TStringList;  // Dateizeilen als Liste aufbereitet
    freeze     : boolean;      // Einfrieren der Properties bis zum erneuten Abspeichern
    filename   : String;       // Datename der .properties Datei
    head       : TStringList;  // Eventuelle Kopfdaten (Kommentarzeilen)
    procedure createProperties();
  public
    constructor Create; overload;
    destructor Destroy; override;
    function get(key: String) : String;
    procedure load(fname : String);
    procedure save();
    procedure put(key: String; value: String);
  end;

implementation

uses strobj, DCL_intf;

(* ----------------------- Instanzmethoden public ----------------------------*)
(*                            Schnittstelle                                   *)
(* ---------------------------------------------------------------------------*)

(* Läd eine .properties Datei und friert sie ein *)
procedure TProperties.load(fname : String);
begin
  if freeze then exit;
  filename := fname;
  list := TStringList.Create();
  list.LoadFromFile(fname);
  createproperties();
  freeze := true;
end;

(* Speichert eine im Speicher vorhandene .propertes Datei *)
procedure TProperties.save();
var
  I    : Integer;
  k, v : String;
  aset : IStrSet;
  it   : IStrIterator;
begin
  list.Clear;
  for I := 0 to head.Count -1 do
  begin
    k := head.Strings[I];
    list.Add(k);
  end;
  aset := properties.KeySet;
  It := aset.First();
  while It.HasNext do
  begin
    k := It.Next;
    v := TString(properties.GetValue(k)).getString();
    list.Add(k + ' = ' + v);
  end;
  list.SaveToFile(filename);
  freeze := false;
end;

(* Fügt den Propeties einen Eintrag hinzu *)
procedure TProperties.put(key: String; value: String);
begin
  properties.PutValue(key, TString.Create(value));
end;

(* Läd ein Key-/Valuepaar *)
function  TProperties.get(key: String) : String;
begin
 result := TString(properties.GetValue(key)).getString();
end;

(* -------------------- Konstruktion, Destruktion ----------------------------*)

constructor TProperties.Create;
begin
  inherited;
  properties := TStrHashMap.Create();
  head := TStringList.Create();
end;

destructor TProperties.Destroy;
begin
   if properties <> nil then properties.Free();
   if list <> nil then list.Free();
   if head <> nil then head.Free();
   inherited;
end;

(* ---------------------- Instanzmethoden private ----------------------------*)

(* Erzeugt Properties aus einer Datei *)
procedure TProperties.createproperties();
var
  I    : Integer;
  K, V : String;
  line : String;
  top  : boolean;
begin
  top := true;
  For I := 0 to list.Count -1 do
  begin
    line := list.Strings[I];
    line := Trim(line);
    if (line <> '') and (line[1] <> '#') and (pos('=', line) > 0) then
    begin
      top := false;
      k := copy(line, 1, pos('=', line) - 1);
      k := Trim(k);
      v := copy(line, pos('=', line) + 1, length(line) - pos('=', line));
      v := Trim(v);
      properties.PutValue(k, TString.Create(v));
    end else
    begin
      if top then
         head.Add(line);
    end;
  end;
end;

end.

