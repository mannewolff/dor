unit ptrcalendar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
(**
  * Funktionen zur Umrechnung von Datum und Uhrzeit in ein internes Format und
  * Berechnung von Differenzen. Datumsdifferenzen werden absolut bemessen.
  *)
  TPtrCalendar = class
  public
    function TimeStrToIntern(s : String) : LongInt;
    function InternToTimeStr(i : LongInt) : String;
    function DateStrToIntern(s : String) : Double;
    function InternToDateStr(d : Double) : String;
    function DateDiff(s1, s2 :  String) : Double;
    function TimeDiff(s1, s2 :  String) : LongInt;
    function TimeDiffStr(s1, s2 :  String) : String;
  end;


implementation

uses AnalyserTools, Vector;

(**
  * Wandelt die Zeit in ein internes Format um.
  *)
function TPtrCalendar.TimeStrToIntern(s : String) : LongInt;
var
  vec   : TStrVector;
  h, m  : LongInt;
begin
  vec := tokenize(s, ':');
  h   := StrToInt(vec.GetString(0));
  m   := StrToInt(vec.GetString(1));
  result := (h*60) + m;
end;

(**
  * Wandelt ein internes Zeitformat in einen String um.
  *)
function TPtrCalendar.InternToTimeStr(i : LongInt) : String;
var
  h, m   : Longint;
  sh, sm : String;
begin
  h := i div 60;
  m := i mod 60;

  sh := intToStr(h);
  if length(sh) = 1 then sh := '0' + sh;
  sm := intToStr(m);
  if length(sm) = 1 then sm := '0' + sm;

  result := sh + ':' + sm;

end;

(**
  * Wandelt das Datum in ein internes Format um.
  *)
function TPtrCalendar.DateStrToIntern(s : String) : Double;
begin
  result := StrToDate(s);
end;

(**
  * Wandelt ein internes Datum in einen String um.
  *)
function TPtrCalendar.InternToDateStr(d : Double) : String;
begin
  result := DateToStr(d);
end;

(**
  * Bildet die absolute Differenz zwischen zwei Datumswerten.
  *)
function TPtrCalendar.DateDiff(s1, s2 :  String) : Double;
var
  d : Double;
begin
  d := (DateStrToIntern(s1)-DateStrToIntern(s2));
  if d < 0 then d := d * -1;
  result := d;
end;

(**
  * Bildet die Differenz zwischen zwei Zeiten mit 00:00 Überlauf.
  *)
function TPtrCalendar.TimeDiff(s1, s2 :  String) : LongInt;
var
  l1 : LongInt;
  l2 : LongInt;
begin
  l1 := TimeStrToIntern(s1);
  l2 := TimeStrToIntern(s2);
  if l2 < l1 then l2 := l2 + 1440;
  result := l2 - l1;
end;

(**
  * Bildes die Differenz zwischen zwei Zeiten mit 00:00 Überlauf als String.
  *)
function TPtrCalendar.TimeDiffStr(s1, s2 :  String) : String;
begin
  result := InternToTimeStr(TimeDiff(s1,s2));
end;

end.

