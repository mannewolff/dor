
USE faktura;
  DROP TABLE IF EXISTS user;
  CREATE TABLE user (
    Id int(11) NOT NULL auto_increment,
    createuser varchar(50) NOT NULL default 0,
    createdate varchar(50) NOT NULL default 0,
    modifyuser varchar(50) NOT NULL default 0,
    modifydate varchar(50) NOT NULL default 0,
    version int(11) NOT NULL default 0,
    uniqueString varchar(50) default NULL,
    active int(1) NOT NULL default 1,
    loginname varchar(50) NOT NULL,
    password varchar(50) NOT NULL,
    prename varchar(100) NOT NULL,
    postname varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    enabled int(1) default 1,
    PRIMARY KEY  (Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 	







