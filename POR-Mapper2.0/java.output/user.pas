(******************************************************************************
 ******************************************************************************
    Pointers - EDV auf den Punkt gebracht
    http://www.pointers.de
    wolff(at)pointers.de

 ******************************************************************************
    Detail: Generierte Fachklasse TUser f�r Lazarus !
 ******************************************************************************
  
  Generierungdatum: 13.03.2019 16:17:41

  ACHTUNG: DIESE UNIT WURDE GENERIERT. DESHALB DARF AN DEM QUELLTEXT
  NICHTS GE�NDERT WERDEN. FALLS ZUS�TZLICHE METHODEN NOTWENDIG WERDEN
  BITTE VON DIESER KLASSE ERBEN UND DIE METHODEN IN DER VERERBTEN KLASSE
  IMPLEMENTIEREN.
  Pointers OR - Generator 2.0
 *****************************************************************************)


unit user;

interface

uses
     SysUtils, Classes, persistenz, Sqldb, 
     HashMap,
     uDebugger;

type
   TUser = class(TPersistenz)
     private
      stringList : TStringList;
     public
      loginname : AnsiString;
      password : AnsiString;
      prename : AnsiString;
      postname : AnsiString;
      email : AnsiString;
      enabled : Integer;

      constructor Create();overload;
      constructor Create(Id : Integer);overload;
      constructor Create(Filter : String);overload;
      destructor  Destroy();override;
	  function    Clone() : TUser;

      function  getMETA() : TStringList; override;
      function  getTableName() : String; override;
      procedure setParams(bool : boolean); override;
      procedure refreshObject(var aObject: TPersistenz); override;
      function  createObject() : TPersistenz; override;
      procedure setFields(var obj : TUser);
      procedure queryToEditFields(map : TStrHashMap);override;
      // Assoziationen zu anderen Objekten
   end;

implementation


uses formfaktura;

// =============================================================================
//                         Methoden von TUser
// =============================================================================

(*********************************************************************
 Clone Operator
 Clont das Fachobject
 *********************************************************************)
function TUser.Clone() : TUser;
var
  cloneObj: TUser;
begin
  cloneObj := TUser.Create();
  cloneObj.loginname := self.loginname;
  cloneObj.password := self.password;
  cloneObj.prename := self.prename;
  cloneObj.postname := self.postname;
  cloneObj.email := self.email;
  cloneObj.enabled := self.enabled;
  result := cloneObj;
 end;

(*********************************************************************
 Constructor
 Baut die Attributliste auf.
 *********************************************************************)
constructor TUser.Create();
begin
  inherited Create();
  getMeta();
  query             := TSQLQuery.Create(nil);
  query.DataBase    := frmFakturaMain.MySQL50;
  query.Transaction := frmFakturaMain.SQLTransaction;
  actUser := 'Manfred Wolff';
end;

(*********************************************************************
 Constructor
 L�d ein Objekt per ID
 *********************************************************************)
constructor TUser.Create(Id : Integer);
begin
  inherited Create();
  getMeta();
  query             := TSQLQuery.Create(nil);
  query.DataBase    := frmFakturaMain.MySQL50;
  query.Transaction := frmFakturaMain.SQLTransaction;
  actUser := 'Manfred Wolff';
  loadPerId(id);
end;

(*********************************************************************
 Constructor
 L�d ein Objekt per Filger
 *********************************************************************)
constructor TUser.Create(Filter : String);
begin
  inherited Create();
  getMeta();
  query             := TSQLQuery.Create(nil);
  query.DataBase    := frmFakturaMain.MySQL50;
  query.Transaction := frmFakturaMain.SQLTransaction;
  actUser := 'Manfred Wolff';
  loadPerFilter(Filter);
end;

(*********************************************************************
 L�scht das Objekt und die dazugeh�rige Query
 *********************************************************************)
Destructor TUser.Destroy();
begin
  query.Close;
  FreeAndNil(query);
  if assigned(stringList) then stringList.Free();
  inherited;
end;

(*********************************************************************
 Gibt die META-Daten f�r diese Klasse zur�ck.
 *********************************************************************)
function TUser.getMETA() : TStringList;
begin
  result := stringList;
  if assigned(stringList) then exit;
  stringList := TStringList.Create();
  stringList.Add('loginname');
  stringList.Add('password');
  stringList.Add('prename');
  stringList.Add('postname');
  stringList.Add('email');
  stringList.Add('enabled');
end;

(*********************************************************************
Gibt den Datenbank-Tabellennamen f�r diese Klasse zur�ck.
 *********************************************************************)
function TUser.getTableName() : String;
begin
  result := 'user';
end;

(*********************************************************************
Setzt die Werte, die in der Datenbank gespeichert sind in das Objekt.
 *********************************************************************)
procedure TUser.refreshObject(var aObject : TPersistenz);
var
  objects : TUser;
begin
   objects := aObject as TUser;
   setFields(objects);
   inherited refreshObject(aObject);
end;

(*********************************************************************
Setzt die Werte, die in der Datenbank gespeichert sind in das Objekt.
 *********************************************************************)
procedure TUser.setFields(var obj : TUser);
begin
  obj.loginname := query.Fields.FieldByName('loginname').AsString;
  obj.password := query.Fields.FieldByName('password').AsString;
  obj.prename := query.Fields.FieldByName('prename').AsString;
  obj.postname := query.Fields.FieldByName('postname').AsString;
  obj.email := query.Fields.FieldByName('email').AsString;
  obj.enabled := query.Fields.FieldByName('enabled').AsInteger;
end;

(*********************************************************************
Setzt die Werte, aus dem Objekt in die Datenbank.
 *********************************************************************)
procedure TUser.setParams(bool : boolean);
begin
  paramInt := 0;
  query.Params[paramInt].AsString := loginname; inc(paramInt);
  query.Params[paramInt].AsString := password; inc(paramInt);
  query.Params[paramInt].AsString := prename; inc(paramInt);
  query.Params[paramInt].AsString := postname; inc(paramInt);
  query.Params[paramInt].AsString := email; inc(paramInt);
  query.Params[paramInt].AsInteger := enabled; inc(paramInt);
  inherited setParams(bool);
end;

(*********************************************************************
Erzeugt ein Objekt vom Typ TUser.
 *********************************************************************)
function TUser.createObject() : TPersistenz;
var
   obj : TUser;
begin
  obj := TUser.Create();
  result := obj;
end;

(*********************************************************************
Setzt Werte aus der Query direkt in View-Editfields
 *********************************************************************)
procedure TUser.queryToEditFields(map : TStrHashMap);
begin
  inherited queryToEditFields(map);
end;

(*********************************************************************
 Generierte 1:1 Zugriffsmethode (wenn vorhanden)
 *********************************************************************)

(*********************************************************************
 Generierte 1:1 L�schmethode (wenn vorhanden)
 *********************************************************************)

(*********************************************************************
 Generierte 1:N Zugriffsmethode (wenn vorhanden)
 *********************************************************************)

(*********************************************************************
 Generierte 1:1 (N:1) Revert Zugriffsmethode (wenn vorhanden)
 *********************************************************************)

(*********************************************************************
 Generierte 1:1 (N:1) Revert L�schmethode (wenn vorhanden)
 *********************************************************************)

end.
