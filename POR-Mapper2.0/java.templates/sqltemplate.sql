#(******************************************************************************
# ******************************************************************************
#    Faktura 2012
#    (c) Pointers - EDV auf den Punkt gebracht 2012
#
#    Realisiert durch:
#    Pointers - EDV auf den Punkt gebracht
#    http://www.pointers.de wolff@pointers.de
# ******************************************************************************
#    Detail: Template f�r die SQL-Statement Datei.
# ******************************************************************************
# 
# 
# ******************************************************************************)

# Erzeugung des CREATE TABLE statements
@ifdef NEUSCHEMA
USE @SCHEMA;
  DROP TABLE IF EXISTS @TABLE_NAME;
  CREATE TABLE @TABLE_NAME (
    Id int(11) NOT NULL auto_increment,
    createuser varchar(50) NOT NULL default 0,
    createdate varchar(50) NOT NULL default 0,
    modifyuser varchar(50) NOT NULL default 0,
    modifydate varchar(50) NOT NULL default 0,
    version int(11) NOT NULL default 0,
    uniqueString varchar(50) default NULL,
    active int(1) NOT NULL default 1,
# Attribute
    @sqlattributes {notempty @Attribute.DBTyp}
# Other
    @foreach @Other do
    @Other.Text,
    @endforeach    
    PRIMARY KEY  (Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 	

@foreach @ForeignKey @do
     ^ALTER TABLE @TABLE_NAME ADD CONSTRAINT @ForeignKey.IndexName |
     ^FOREIGN KEY (@ForeignKey.IndexColName) |
     ^REFERENCES @ForeignKey.TableName (@ForeignKey.Tablecolname)|
     @if ((@ForeignKey.OnDelete <> ''))
     ^ ON DELETE @ForeignKey.OnDelete|
     @endif  
     @if ((@ForeignKey.OnUpdate <> ''))
        ^ ON UPDATE @ForeignKey.OnUpdate|
     @endif  
     ^;
     @endif  
@endforeach   

@foreach @Indizes @do
     ^ALTER TABLE @TABLE_NAME ADD Index  @Indizes.Indexname (@Indizes.Indexspalte);
@endforeach   

@enddef

# Modify Modus: Neue Attribute
@ifndef NEUSCHEMA
@foreach @Attribute @do
  @if (@Attribute.Signifikant == @add)
    ^ALTER TABLE @TABLE_NAME ADD COLUMN @Attribute.Identifier @Attribute.DBTyp @Attribute.Additional;
  @endif
@endforeach    
@enddef

# Modify Modus: Attrbute entfernen
@ifndef NEUSCHEMA
@foreach @Attribute @do
  @if (@Attribute.Signifikant == @delete)
    ^ALTER TABLE @TABLE_NAME DROP COLUMN @Attribute.Identifier;
  @endif
@endforeach    
@enddef

# Modify Modus: Neue Foreign Key Constraints
@ifndef NEUSCHEMA
@foreach @ForeignKey @do
     @if (@foreignkey.Signifikant = @add)
     ^ALTER TABLE @TABLE_NAME ADD CONSTRAINT @ForeignKey.IndexName |
     ^FOREIGN KEY (@ForeignKey.IndexColName) |
     ^REFERENCES @ForeignKey.TableName (@ForeignKey.Tablecolname)|
     @endif
     @if ((@ForeignKey.OnDelete <> '') and (@ForeignKey.Signifikant == @add))
     ^ ON DELETE @ForeignKey.OnDelete|
     @endif  
     @if ((@ForeignKey.OnUpdate <> '') and (@ForeignKey.Signifikant == @add))
        ^ ON UPDATE @ForeignKey.OnUpdate|
     @endif  
     @if (@foreignkey.Signifikant = @add)
     ^;
     @endif  
@endforeach   
@enddef

# Modify Modus: Neue Indizes
@ifndef NEUSCHEMA
@foreach @Indizes @do
     @if (@Indizes.Signifikant == @add)
     ^ALTER TABLE @TABLE_NAME ADD Index  @Indizes.Indexname (@Indizes.Indexspalte);
     @endif
@endforeach   
@enddef