(******************************************************************************
 ******************************************************************************
    Pointers - EDV auf den Punkt gebracht
    http://www.pointers.de
    wolff(at)pointers.de

 ******************************************************************************
    Detail: Generierte Fachklasse @OBJ_NAME f�r Lazarus !
 ******************************************************************************
  CVS/Subversion - Version Information:
  @Id: @
  
  Generierungdatum: @datetime

  ACHTUNG: DIESE UNIT WURDE GENERIERT. DESHALB DARF AN DEM QUELLTEXT
  NICHTS GE�NDERT WERDEN. FALLS ZUS�TZLICHE METHODEN NOTWENDIG WERDEN
  BITTE VON DIESER KLASSE ERBEN UND DIE METHODEN IN DER VERERBTEN KLASSE
  IMPLEMENTIEREN.
  Pointers OR - Generator 2.0
 *****************************************************************************)


unit @UNITSdao;

interface

uses
     @USES {notempty @Attribute.Packages}
     SysUtils, Classes, @UNITS;

type
   @OBJ_NAMEDAO = class(@OBJ_NAME)
     private
     public
   end;

implementation

// =============================================================================
//                         Methoden von @OBJ_NAMEDAO
// =============================================================================

end.
