(******************************************************************************
 ******************************************************************************
    Pointers - EDV auf den Punkt gebracht
    http://www.pointers.de
    wolff(at)pointers.de

 ******************************************************************************
    Detail: Generierte Fachklasse @OBJ_NAME f�r Lazarus !
 ******************************************************************************
  CVS/Subversion - Version Information:
  @Id: @
  
  Generierungdatum: @datetime

  ACHTUNG: DIESE UNIT WURDE GENERIERT. DESHALB DARF AN DEM QUELLTEXT
  NICHTS GE�NDERT WERDEN. FALLS ZUS�TZLICHE METHODEN NOTWENDIG WERDEN
  BITTE VON DIESER KLASSE ERBEN UND DIE METHODEN IN DER VERERBTEN KLASSE
  IMPLEMENTIEREN.
  Pointers OR - Generator 2.0
 *****************************************************************************)


unit @UNITS;

interface

uses
     @USES {notempty @Attribute.Packages}
     SysUtils, Classes, persistenz, SqlExpr, HashMap, windows,
     @ifdef DEBUG
     ulogging,
     @enddef
     uDebugger;

type
   @OBJ_NAME = class(TPersistenz)
     private
      stringList : TStringList;
     public
      @Attributlist

      constructor Create(keep : Boolean = false; remark: String = '');overload;
      constructor Create(Id : Integer; remark: String = '');overload;
      constructor Create(Filter : String; remark: String = '');overload;
      destructor  Destroy();override;
	    function    Clone() : @OBJ_NAME;

      function  getMETA() : TStringList; override;
      function  getTableName() : String; override;
      procedure setParams(bool : boolean); override;
      procedure refreshObject(var aObject: TPersistenz); override;
      function  createObject() : TPersistenz; override;
      procedure setFields(var obj : @OBJ_NAME);
      procedure queryToEditFields(map : TStrHashMap);override;
      // Assoziationen zu anderen Objekten
      @Associationkopf1zuN
      @Associationkopf1zu1
      @Associationkopf1zu1revert
   end;

implementation


uses dmKohneMySQL, uCommonHelper, benutzerimpl;

// =============================================================================
//                         Methoden von @OBJ_NAME
// =============================================================================

(*********************************************************************
 Clone Operator
 Clont das Fachobject
 *********************************************************************)
function @OBJ_NAME.Clone() : @OBJ_NAME;
var
  cloneObj: @OBJ_NAME;
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.Clone()', 'Begin');
  @enddef
  cloneObj := @OBJ_NAME.Create();
  @foreach @Attribute @do
  @if (@Attribute.Identifier <> 'id')
  cloneObj.@Attribute.Identifier := self.@Attribute.Identifier;
  @endif
  @EndForEach
  result := cloneObj;
  @ifdef DEBUG
  Trace('@OBJ_NAME.Clone()', 'Ende');
  @enddef
 end;

(*********************************************************************
 Constructor
 Baut die Attributliste auf.
 *********************************************************************)
constructor @OBJ_NAME.Create(keep : Boolean = false; remark: String = '');
var
  benutzer : TBenutzerImpl;
@ifdef DEBUG
var 
  ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.Create()', 'Begin');
  ticks := getTickCount;
  @enddef
  inherited Create();
  @ifdef DEBUG
  getDebugger().AddReference('@OBJ_NAME', self, '@OBJ_NAME.Create() ' + remark);
  @enddef

  query                  := TSQLQuery.Create(nil);
  query.SQLConnection    := KohneDatenModul.SQLConnection;
  benutzer := getCommonHelper().getUser();
  if benutzer <> nil then
  actUser :=   benutzer.vorname + ' ' + benutzer.nachname +
            ' (' + benutzer.username + ')' else
  actUser := 'system';
  stringList := nil;
  getMeta();

  @ifdef DEBUG
  Trace('@OBJ_NAME.Create()', 'Ende');
  Trace('@OBJ_NAME.Create()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
 Constructor
 L�d ein Objekt per ID
 *********************************************************************)
constructor @OBJ_NAME.Create(Id : Integer;remark: String = '');
var
  benutzer : TBenutzerImpl;
@ifdef DEBUG
var 
  ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.Create(Id)', 'Begin');
  ticks := getTickCount;
  @enddef
  inherited Create();
  @ifdef DEBUG
  getDebugger().AddReference('@OBJ_NAME', self, '@OBJ_NAME.Create(Id) ' + remark);
  @enddef

  query                  := TSQLQuery.Create(nil);
  query.SQLConnection    := KohneDatenModul.SQLConnection;
  benutzer := getCommonHelper().getUser();
  if benutzer <> nil then
  actUser :=   benutzer.vorname + ' ' + benutzer.nachname +
            ' (' + benutzer.username + ')' else
  actUser := 'system';
  stringList := nil;
  getMeta();

  loadPerId(id);
  @ifdef DEBUG
  Trace('@OBJ_NAME.Create(Id)', 'Ende');
  Trace('@OBJ_NAME.Create(Id)', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
 Constructor
 L�d ein Objekt per Filger
 *********************************************************************)
constructor @OBJ_NAME.Create(Filter : String;remark: String = '');
var
  benutzer : TBenutzerImpl;
@ifdef DEBUG
var 
  ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.Create(Filter)', 'Begin');
  ticks := getTickCount;
  @enddef
  inherited Create();
  @ifdef DEBUG
  getDebugger().AddReference('@OBJ_NAME', self, '@OBJ_NAME.Create(Filter) ' + remark);
  @enddef

  query                  := TSQLQuery.Create(nil);
  query.SQLConnection    := KohneDatenModul.SQLConnection;
  benutzer := getCommonHelper().getUser();
  if benutzer <> nil then
  actUser :=   benutzer.vorname + ' ' + benutzer.nachname +
            ' (' + benutzer.username + ')' else
  actUser := 'system';
  stringList := nil;
  getMeta();

  loadPerFilter(Filter);
  @ifdef DEBUG
  Trace('@OBJ_NAME.Create(Filter)', 'Ende');
  Trace('@OBJ_NAME.Create(Filter)', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
 L�scht das Objekt und die dazugeh�rige Query
 *********************************************************************)
Destructor @OBJ_NAME.Destroy();
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.Destroy()', 'Begin');
  ticks := getTickCount;
  @enddef
  @ifdef DEBUG
  Trace('@OBJ_NAME.Destroy()', 'Close()');
  @enddef
  if assigned(query) then query.Close;
  @ifdef DEBUG
  Trace('@OBJ_NAME.Destroy()', 'FreeAndNil(query)');
  @enddef
  if assigned(query) then FreeAndNil(query);
  @ifdef DEBUG
  Trace('@OBJ_NAME.Destroy()', 'FreeAndNil(stringList)');
  @enddef
  if assigned(stringList) then FreeAndNil(stringList);
  @ifdef DEBUG
  getDebugger().DeleteReference('@OBJ_NAME', self);
  @enddef
  @ifdef DEBUG
  Trace('@OBJ_NAME.Destroy()', 'Ende');
  Trace('@OBJ_NAME.Destroy()', formatTicks(getTickCount - ticks));
  @enddef
  inherited;
end;

(*********************************************************************
 Gibt die META-Daten f�r diese Klasse zur�ck.
 *********************************************************************)
function @OBJ_NAME.getMETA() : TStringList;
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.getMETA()', 'Begin');
  ticks := getTickCount;
  @enddef
  result := stringList;
  if not assigned(stringList) then 
  begin
    stringList := TStringList.Create();
    @foreach @Attribute @do
    stringList.Add('@Attribute.Identifier');
    @EndForEach
  end;
  @ifdef DEBUG
  Trace('@OBJ_NAME.getMETA()', 'Ende');
  Trace('@OBJ_NAME.getMETA()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
Gibt den Datenbank-Tabellennamen f�r diese Klasse zur�ck.
 *********************************************************************)
function @OBJ_NAME.getTableName() : String;
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.getTableName()', 'Begin');
  @enddef
  result := '@TABLE_NAME';
  @ifdef DEBUG
  Trace('@OBJ_NAME.getTableName()', 'Ende');
  @enddef
end;

(*********************************************************************
Setzt die Werte, die in der Datenbank gespeichert sind in das Objekt.
 *********************************************************************)
procedure @OBJ_NAME.refreshObject(var aObject : TPersistenz);
var
  objects : @OBJ_NAME;
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.refreshObject()', 'Begin');
  ticks := getTickCount;
  @enddef
   objects := aObject as @OBJ_NAME;
   setFields(objects);
   inherited refreshObject(aObject);
  @ifdef DEBUG
  Trace('@OBJ_NAME.refreshObject()', 'Ende');
  Trace('@OBJ_NAME.refreshObject()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
Setzt die Werte, die in der Datenbank gespeichert sind in das Objekt.
 *********************************************************************)
procedure @OBJ_NAME.setFields(var obj : @OBJ_NAME);
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.setFields()', 'Begin');
  ticks := getTickCount;
  @enddef
  @setfieldlist
  @ifdef DEBUG
  Trace('@OBJ_NAME.setFields()', 'Ende');
  Trace('@OBJ_NAME.setFields()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
Setzt die Werte, aus dem Objekt in die Datenbank.
 *********************************************************************)
procedure @OBJ_NAME.setParams(bool : boolean);
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.setParams()', 'Begin');
  ticks := getTickCount;
  @enddef
  paramInt := 0;
  @foreach @Attribute @do
  @if(copy @Attribute.Identifier 1 3 == NFK)
  if (@Attribute.Identifier <> 0) then
  begin
    query.Params[paramInt].@Attribute.Subtyp := @Attribute.Identifier; 
    inc(paramInt);
  end else
    inc(paramInt);
  @endif
  @if(copy @Attribute.Identifier 1 3 <> NFK)
  query.Params[paramInt].@Attribute.Subtyp := @Attribute.Identifier; inc(paramInt);
  @endif
  @endforeach
  inherited setParams(bool);
  @ifdef DEBUG
  Trace('@OBJ_NAME.setFields()', 'Ende');
  Trace('@OBJ_NAME.setFields()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
Erzeugt ein Objekt vom Typ @OBJ_NAME.
 *********************************************************************)
function @OBJ_NAME.createObject() : TPersistenz;
var
   obj : @OBJ_NAME;
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.createObject()', 'Begin');
  ticks := getTickCount;
  @enddef
  obj := @OBJ_NAME.Create();
  result := obj;
  @ifdef DEBUG
  Trace('@OBJ_NAME.createObject()', 'Ende');
  Trace('@OBJ_NAME.createObject()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
Setzt Werte aus der Query direkt in View-Editfields
 *********************************************************************)
procedure @OBJ_NAME.queryToEditFields(map : TStrHashMap);
@ifdef DEBUG
var ticks : DWORD;
@enddef
begin
  @ifdef DEBUG
  Trace('@OBJ_NAME.queryToEditFields()', 'Begin');
  ticks := getTickCount;
  @enddef
  inherited queryToEditFields(map);
  @ifdef DEBUG
  Trace('@OBJ_NAME.queryToEditFields()', 'Begin');
  Trace('@OBJ_NAME.queryToEditFields()', formatTicks(getTickCount - ticks));
  @enddef
end;

(*********************************************************************
 Generierte 1:1 Zugriffsmethode (wenn vorhanden)
 *********************************************************************)
@Associationimpl1zu1

(*********************************************************************
 Generierte 1:1 L�schmethode (wenn vorhanden)
 *********************************************************************)
@Assocationimpldelete1zu1

(*********************************************************************
 Generierte 1:N Zugriffsmethode (wenn vorhanden)
 *********************************************************************)
@AssociationeImpl1zuN

(*********************************************************************
 Generierte 1:1 (N:1) Revert Zugriffsmethode (wenn vorhanden)
 *********************************************************************)
@Associationimpl1zu1revert

(*********************************************************************
 Generierte 1:1 (N:1) Revert L�schmethode (wenn vorhanden)
 *********************************************************************)
@Assocationimpldelete1zu1revert

end.
